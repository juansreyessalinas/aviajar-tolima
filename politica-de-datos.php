<?php include("partials/funciones.php");

$publicidad1= busquedaIndividual("imagenes", "id = 46");
$not= busquedasGenerales("noticias", "WHERE estado = 1 ", "ORDER BY id DESC LIMIT 0,3");
$metas       = busquedaIndividual("metatags", "id = 1"); 
$dato1 = busquedaIndividual("paginas", "id=1");
$dato2 = busquedaIndividual("paginas", "id=2");
$dato3 = busquedaIndividual("paginas", "id=3");
$dato4 = busquedaIndividual("paginas", "id=4");
$dato5 = busquedaIndividual("paginas", "id=5");
$dato6 = busquedaIndividual("paginas", "id=6");

$img1 = busquedaIndividual("imagenes", "id=103");
$img2 = busquedaIndividual("imagenes", "id=93");
$img3 = busquedaIndividual("imagenes", "id=94");
$img4 = busquedaIndividual("imagenes", "id=95");
$img5 = busquedaIndividual("imagenes", "id=96");
$img6 = busquedaIndividual("imagenes", "id=97");

?>
<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8" />
            <title>POLITICAS DE DATOS - A VIAJAR TOLIMA OPERADORES TURISTICOS</title>
        <meta name="description" content="POLITICAS DE DATOS  - A VIAJAR TOLIMA OPERADORES TURISTICOS">
        <meta name="author" content="www.creasotol.com, contactos@creasotol.com">
        <meta name="Subject" content="POLITICAS DE DATOS  - A VIAJAR TOLIMA OPERADORES TURISTICOS. ">
        <meta name="author" content="www.creasotol.com, contactos@creasotol.com">
        <meta name="robots" content="index,follow">
        <meta NAME="Language" CONTENT="Spanish">
        <meta NAME="Revisit" CONTENT="1 day">
        <meta NAME="Distribution" CONTENT="Global">
        <!--para desactivar el cache de todos los navegadores-->
        <meta http-equiv="cache-control" content="max-age=0" />
        <meta http-equiv="cache-control" content="no-cache, no-store, must-revalidate" />
        <meta http-equiv="pragma" content="no-cache" />
        <meta http-equiv="Expires" content="0">
        <meta http-equiv="Last-Modified" content="0">
        <!--cierra-->
        <!-- Mobile Specific Metas
        ================================================== -->
        
        <?php include 'partials/scriptsGlobal.php';  ?>
        
    </head>
    <body id="pageRecomendados" class="">                
        <?php include 'partials/header.php'; ?>


        <div id="cont-slider">
            <img src="public/img/imagenes/105.jpg<?php echo elRandom()?>" class="img-interna" alt="a viajar tolima - operadores turisticos">
        </div>


        <div class="informacion disTable">
            <div class="col-izq">
                <h2>POLITICAS DE DATOS</h2>

                <p>
                  <?php echo $dato6['detalle'] ?>                </p>
            </div>
            <div class="col-der">
                <img src="public/img/iconos.png" alt="a viajar tolima" class="iconos">
                <h2>Viajar es pasión, somos la opción</h2>
                <img src="public/img/imagenes/107.jpg<?php echo elRandom()?>" alt="a viajar tolima" class="certificado">
                <img src="public/img/imagenes/108.jpg<?php echo elRandom()?>" alt="a viajar tolima" class="sostenibilidad">
            </div>
        </div>       
        
        

        <?php include 'partials/implementacion.php'; ?>        

        <div class="img-full">
            <img src="public/img/imagenes/106.jpg<?php echo elRandom()?>" alt="caño cristales">
        </div>
        
        <?php include 'partials/footer.php'; ?>        

    </body>
</html>