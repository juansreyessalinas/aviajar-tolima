<?php
include("partials/funciones.php");

//Verificando si esta logueado
if (!isset($_SESSION['usuPA_admin'])) {
    header('Location: index.php');
}

if (isset($_REQUEST['id'])) {
    $dato = busquedaIndividual("tienda_categoria", "id = ".$_REQUEST['id']);
}
$todos = busquedasGenerales("tienda_categoria", "", "ORDER BY id ASC");
?>
<!DOCTYPE html>
<html>
    <head>
        <?php include('partials/header.php');  ?>
    </head>
    <body class="fixed-left">
        <div id="wrapper">
            <?php include('partials/menu_horizontal.php');  ?>       
            <div class="content-page">
                <div class="content">
                    <div class="container">  

                        <!-- Donde Estoy -->
                        <div class="row">
                            <div class="col-sm-12">
                                <h4 class="page-title">Categorias</h4>
                                <ol class="breadcrumb">
                                    <li><a href="index2.php">Inicio</a></li>
                                    <!-- <li><a href="inmobiliaria_seccion.php">Sección</a></li> -->
                                    <li class="active">Categorias</li>
                                </ol>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="card-box">
                                    <form action="#" method="post">
                                        <input type="hidden" name="txtId" id="txtId" value="<?php if(isset($dato['id'])) echo $dato['id'];?>" />
                                        <div class="form-group">

                                            <div class="col-lg-4">
                                                <label>Categoria:</label>
                                                <input type="text" class="form-control" name="txtTitulo" id="txtTitulo" placeholder="Editorial" required="required" value="<?php if(isset($dato['titulo'])) echo $dato['titulo'];?>">
                                            </div>
                                            
                                            <div class="col-lg-4">
                                                <label>Descripción:</label>
                                                <textarea class="form-control" name="txtDescripcion" id="txtDescripcion"><?php if(isset($dato['descripcion'])) echo $dato['descripcion'];?></textarea>
                                            </div>

                                            <div class="col-lg-4">
                                                <label>Estado:</label>
                                                <div class="radio radio-primary">
                                                    <input type="radio" name="rdoActivo" id="rdoActivo" value="1"<?php if(isset($dato['estado']) && $dato['estado'] == 1) echo "checked";?>>
                                                    <label for="radio3">Activo</label> &nbsp; &nbsp; &nbsp; &nbsp;                                                 
                                                    <input type="radio" name="rdoActivo" id="rdoActivo" value="2"<?php if(isset($dato['estado']) && $dato['estado'] == 2) echo "checked";?>>
                                                    <label for="radio3">Inactivo</label>
                                                </div>
                                                <br>
                                            </div>

                                        </div>
                                        <div class="form-group text-center">
                                            <button class="btn btn-primary waves-effect waves-light" type="submit" name="btnGuardar" id="btnGuardar">Guardar</button>
                                            <a href="tienda_categoria.php" class="btn btn-default waves-effect waves-light m-l-5">Cancel</a>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>


                       <div class="row">
                            <div class="col-sm-12">
                                <div class="card-box">
                                    <table id="datatable" class="table table-striped table-bordered">
                                        <thead>
                                            <tr>
                                                <th width="80px" class="text-center">ID</th>
                                                <th class="text-center">Categoria</th>
                                                
                                                <th width="80px" class="text-center">Estado</th>
                                                <th width="160px" class="text-center">Acción</th>
                                            </tr>
                                        </thead>

                                        <tbody>
                                            <?php while ($rowTodos = mysqli_fetch_array($todos)){ 
                                               if ($rowTodos['estado'] == 1)
                                                    $elEstado = '<span class="label label-table label-success" onclick="inactivar('."'tienda_categoria'".', '."'".$rowTodos['id']."'".', 2, '."'tienda_categoria.php'".')" style="cursor:pointer">Activo</span>';
                                                else
                                                    $elEstado = '<span class="label label-table label-danger"  onclick="inactivar('."'tienda_categoria'".', '."'".$rowTodos['id']."'".', 1, '."'tienda_categoria.php'".')" style="cursor:pointer">Inactivo</span>';
                                                ?>
                                                <tr>
                                                    <td class="text-center"><?php echo $rowTodos['id'];?></td>
                                                    <td><?php echo $rowTodos['titulo'];?></td>
                                                    
                                                    <td class="text-center"><?php echo $elEstado;?></td>
                                                    <td class="text-center">
                                                         <a class="btn btn-inverse waves-effect waves-light btn-xs" href="documentos.php?id=<?php echo $rowTodos['id'];?>"><i class="md-collections"></i> Galeria</a>
                                                        <a class="btn btn-inverse waves-effect waves-light btn-xs" href="tienda_categoria.php?id=<?php echo $rowTodos['id'];?>"><i class="ion-edit"></i> Editar</a>
                                                        <a class="btn btn-inverse waves-effect waves-light btn-xs" onclick="eliminarF('tienda_categoria', <?php echo $rowTodos['id'];?>, 'tienda_categoria.php','../../public/img/clientes/<?php echo $rowTodos['id'];?>')"><i class="glyphicon glyphicon-trash"></i> Eliminar</a>
                                                    </td>
                                                </tr>
                                            <?php } ?> 
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <footer class="footer">2016 © Creasotol</footer>
                </div>        
            </div>
        </div>
        <?php include("partials/footer.php"); ?>
        <script type="text/javascript">
            $('.selectpicker').selectpicker();
        </script>
    </body>
</html>
<?php 
if (isset($_REQUEST['btnGuardar'])) {
    $id = $_REQUEST['txtId'];

    if ($id == '') {
        $mensaje = "Se creo la categoria.";
        $campos  = "titulo, descripcion, estado";
        $valores = "'".$_REQUEST['txtTitulo']."', '".$_REQUEST['txtDescripcion']."', '".$_REQUEST['rdoActivo']."'";
        $ide=crearDato("tienda_categoria", $campos, $valores);
        $carpeta = "../public/img/clientes/".$ide."/";

    if(!file_exists($carpeta)){
        mkdir($carpeta, 0777);
        chmod($carpeta, 0777);
    }
    }else{
        $mensaje = "Se modificaron los datos de la categoria.";
        $campos  = "titulo = '".$_REQUEST['txtTitulo']."', descripcion = '".$_REQUEST['txtDescripcion']."', estado = '".$_REQUEST['rdoActivo']."'";
        actualizarDatos("tienda_categoria", $campos, "id = ".$id);
         $carpeta = "../public/img/clientes/".$id."/";

    if(!file_exists($carpeta)){
        mkdir($carpeta, 0777);
        chmod($carpeta, 0777);
    }
    }

    echo "<script> 
            $.Notification.notify('success','top left', 'Exito!', '".$mensaje."');
            setInterval(function(){ location.href = 'tienda_categoria.php' }, 3000);
          </script>";
}
?>