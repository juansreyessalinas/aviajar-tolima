<?php
include("partials/funciones.php");

//Verificando si esta logueado
if (isset($_SESSION['usuPA_admin'])) {
    header('Location: index2.php');
}

?>
<!DOCTYPE html>
<html>
    <head>
        <?php include("partials/header.php"); ?>
    </head>
    <body>
        <div class="account-pages"></div>
        <div class="clearfix"></div>
        <div class="wrapper-page">
        	<div class=" card-box">
            <div class="panel-heading"> 
                <h3 class="text-center"> Inicio <strong class="text-primary">Panel Administrativo</strong> </h3>
            </div>
            <?php //echo md5("123"); ?>
            <div class="panel-body">
            <form class="form-horizontal m-t-20" action="index.php">
                
                <div class="form-group ">
                    <div class="col-xs-12">
                        <input class="form-control" type="text" name="txtUsuario" id="txtUsuario" required="" placeholder="Usuario">
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-xs-12">
                        <input class="form-control" type="password" name="txtClave" id="txtClave" required="" placeholder="Contraseña">
                    </div>
                </div>
                
                <div class="form-group text-center m-t-40">
                    <div class="col-xs-12">
                        <a class="btn btn-primary btn-block text-uppercase waves-effect waves-light" onclick="logAdm()">Ingresar</a>
                    </div>
                </div>
            </form> 
            
            </div>   
            </div>            
        </div>
        
    	<?php include("partials/footer.php"); ?>
        <script type="text/javascript">
            function logAdm(){
                var usu = $("#txtUsuario").val();
                var cla = $("#txtClave").val();

                if (usu == '' || cla == ''){
                    $.Notification.notify('error','top left', 'Diligencie los campos', 'Ingrese su usuario y contraseña para acceder al sistema.')
                }else{
                    $.ajax({
                        method: "POST",
                        url: "partials/metodos.php",
                        data: { accion: "loginAdm", usu: usu, cla: cla }
                    })
                    .done(function( msg ) {

                        if(msg == '1'){                            
                            location.href = "index2.php";
                        }else{
                            $.Notification.notify('error','top left', 'Error!', msg)
                        }
                    }); 
                }
            }
        </script>
	</body>
</html>