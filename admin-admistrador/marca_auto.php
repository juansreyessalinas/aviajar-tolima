<?php
include("partials/funciones.php");

//Verificando si esta logueado
if (!isset($_SESSION['usuPA_admin'])) {
    header('Location: index.php');
}

if (isset($_REQUEST['id'])) {
    $dato = busquedaIndividual("marca_auto", "id = ".$_REQUEST['id']);
}
$todos = busquedasGenerales("marca_auto", "", "ORDER BY id ASC");
?>
<!DOCTYPE html>
<html>
    <head><meta http-equiv="Content-Type" content="text/html; charset=gb18030">
        <?php include('partials/header.php');  ?>
    </head>
    <body class="fixed-left">
        <div id="wrapper">
            <?php include('partials/menu_horizontal.php');  ?>       
            <div class="content-page">
                <div class="content">
                    <div class="container">  

                        <!-- Donde Estoy -->
                        <div class="row">
                            <div class="col-sm-12">
                                <h4 class="page-title">Marca</h4>
                                <ol class="breadcrumb">
                                    <li><a href="index2.php">Inicio</a></li>
                                    <!-- <li><a href="inmobiliaria_seccion.php">Secci贸n</a></li> -->
                                    <li class="active">Marca</li>
                                </ol>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="card-box">
                                    <form action="#" method="post" enctype="multipart/form-data">
                                        <input type="hidden" name="txtId" id="txtId" value="<?php if(isset($dato['id'])) echo $dato['id'];?>" />
                                        <div class="form-group">
                                            
                                            <div class="col-lg-4">
                                                <label>Marca:</label>
                                                <input type="text" class="form-control" name="txtTitulo" id="txtTitulo" placeholder="Nombre" required="required" value="<?php if(isset($dato['nombre'])) echo $dato['nombre'];?>">
                                            </div>
                                          
                                           
                                        
                                         
                                                 
                                        </div>
                                     
                                        <div class="form-group text-center">
                                            
                                            <button class="btn btn-primary waves-effect waves-light" type="submit" name="btnGuardar" id="btnGuardar">Guardar</button>
                                            <a href="marca.php" class="btn btn-default waves-effect waves-light m-l-5">Cancel</a>
                                       
                                    </form>
                                </div>
                            </div>
                        </div>


                       <div class="row">
                            <div class="col-sm-12">
                                <div class="card-box">
                                    <table id="datatable" class="table table-striped table-bordered">
                                        <thead>
                                            <tr>
                                                <th width="80px" class="text-center">ID</th>
                                               
                                                 <th width="150px" class="text-center">Marca </th>
                                                
                                           
                                                <th width="160px" class="text-center">Accion</th>
                                            </tr>
                                        </thead>

                                        <tbody>
                                            <?php while ($rowTodos = mysqli_fetch_array($todos)){ 
                                              
                                                ?>
                                                <tr>
                                                    <td class="text-center"><?php echo $rowTodos['id'];?></td>
                                                    <td><?php echo $rowTodos['nombre'];?></td>
                                                    
                                                    
                                          
                                                    <td class="text-center">
                                                        <a class="btn btn-inverse waves-effect waves-light btn-xs" href="marca_auto.php?id=<?php echo $rowTodos['id'];?>"><i class="ion-edit"></i> Editar</a>
                                                        <a class="btn btn-inverse waves-effect waves-light btn-xs" onclick="eliminar('marca_auto', <?php echo $rowTodos['id'];?>, 'marca_auto.php')"><i class="glyphicon glyphicon-trash"></i> Eliminar</a>
                                                    </td>
                                                </tr>
                                            <?php } ?> 
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <footer class="footer">2016 - Creasotol</footer>
                </div>        
            </div>
        </div>
        <?php include("partials/footer.php"); ?>
        <script type="text/javascript">
            $('.selectpicker').selectpicker();
        </script>
    </body>
</html>
<?php 
if (isset($_REQUEST['btnGuardar'])) {
    $id = $_REQUEST['txtId'];

    if ($id == '') {
        $mensaje = "Se creo la marca.";
        $campos  = "nombre";
        $valores = "'".$_REQUEST['txtTitulo']."'";
        $id=crearDato("marca_auto", $campos, $valores);
    }else{
        $mensaje = "Se modificaron los datos de la marca.";
        $campos  = "nombre = '".$_REQUEST['txtTitulo']."'";
        actualizarDatos("marca_auto", $campos, "id = ".$id);
    }


   echo "<script> 
            $.Notification.notify('success','top left', 'Exito!', '".$mensaje."');
            setInterval(function(){ location.href = 'marca_auto.php' }, 3000);
          </script>";

}
?>