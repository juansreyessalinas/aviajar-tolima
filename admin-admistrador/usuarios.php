<?php
include("partials/funciones.php");

//Verificando si esta logueado
if (!isset($_SESSION['usuPA_admin'])) {
    header('Location: index.php');
}

if (isset($_REQUEST['id'])) {
    $dato = busquedaIndividual("usuario", "id = ".$_REQUEST['id']);
}

$todos = busquedasGenerales("usuario", "", "ORDER BY id ASC");
?>
<!DOCTYPE html>
<html>
    <head><meta http-equiv="Content-Type" content="text/html; charset=gb18030">
        <?php include('partials/header.php');  ?>
    </head>
    <body class="fixed-left">
        <div id="wrapper">
            <?php include('partials/menu_horizontal.php');  ?>
        
            <div class="content-page">
                <div class="content">
                    <div class="container">
    
                        <!-- Donde Estoy -->
                        <div class="row">
                            <div class="col-sm-12">
                                <h4 class="page-title">Usuarios</h4>
                                <ol class="breadcrumb">
                                    <li><a href="index2.php">Inicio</a></li>
                                    <li class="active">Usuarios</li>
                                </ol>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-12">
                                <div class="card-box">
                                    <form action="#" method="post" enctype="multipart/form-data"  class="form-horizontal">
                                        <input type="hidden" name="txtId" id="txtId" value="<?php if(isset($dato['id'])) echo $dato['id'];?>" />                                       

                                        <div class="form-group">
                                            <div class="col-lg-6">
                                                <label>Nombre:</label>
                                                <input type="text" class="form-control" name="txtNombre" id="txtNombre" placeholder="Usuario Prueba" value="<?php if(isset($dato['nombre'])) echo $dato['nombre'];?>" required="required">
                                            </div>
                                                                                      
                                        </div>

                                        <div class="form-group">
                                            <div class="col-lg-6">
                                                <label>Dirección:</label>
                                                <input type="text" class="form-control" name="txtDireccion" id="txtDireccion" placeholder="Cliente Prueba" value="<?php if(isset($dato['direccion'])) echo $dato['direccion'];?>">
                                            </div>
                                            
                                            <div class="col-lg-6">
                                                <label>Teléfonos:</label>
                                                <input type="text" class="form-control" name="txtTelefono" id="txtTelefono" placeholder="111023444" value="<?php if(isset($dato['telefono'])) echo $dato['telefono'];?>">
                                            </div>                                            
                                        </div>

                                        <div class="form-group">
                                            <div class="col-lg-6">
                                                <label>Usuario:</label>
                                                <input type="text" class="form-control" name="txtUsuario" id="txtUsuario" placeholder="11102344" value="<?php if(isset($dato['usuario'])) echo $dato['usuario'];?>" required="required">
                                            </div>
                                            <div class="col-lg-6">
                                                <label>Clave:</label>
                                                <input type="password" class="form-control" name="txtClave" id="txtClave" placeholder="***" <?php if(!isset($_REQUEST['id'])) echo 'required="required"'; ?> >
                                            </div>                                            
                                        </div>

                                        <div class="form-group">
                                            <div class="col-lg-6">
                                                <label>Correo:</label>
                                                <input type="email" class="form-control" name="txtCorreo" id="txtCorreo" placeholder="Correo" value="<?php if(isset($dato['correo'])) echo $dato['correo'];?>">
                                            </div>
                                            
                                            <div class="col-lg-3">
                                                <label>Estado:</label>
                                                <div class="radio radio-primary">
                                                    <input type="radio" name="rdoActivo" id="rdoActivo" required value="1"<?php if(isset($dato['estado']) && $dato['estado'] == 1) echo "checked";?>>
                                                    <label for="radio3">Activo</label> &nbsp; &nbsp; &nbsp; &nbsp;
                                                    <input type="radio" name="rdoActivo" id="rdoActivo" required value="2"<?php if($dato['estado'] == 2 || !isset($_REQUEST['id'])) echo "checked";?>>
                                                    <label for="radio3">Inactivo</label>
                                                </div>                                                
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-lg-12 text-center">
                                                <button class="btn btn-primary waves-effect waves-light" type="submit" name="btnGuardar" id="btnGuardar">Guardar</button>
                                                <a href="usuarios.php" class="btn btn-default waves-effect waves-light m-l-5">Cancel</a>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>

                       <div class="row">
                            <div class="col-sm-12">
                                <div class="card-box">
                                    <table id="datatable" class="table table-striped table-bordered">
                                        <thead>
                                            <tr>
                                                <th width="50px"  class="text-center">ID</th>
                                                <th class="text-center">Nombre</th>
                                                <th width="200px" class="text-center">Correo</th>
                                                <th width="150px" class="text-center">Usuario</th>
                                                <th width="80px"  class="text-center">Estado</th>
                                                <th width="200px" class="text-center">Departamento</th>
                                                <th width="200px" class="text-center">Ciudad</th>
                                                <th width="160px" class="text-center">Acción</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php while ($rowTodos = mysqli_fetch_array($todos)){ 
                                                if ($rowTodos['estado'] == 1)
                                                    $elEstado = '<span class="label label-table label-success" onclick="inactivar('."'usuario'".', '."'".$rowTodos['id']."'".', 2, '."'usuarios.php'".')" style="cursor:pointer">Activo</span>';
                                                else
                                                    $elEstado = '<span class="label label-table label-danger"  onclick="inactivar('."'usuario'".', '."'".$rowTodos['id']."'".', 1, '."'usuarios.php'".')" style="cursor:pointer">Inactivo</span>';
                                                ?>
                                                <tr>
                                                    <td class="text-center"><?php echo $rowTodos['id'];?></td>
                                                    <td class="text-center"><?php echo $rowTodos['nombre'];?></td>
                                                    <td class="text-center"><?php echo $rowTodos['correo'];?></td>
                                                    <td class="text-center"><?php echo $rowTodos['usuario'];?></td>
                                                    <td class="text-center"><?php echo $elEstado;?></td>
                                                    <td class="text-center"><?php echo $rowTodos['departamento'];?></td>
                                                    <td class="text-center"><?php echo $rowTodos['ciudad'];?></td>
                                                    <td class="text-center">
                                                        <a class="btn btn-inverse waves-effect waves-light btn-xs" href="usuarios.php?id=<?php echo $rowTodos['id'];?>"><i class="ion-edit"></i> Editar</a>
                                                        <a class="btn btn-inverse waves-effect waves-light btn-xs" onclick="eliminar('usuario', <?php echo $rowTodos['id'];?>, 'usuarios.php')"><i class="glyphicon glyphicon-trash"></i> Eliminar</a>
                                                    </td>
                                                </tr>
                                           <?php } ?>                                            
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <footer class="footer">2016 © Creasotol</footer>
                </div>        
            </div>
        </div>
        <?php include("partials/footer.php"); ?>
    </body>
</html>

<?php 
if (isset($_REQUEST['btnGuardar'])) {
    $id = $_REQUEST['txtId'];

    if ($id == '') {
        $exisUsu = busquedaCantidad("usuario", "WHERE usuario = '".$_REQUEST['txtUsuario']."'");

        if ($exisUsu > 0) {
            $mensaje .= "El usuario digitado pertenece a un Usuario.";
            $elTip    = "error";
        }else{
            $mensaje .= "Se creo el usuario.";
            $campos   = "nombre, direccion, telefono, correo, usuario, clave, estado, fecha_registro";
            $valores  = "'".$_REQUEST['txtNombre']."', '".$_REQUEST['txtDireccion']."', '".$_REQUEST['txtTelefono']."',
                        '".$_REQUEST['txtCorreo']."', '".$_REQUEST['txtUsuario']."', '".md5($_REQUEST['txtClave'])."', '".$_REQUEST['rdoActivo']."', '".date("Y-m-d H:i:s")."'";
            $id       = crearDato("usuario", $campos, $valores);
            $elTip    = "success";
        }
    }else{
        $datUsu = busquedaIndividual("usuario", "id = ".$id);

        if ($_REQUEST['txtClave'] != '')
            $laClave = ", clave = '".md5($_REQUEST['txtClave'])."'";
        else
            $laClave = "";

        if ($datUsu['usuario'] != $_REQUEST['txtUsuario']) {
            $exisUsu = busquedaCantidad("usuario",  "WHERE usuario = '".$_REQUEST['txtUsuario']."'");

            if ($exisUsu > 0) {
                $mensaje  .= "El usuario ingresado no esta disponible. ";
                $elUsuario = "";
            }else{
                $elUsuario = ", usuario = '".$_REQUEST['txtUsuario']."' ";
            }
        }


        $mensaje .= "Se modificaron los datos del usuario.";
        $campos  = "nombre = '".$_REQUEST['txtNombre']."', 
                    direccion = '".$_REQUEST['txtDireccion']."', telefono = '".$_REQUEST['txtTelefono']."', correo = '".$_REQUEST['txtCorreo']."', estado = '".$_REQUEST['rdoActivo']."' $elUsuario $laClave";
        actualizarDatos("usuario", $campos, "id = ".$id);
        $elTip   = "success";
    }

    echo "<script> 
            $.Notification.notify('".$elTip."','top left', 'Exito!', '".$mensaje."');
            setInterval(function(){ location.href = 'usuarios.php' }, 3000);
          </script>";
}
?>