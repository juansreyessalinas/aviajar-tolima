<?php

include("partials/funciones.php");



//Verificando si esta logueado

if (!isset($_SESSION['usuPA_admin'])) {

    header('Location: index.php');

}



if (isset($_REQUEST['id'])) {

    $dato = busquedaIndividual("imagenes", "id = ".$_REQUEST['id']);

}



$todos = busquedasGenerales("imagenes", "", "ORDER BY id ASC");





?>

<!DOCTYPE html>

<html>

    <head>

        <?php include('partials/header.php');  ?>

    </head>

    <body class="fixed-left">

        <div id="wrapper">

            <?php include('partials/menu_horizontal.php');  ?>

        

            <div class="content-page">

                <div class="content">

                    <div class="container">

    

                        <!-- Donde Estoy -->

                        <div class="row">

                            <div class="col-sm-12">

                                <h4 class="page-title">Imágenes</h4>

                                <ol class="breadcrumb">

                                    <li><a href="index2.php">Inicio</a></li>

                                    <li class="active">Imágenes</li>

                                </ol>

                            </div>

                        </div>



                        <div class="row">

                            <div class="col-lg-12">

                                <div class="card-box">



                                    <form action="#" method="post" enctype="multipart/form-data">

                                        <input type="hidden" name="txtId" id="txtId" value="<?php if(isset($dato['id'])) echo $dato['id'];?>" />



                                        <div class="form-group">

                                            <div class="col-lg-6">

                                                <label>Imágen:</label>

                                                <input type="file" name="txtArchivo" id="txtArchivo" class="filestyle" data-buttontext="Seleccione" data-buttonname="btn-inverse" <?php if(!isset($dato['id'])) echo 'required';?> >

                                            </div>



                                            <div class="col-lg-6">

                                                <label>Titulo:</label>

                                                <input type="text" class="form-control" name="txtTitulo" id="txtTitulo" placeholder="Imagen logo" required="required" value="<?php if(isset($dato['titulo'])) echo $dato['titulo'];?>">

                                                <br>

                                            </div>

                                            <br>

                                        </div>
                                    
 <div class="col-lg-6">

                                                <label>URL:</label>

                                                <input type="text" class="form-control" name="url" id="url" placeholder="Imagen logo" required="required" value="<?php if(isset($dato['url'])) echo $dato['url'];?>">

                                                <br>

                                            </div>

                                            <br>

                                       


                                        <div class="form-group text-center">

                                            <button class="btn btn-primary waves-effect waves-light" type="submit" name="btnGuardar" id="btnGuardar">Guardar</button>

                                            <button type="reset" class="btn btn-default waves-effect waves-light m-l-5">Cancel</button>

                                        </div>

                                    </form>

                                </div>

                            </div>

                        </div>





                        <div class="row">

                            <div class="col-sm-12">

                                <div class="card-box">

                                    <table id="datatable" class="table table-striped table-bordered">

                                        <thead>

                                            <tr>

                                                <th width="80px" class="text-center">ID</th>

                                                <th width="250px" class="text-center">Foto</th>

                                                

                                                <th class="text-center">Imágen</th>

                                                <th width="160px" class="text-center">Acción</th>

                                            </tr>

                                        </thead>



                                        <tbody>

                                            <?php while ($rowTodos = mysqli_fetch_array($todos)){ 

                                                if (file_exists('../public/img/imagenes/'.$rowTodos['id'].'.jpg'))

                                                    $img = '<img src="../public/img/imagenes/'.$rowTodos['id'].'.jpg" height="90px">' ;

                                                else

                                                    $img = '';

                                                ?>

                                                <tr>

                                                    <td class="text-center"><?php echo $rowTodos['id'];?></td>

                                                    <td class="text-center">

                                                        <img src="../public/img/imagenes/<?php echo $rowTodos['id'];?>.jpg" height="90px" data-toggle="modal" data-target="#modal_<?php echo $rowTodos['id'];?>" style="cursor:pointer">

                                                        

                                                        <div id="modal_<?php echo $rowTodos['id'];?>" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">

                                                            <div class="modal-dialog">

                                                                <div class="modal-content p-0 b-0">

                                                                    <div class="panel panel-color panel-primary">

                                                                        <div class="panel-heading"> 

                                                                            <button type="button" class="close m-t-5" data-dismiss="modal" aria-hidden="true">×</button> 

                                                                            <h3 class="panel-title">Imagen</h3> 

                                                                        </div> 

                                                                        <div class="panel-body"> 

                                                                            <img src="../public/img/imagenes/<?php echo $rowTodos['id'];?>.jpg<?php echo elRandom()?>" height="350px">

                                                                        </div> 

                                                                    </div>

                                                                </div>

                                                            </div>

                                                        </div>

                                                    </td>

                                                    

                                                    <td><?php echo $rowTodos['titulo'];     ?></td>

                                                    <td class="text-center">

                                                        <a class="btn btn-inverse waves-effect waves-light btn-xs" href="imagenes.php?id=<?php echo $rowTodos['id'];?>"><i class="ion-edit"></i> Editar</a>

                                                        

                                                    </td>

                                                </tr>

                                            <?php } ?>                                            

                                        </tbody>

                                    </table>

                                </div>

                            </div>

                        </div>

                    </div>

                    <footer class="footer">2016 © Creasotol</footer>

                </div>        

            </div>

        </div>

        <?php include("partials/footer.php"); ?>

    </body>

</html>



<?php 

if (isset($_REQUEST['btnGuardar'])) {

    $id = $_REQUEST['txtId'];



    if ($id == '') {

        $mensaje = "Se creo la imagen.";

        $campos  = "titulo,url";

        $valores = "'".$_REQUEST['txtTitulo']."','".$_REQUEST['url']."'";

        $id = crearDato("imagenes", $campos, $valores);



    }else{

        $mensaje = "Se modificaron los datos de la imagen.";

        $campos  = "titulo = '".$_REQUEST['txtTitulo']."',url= '".$_REQUEST['url']."'";

        actualizarDatos("imagenes", $campos, "id = ".$id);

    }



    //SE CAMBIA EL LOGO SI SE CARGO UNO NUEVO

    if($_FILES['txtArchivo']['name'] != ''){

        $logo = "../public/img/imagenes/".$id.".jpg";

        if(copy($_FILES['txtArchivo']['tmp_name'], $logo) ){ 

            $mensaje .= ' Se cargo la imagen. ';

        }else{

            $mensaje .= ' Se produjo un error al cargar la imagen. ';

        }

    }



    echo "<script> 

            $.Notification.notify('success','top left', 'Exito!', '".$mensaje."');

            setInterval(function(){ location.href = 'imagenes.php' }, 3000);

          </script>";

}







?>