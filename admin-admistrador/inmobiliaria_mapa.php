<?php
include("partials/funciones.php");
//Verificando si esta logueado
if (!isset($_SESSION['usuPA_admin'])) {
    header('Location: index.php');
}

if (isset($_REQUEST['id'])) {
    $dato = busquedaIndividual("inmobiliaria_inmuebles", "id = ".$_REQUEST['id']);
}
$todos = busquedasGenerales("inmobiliaria_inmuebles", "", "ORDER BY id ASC");
header('Content-Type: text/html; charset=utf-8');
?>
<!DOCTYPE html>
<html>
    <head><meta http-equiv="Content-Type" content="text/html; charset=gb18030">
        <?php include('partials/header.php');  ?>
        <style>
      /* Always set the map height explicitly to define the size of the div
       * element that contains the map. */
      #map {
        height: 60%;
        width:60%;
        display:block;
        text-align: center; 
        margin 10px auto;
      }
      /* Optional: Makes the sample page fill the window. */
      html, body {
        height: 100%;
        margin: 0;
        padding: 0;
        width:100%;
   
      }
     
      .row{
          
      
          mar
      }
      .su{
           margin-top: 30%;
      }
    </style>
        
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    </head>
    <body class="fixed-left">
        
        <div id="wrapper">
            <?php include('partials/menu_horizontal.php');  ?>
        
            <div class="content-page">
                <div class="content">
                    <div class="container">
   
                        <!-- Donde Estoy -->
                        <div class="row">
                            <div class="col-sm-12">
                                <h4 class="page-title">Ubicacion</h4>
                                <ol class="breadcrumb">
                                    <li><a href="inmobiliaria_inmueble.php">Inmuebles</a></li>
                                    <!-- <li><a href="inmobiliaria_seccion.php">Sección</a></li>
                                    <li><a href="inmobiliaria_categoria.php">Categorias</a></li> -->
                                    <li class="active">Ubicacion</li>
                                </ol>
                            </div>
                        </div>

                        <div class="row docs">
                            <div class="col-lg-12">
                                <div class="card-box">
                                    <form action="#" method="post" enctype="multipart/form-data" id="form4" class="form-horizontal">
                                        <input type="hidden" name="txtId"      id="txtId"      value="<?php if(isset($dato['id'])) echo $dato['id'];?>" />
                                        <input type="hidden" name="selSeccion" id="selSeccion" value="2" />
                                      
                                        <div class="form-group">
                                            <div class="col-lg-3">
                                                <label>Latitud:</label>
                                                <input type="text" class="form-control" name="lati" id="lati" required="required" readonly value="<?php if(isset($dato['lati'])) echo $dato['lati'];?>">
                                            </div>
                                            <div class="col-lg-3">
                                                <label>Longitud:</label>
                                                <input type="text" class="form-control" name="longi" id="longi" required="required" readonly value="<?php if(isset($dato['longi'])) echo $dato['longi'];?>">
                                            </div>
                                            <div class="col-lg-3">
                                                <label>Ubicacion:</label>
                                                 <select data-placeholder="Seleccione un departamento" name="selUbicacion" id="selUbicacion" class=" form-control chosen-select txt-tienda"  tabindex="2" onchange="ciu(this.value)" required>
            		<option value=""></option>
            		<?php 
               		 $central = busquedasGenerales("ciudades", " ", "ORDER BY nombre ASC");
                        	while ($rowCentral = mysqli_fetch_array($central)) {
                        	
                        	$bus= busquedaIndividual("departamento",  "id=".$rowCentral['idDepartamento']);
                        	
                        	
                        	  ?>
                        	 <option value="<?php echo $rowCentral['lati'].",".$rowCentral['longi'] ?>"><?php echo utf8_encode($rowCentral['nombre']."(".$bus['nombre'].")") ?></option>
               			 <?php }  

            			 ?>
          			</select>
                                            </div>
                                          
                                        </div>
                                      <br>
                                          <div id="mm"> <div id="map" style="position:absolute;" ></div> </div>
                                <br>
                               
        <script type="text/javascript">

		 	var map = null;
		 	var infoWindow = null;
		 	 function ciu(cor) {
		 	     
		 	    
		 	     var co=cor.split(",");
		 	     
		 	     var la=  parseFloat(co[0]);
		 	     var lo= parseFloat(co[1]);
		 	     
		 	     
		 	     var myLatlng = new google.maps.LatLng(la,lo);
		 	    var myOptions = {
		 	      zoom: 14,
		 	      center: myLatlng,
		 	      mapTypeId: google.maps.MapTypeId.ROADMAP
		 	    };
		 	 
		 	    map = new google.maps.Map(document.getElementById('map'), myOptions);
		 	
		 	    infoWindow = new google.maps.InfoWindow();
		 	 
		 	    var marker = new google.maps.Marker({
		 	        position: myLatlng,
		 	        draggable: true,
		 	        map: map,
		 	        title:"Ejemplo marcador arrastrable"
		 	    });
		 	               google.maps.event.addListener(marker, 'dragend', function(){ openInfoWindow(marker); });
		 	       	google.maps.event.addListener(marker, 'click', function(){ openInfoWindow(marker); });
		 	     
		 	 }
		 	function openInfoWindow(marker) {
		 	    var markerLatLng = marker.getPosition();
		 	    infoWindow.setContent([
		 	        '<strong>La posicion del marcador es:</strong><br/>',
		 	        markerLatLng.lat(),
		 	        ', ',
		 	        markerLatLng.lng(),
		 	        '<br/>Arrastrame para actualizar la posicion.'
		 	    ].join(''));
		 	    infoWindow.open(map, marker);
		 	    document.getElementById('lati').value= markerLatLng.lat() ;
		 	  	document.getElementById('longi').value= markerLatLng.lng() ;
		 	    
		 	}
		 	 
		 	function initMap() {
		 	    var myLatlng = new google.maps.LatLng(document.getElementById('lati').value ,document.getElementById('longi').value);
		 	    var myOptions = {
		 	      zoom: 14,
		 	      center: myLatlng,
		 	      mapTypeId: google.maps.MapTypeId.ROADMAP
		 	    };
		 	 
		 	    map = new google.maps.Map(document.getElementById('map'), myOptions);
		 	
		 	    infoWindow = new google.maps.InfoWindow();
		 	 
		 	    var marker = new google.maps.Marker({
		 	        position: myLatlng,
		 	        draggable: true,
		 	        map: map,
		 	        title:"Ejemplo marcador arrastrable"
		 	    });
		 	               google.maps.event.addListener(marker, 'dragend', function(){ openInfoWindow(marker); });
		 	       	google.maps.event.addListener(marker, 'click', function(){ openInfoWindow(marker); });
		 	}
		 	 
		 	$(document).ready(function() {
		 	    initMap();
		 	   
		 	});   
</script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA2b3YrlPVAmGm7ebHuYvMUTqoUELZFN0Q&callback=initMap"
    async defer></script> 

                                        
    
                                        <div class="form-group su">
                                            
                                            <div class="col-lg-12 text-center">
                                                <button class="btn btn-primary waves-effect waves-light" type="submit" name="btnGuardar" id="btnGuardar">Guardar</button>
                                               
                                            </div>
                                        </div>
                                    </form>
                                    
                                </div>
                                
                            </div>
                        </div>
                       
                        
                    <footer class="footer">2016 - Creasotol</footer>
                </div>        
            </div>
        </div>
        
        <?php include("partials/footer.php"); ?>
       
       
    </body>
</html>
<?php 
if (isset($_REQUEST['btnGuardar'])) {
    $id  = $_REQUEST['id'];
    $lati = $_REQUEST['lati'];
    $longi = $_REQUEST['longi'];
    

    


    if ($id == '') {
        $mensaje = "Se creo el inmueble.";
        $campos  = "id_seccion, id_categoria, id_tipo, codigo, titulo, detalle, valor, administracion, id_ubicacion, barrio, area, area2, habitaciones, banos, estrato, garage, mapa, estado, recomendado, antiguedad, fecha_registro";
        $valores = "'".$sec."', '".$cat."', '".$tip."',  '".$cod."', '".$tit."', '".$tex."', '".$val."', '".$vaA."', '".$ciu."', '".$bar."', '".$are."', '".$arC."', '".$hab."', '".$ban."', '".$est."', '".$gar."', '".$map."', '".$sta."', '".$rec."', '".$ant."', '".date("Y-m-d")."'";
        $id = crearDato("inmobiliaria_inmuebles", $campos, $valores);
    }else{
        $mensaje = "Se modificaron los datos del inmueble.";
        $campos  = "lati = '".$lati."', longi = '".$longi."'";
        actualizarDatos("inmobiliaria_inmuebles", $campos, "id = ".$id);
    }



 

    echo "<script> 
            $.Notification.notify('success','top left', 'Exito!', '".$mensaje."');
            setInterval(function(){ location.href = 'inmobiliaria_mapa.php?id=".$id."' }, 3000);
          </script>";
}
?>

 