<?php
include("partials/funciones.php");

//Verificando si esta logueado
if (!isset($_SESSION['usuPA_admin'])) {
    header('Location: index.php');
}

if (isset($_REQUEST['id'])) {
    $dato = busquedaIndividual("paginas", "id = ".$_REQUEST['id']);
}

$todos = busquedasGenerales("paginas", "", "ORDER BY id ASC");
?>
<!DOCTYPE html>
<html>
    <head>
        <?php include('partials/header.php');  ?>
    </head>
    <body class="fixed-left">
        <div id="wrapper">
            <?php include('partials/menu_horizontal.php'); ?>      
            <div class="content-page">
                <div class="content">
                    <div class="container">  
                        <!-- Donde Estoy -->
                        <div class="row">
                            <div class="col-sm-12">
                                <h4 class="page-title">Paginas</h4>
                                <ol class="breadcrumb">
                                    <li><a href="index2.php">Inicio</a></li>
                                    <li class="active">Pagina</li>
                                </ol>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-12">
                                <div class="card-box">
                                    <form action="#" method="post">
                                        <input type="hidden" name="txtId" id="txtId" value="<?php if(isset($dato['id'])) echo $dato['id'];?>" />
                                        <div class="form-group">
                                            <div class="col-lg-12">
                                                <label>Articulo:</label>
                                                <input type="text" class="form-control" name="txtTitulo" id="txtTitulo" required="required" placeholder="Quienes Somos" value="<?php if(isset($dato['titulo'])) echo $dato['titulo'];?>">
                                                <br> 
                                            </div>

                                            <div class="col-lg-12">
                                                <textarea name="txtDetalle" id="txtDetalle" class="form-control" rows="10" required="required"><?php if(isset($dato['detalle'])) echo $dato['detalle'];?></textarea>
                                            <br><br><br>
                                            </div>

                                            <?php if (isset($_REQUEST['id'])) { ?>
                                                <button class="btn btn-primary waves-effect waves-light" type="submit" name="btnGuardar" id="btnGuardar">Guardar</button>
                                            <?php }else{ echo "<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>"; } ?>
                                        </div>                                        
                                    </form>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-12">
                                <div class="card-box">
                                    <table id="datatable" class="table table-striped table-bordered">
                                        <thead>
                                            <tr>
                                                <th width="80px" class="text-center">ID</th>
                                                <th width="30%" class="text-center">Titulo</th>
                                                <th class="text-center">Detalle</th>
                                                <th width="160px" class="text-center">Acción</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php while ($rowTodos = mysqli_fetch_array($todos)){ ?>
                                                <tr>
                                                    <td class="text-center"><?php echo $rowTodos['id'];?></td>
                                                    <td class="text-center"><?php echo $rowTodos['titulo'];?></td>
                                                    <td><?php echo substr(strip_tags($rowTodos['detalle']), 0, 80);?>...</td>
                                                    <td class="text-center">
                                                        <a class="btn btn-inverse waves-effect waves-light btn-xs" href="paginas.php?id=<?php echo $rowTodos['id'];?>"><i class="ion-edit"></i> Editar</a>
                                                    </td>
                                                </tr>
                                            <?php } ?>                                            
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <footer class="footer">2016 © Creasotol</footer>
                </div>        
            </div>
        </div>
        <?php include("partials/footer.php"); ?>
        <script type="text/javascript">
            $(document).ready(function () {
                $("#txtDetalle").htmlarea();
            });
        </script>
    </body>
</html>

<?php 
if (isset($_REQUEST['btnGuardar'])) {
    $id      = $_REQUEST['txtId'];
    $mensaje = "Se modificaron los datos de la página.";
    $campos  = "titulo = '".$_REQUEST['txtTitulo']."', detalle = '".$_REQUEST['txtDetalle']."'";
    actualizarDatos("paginas", $campos, "id = ".$id);

    echo "<script> 
            $.Notification.notify('success','top left', 'Exito!', '".$mensaje."');
            setInterval(function(){ location.href = 'paginas.php' }, 3000);
          </script>";
} ?>