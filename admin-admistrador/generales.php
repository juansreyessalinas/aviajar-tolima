i<?php
include("partials/funciones.php");

//Verificando si esta logueado
if (!isset($_SESSION['usuPA_admin'])) {
    header('Location: index.php');
}
$dato = busquedaIndividual("configuracion", "id = 1");
?>
<!DOCTYPE html>
<html>
    <head>
        <?php include('partials/header.php');  ?>
    </head>
    <body class="fixed-left">
        <div id="wrapper">
            <?php include('partials/menu_horizontal.php');  ?>
        
            <div class="content-page">
                <div class="content">
                    <div class="container">
    
                        <!-- Donde Estoy -->
                        <div class="row">
                            <div class="col-sm-12">
                                <h4 class="page-title">Datos Generales</h4>
                                <ol class="breadcrumb">
                                    <li><a href="index2.php">Inicio</a></li>
                                    <li class="active">Generales</li>
                                </ol>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-12">
                                <div class="card-box">
                                    <form action="#" method="post">   

                                        <div class="form-group">
                                            <div class="col-lg-6">
                                                <label><br>Dirección  :</label>
                                                <input type="text" class="form-control" name="txtDireccion" id="txtDireccion" placeholder="Calle 36 # 6-52" value="<?php echo $dato['direccion'];?>">
                                            </div>
                                          
                                        </div>

                                        <div class="form-group">
                                            <div class="col-lg-6">
                                                <label><br>Teléfonos :</label>
                                                <input type="text" class="form-control" name="txtTelefono" id="txtTelefono" placeholder="314 4880208" value="<?php echo $dato['telefonos'];?>">
                                            </div>
                                            
                                        </div>
                                        
                                        <!--
                                        <div class="form-group">
                                            <div class="col-lg-6">
                                                <label><br>Teléfonos Sede Neiva:</label>
                                                <input type="text" class="form-control" name="txtTelefono2" id="txtTelefono2" placeholder="314 4880208" value="<?php echo $dato['telefonos2'];?>">
                                            </div>
                                            
                                        </div>
-->
                                        <div class="form-group">
                                            
                                            <div class="col-lg-6">
                                                <label><br>Facebook</label>
                                                <input type="text" class="form-control" name="txtFacebook" id="txtFacebook" placeholder="www.facebook.com/creasotol" value="<?php echo $dato['facebook'];?>">
                                            </div>  
                                            <div class="col-lg-6">
                                                <label><br>Twitter</label>
                                                <input type="text" class="form-control" name="txtTwitter" id="txtTwitter" placeholder="www.twitter.com/creasotol" value="<?php echo $dato['twitter'];?>">
                                            </div>               
                                        </div>

                                        <div class="form-group">
                                            <div class="col-lg-6">
                                                <label><br>Google +</label>
                                                <input type="text" class="form-control" name="txtGoogle" id="txtGoogle" placeholder="plus.google.com/+Creasotoldiseñoweb/" value="<?php echo $dato['google'];?>">
                                            </div>
                                            <div class="col-lg-6">
                                                <label><br>Intagram:</label>
                                                <input type="text" class="form-control" name="txtInstagram" id="txtInstagram" placeholder="www.youtube.com/creasotol" value="<?php echo $dato['instagram'];?>">
                                            </div>    
                                            <div class="col-lg-6">
                                                <label><br>Youtube:</label>
                                                <input type="text" class="form-control" name="txtYoutube" id="txtYoutube" placeholder="www.youtube.com/creasotol" value="<?php echo $dato['youtube'];?>">
                                            </div>    
                                        </div>
					 
                                        <div class="form-group">
                                            
                                            <!--
                                            <div class="col-lg-6">
                                                <label><br>Mapa 2:</label>    
                                                <input type="text" class="form-control" name="txtMapa2" id="txtMapa2" value="<?php echo $dato['mapa2'];?>" />
                                            </div>  -->                                 
                                        </div>

                                        <div class="form-group">
                                            <div class="col-lg-6">
                                                <label><br>Email:</label>
                                                <input type="text" class="form-control" name="txtEmail" id="txtEmail" placeholder="a@a.com" value="<?php echo $dato['email'];?>">
                                            </div>
                                            
                                            <div class="col-lg-6">
                                                <label><br>Mapa :</label>
                                               <a href="mapa1.php">Click Aqui</a>
                                            </div>
                                             
                                        <!--
                                            <div class="col-lg-6">
                                                <label><br>Precio Envio</label>
                                                <input type="number" class="form-control" name="txtPrecioEnv" id="txtPrecioEnv" placeholder="25000" value="<?php echo $dato['precio'];?>">
                                            </div>
                                                 -->                                 
                                        </div>

                                        <!--
                                        -->

                                        <!--
                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                <label><br>Horario de Atención:</label>
                                                <textarea name="txtHorario" id="txtHorario" class="form-control"><?php echo $dato['horario'];?></textarea><br>
                                            </div>
                                        </div> 

                                        <div class="form-group">
                                            <div class="col-lg-6">
                                                <br><label>Imagen Fondo</label>                                            
                                                <input type="file" name="txtArchivo" id="txtArchivo" class="filestyle" data-buttontext="Seleccione" data-buttonname="btn-inverse" <?php if(!isset($dato['id'])) echo 'required';?> onchange="verificarjpg(this.value, 'txtArchivo')">                                              
                                                <br>
                                            </div>
                                           
                                        </div>

                                       <div class="form-group">
                                            <div class="col-lg-6">
                                                <label>WhatsApp</label>                                            
                                                <input type="text" class="form-control" name="txtWhatsapp" id="txtWhatsapp" placeholder="314 4880208" value="<?php echo $dato['whatsapp'];?>">
                                                <br>
                                            </div>
                                            <div class="col-lg-6"><br><br><br><br></div>
                                        </div>-->
                                        <!-- <div class="form-group">
                                            <div class="col-lg-6">
                                                <br>
                                                <label>Feed</label>                                            
                                                <input type="text" class="form-control" name="txtFeed" id="txtFeed" value="<?php echo $dato['feed'];?>">
                                            </div>                                      
                                            <div class="col-lg-6">
                                                <br>
                                                <label>Chat</label>
                                                <input type="text" class="form-control" name="txtChat" id="txtChat" placeholder="www.youtube.com/creasotol" value="<?php echo $dato['chat'];?>">
                                            </div>
                                        </div>-->
                                        <br><br>
                                         <br><br>
                                       
                                        <div class="form-group text-center">
                                            <button class="btn btn-primary waves-effect waves-light" type="submit" name="btnGuardar" id="btnGuardar">Guardar Datos</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <footer class="footer">2016 © Creasotol</footer>
                </div>        
            </div>
        </div>
        <?php include("partials/footer.php"); ?>        
    </body>
</html>
<?php 
if (isset($_REQUEST['btnGuardar'])) {
    $campos  = " horario = '".$_REQUEST['txtHorario']."', telefonos = '".$_REQUEST['txtTelefono']."', telefonos2 = '".$_REQUEST['txtTelefono2']."', email = '".$_REQUEST['txtEmail']."', 
                 direccion = '".$_REQUEST['txtDireccion']."', direccion2 = '".$_REQUEST['txtDireccion2']."', mapa1 = '".$_REQUEST['txtMapa1']."', mapa2 = '".$_REQUEST['txtMapa2']."', 
                 facebook = '".$_REQUEST['txtFacebook']."', twitter = '".$_REQUEST['txtTwitter']."', instagram = '".$_REQUEST['txtInstagram']."', google = '".$_REQUEST['txtGoogle']."', 
                 whatsapp = '".$_REQUEST['txtWhatsapp']."', youtube = '".$_REQUEST['txtYoutube']."', feed = '".$_REQUEST['txtFeed']."', chat = '".$_REQUEST['txtChat']."', 
                 precio = '".$_REQUEST['txtPrecioEnv']."' ";
    actualizarDatos("configuracion", $campos, "id = 1");

    //SE CAMBIA EL LOGO SI SE CARGO UNO NUEVO
    if($_FILES['txtArchivo']['name'] != ''){
        $logo = "../public/img/fondo-inicio.jpg";
        if(copy($_FILES['txtArchivo']['tmp_name'], $logo) ){ 
            $mensaje = ' Se cargo la imagen. ';
        }else{
            $mensaje = ' Se produjo un error al cargar la imagen. ';
        }
    }

    echo "<script> 
            $.Notification.notify('success','top left', 'Exito!', 'Se modificaron los datos de la configuración general. ".$mensaje."');
            setInterval(function(){ location.href = 'generales.php' }, 3000);
          </script>";
} ?>