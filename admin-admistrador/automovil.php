<?php
include("partials/funciones.php");
//Verificando si esta logueado
if (!isset($_SESSION['usuPA_admin'])) {
    header('Location: index.php');
}

if (isset($_REQUEST['id'])) {
    $dato = busquedaIndividual("auto", "id = ".$_REQUEST['id']);
}
$todos = busquedasGenerales("auto", "", "ORDER BY id DESC");
?>
<!DOCTYPE html>
<html>
    <head>
        <?php include('partials/header.php');  ?>
        
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    </head>
    <body class="fixed-left">
        <div id="wrapper">
            <?php include('partials/menu_horizontal.php');  ?>
        
            <div class="content-page">
                <div class="content">
                    <div class="container">
   
                        <!-- Donde Estoy -->
                        <div class="row">
                            <div class="col-sm-12">
                                <h4 class="page-title">Autos</h4>
                                <ol class="breadcrumb">
                                    <li><a href="index2.php">Inicio</a></li>
                                    <!-- <li><a href="inmobiliaria_seccion.php">Sección</a></li>
                                    <li><a href="inmobiliaria_categoria.php">Categorias</a></li> -->
                                    <li class="active">Autos</li>
                                </ol>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-12">
                                <div class="card-box">
                                    <form action="#" method="post" enctype="multipart/form-data"  class="form-horizontal">
                                        <input type="hidden" name="txtId"      id="txtId"      value="<?php if(isset($dato['id'])) echo $dato['id'];?>" />
                                        <input type="hidden" name="selSeccion" id="selSeccion" value="2" />
                                      
                                        <div class="form-group">
                                            <div class="col-lg-6">
                                                <label>Titulo:</label>
                                                <input type="text" class="form-control" name="txtTitulo" id="txtTitulo" required="required" placeholder="Auto" value="<?php if(isset($dato['titulo'])) echo $dato['titulo'];?>">
                                            </div>
                                            <div class="col-lg-3">
                                                <label>Imágen de Muestra:</label>
                                                <input type="file" name="txtArchivo" id="txtArchivo" class="filestyle" data-buttontext="Seleccione" data-buttonname="btn-inverse"  >
                                            </div>
                                          
                                           
                                        </div>
                                        <div class="form-group">
                                            <div class="col-lg-2">
                                                <label>marca:</label>
                                                <select class="selectpicker" data-style="btn-white" name="marca" id="marca" required="required">
                                                    <?php optOrder($dato['marca'], "marca_auto", "", "nombre", "", "ORDER BY nombre ASC"); ?>
                                                </select>
                                            </div>
                                           

                                            <div class="col-lg-2">
                                                <label>Modelo:</label>
                                                <input type="text" class="form-control" name="modelo" id="modelo" placeholder="2" value="<?php if(isset($dato['modelo'])) echo $dato['modelo'];?>" onKeyPress="return soloNumeros(event)">
                                            </div>
                                            
                                            <div class="col-lg-3">
                                                <label>Valor:</label>
                                                <input type="number" class="form-control" name="txtValor" id="txtValor" required="required" placeholder="350000" value="<?php if(isset($dato['valor'])) echo $dato['valor'];?>" onKeyPress="return soloNumeros(event)">
                                            </div>

                                            <div class="col-lg-3">
                                                <label>Estado:</label>
                                                <div class="radio radio-primary">
                                                    <input type="radio" name="rdoActivo" id="rdoActivo" required value="1"<?php if(isset($dato['estado']) && $dato['estado'] == 1) echo "checked";?>>
                                                    <label for="radio3">Activo</label> &nbsp; &nbsp; &nbsp; &nbsp;                                                  
                                                    <input type="radio" name="rdoActivo" id="rdoActivo" required value="2"<?php if(isset($dato['estado']) && $dato['estado'] == 2) echo "checked";?>>
                                                    <label for="radio3">Inactivo</label>
                                                </div>                                             
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-lg-2">
                                                <label>Kilometraje:</label>
                                                <input type="text" class="form-control" name="kilometraje" id="kilometraje" placeholder="300 mt2" value="<?php if(isset($dato['kilometraje'])) echo $dato['kilometraje'];?>" onKeyPress="return soloNumeros(event)">
                                            </div>
                                          
                                            <!-- <div class="col-lg-2">
                                                <label>Código de Búsqueda:</label>
                                                <input type="text" class="form-control" name="txtCodigo" id="txtCodigo" required="required" placeholder="ABC01" value="<?php if(isset($dato['codigo'])) echo $dato['codigo'];?>">
                                            </div> -->

                                            


                                            <div class="col-lg-3">
                                                <label>Ubicación:</label>
                                                 <select data-placeholder="Seleccione un departamento" name="selUbicacion" id="selUbicacion" class=" form-control chosen-select txt-tienda"  tabindex="2" onchange="ciu()" required>
            		<option value=""></option>
            		<?php 
               		 $central = busquedasGenerales("ciudades", " ", "ORDER BY nombre ASC");
                        	while ($rowCentral = mysqli_fetch_array($central)) {
                        	
                        	$bus= busquedaIndividual("departamento",  "id=".$rowCentral['idDepartamento']);
                        	
                        	
                        	  
                        	  if($dato['ciudad']==$rowCentral['id']){
                        	  ?>
                        	     <option value="<?php echo $rowCentral['id'] ?>" selected><?php echo utf8_encode($rowCentral['nombre']."(".$bus['nombre'].")") ?></option>
                        	  <?php
                        	  } else{
                        	      ?>
                        	   <option value="<?php echo $rowCentral['id'] ?>"><?php echo utf8_encode($rowCentral['nombre']."(".$bus['nombre'].")") ?></option>
                        	 
                        	  
                        	
               			 <?php }  }  

            			 ?>
          			</select>
                                            </div>
                                            
                                         <div class="form-group">
                                            <div class="col-lg-2">
                                                <label>Cilindraje:</label>
                                                <input type="number" class="form-control" name="cilindraje" id="cilindraje"  placeholder="2" value="<?php if(isset($dato['cilindraje'])) echo $dato['cilindraje'];?>" onKeyPress="return soloNumeros(event)">
                                            </div>
                                            
                                                                                    
                                        </div>
                                
                                            
                                                                                        
                                        </div>

                                        
    
    
                                        
                                        <div class="form-group">
                                            <div class="col-lg-12">
                                                <textarea name="txtDetalle" id="txtDetalle" class="form-control" rows="15"><?php if(isset($dato['detalle'])) echo $dato['detalle'];?></textarea>
                                            </div>
                                            <div class="col-lg-12 text-center">
                                                <button class="btn btn-primary waves-effect waves-light" type="submit" name="btnGuardar" id="btnGuardar">Guardar</button>
                                                <a href="inmobiliaria_inmueble.php" class="btn btn-default waves-effect waves-light m-l-5">Cancel</a>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                       

                        <div class="row">
                            <div class="col-sm-12">
                                <div class="card-box">
                                    <table id="datatable" class="table table-striped table-bordered">
                                        <thead>
                                            <tr>
                                                <th width="50px" class="text-center">CODIGO</th>
                                              
                                                <th width="150px" class="text-center">Marca</th>
                                                <th class="text-center">Titulo</th>
                                            
                                                <th width="80px" class="text-center">Estado</th>
                                                <th  class="text-center">Acción</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php while ($rowTodos = mysqli_fetch_array($todos)){ 
                                                if ($rowTodos['estado'] == 1)
                                                    $elEstado = '<span class="label label-table label-success" onclick="inactivar('."'inmobiliaria_inmuebles'".', '."'".$rowTodos['id']."'".', 2, '."'inmobiliaria_inmueble.php'".')" style="cursor:pointer">Activo</span>';
                                                else
                                                    $elEstado = '<span class="label label-table label-danger"  onclick="inactivar('."'inmobiliaria_inmuebles'".', '."'".$rowTodos['id']."'".', 1, '."'inmobiliaria_inmueble.php'".')" style="cursor:pointer">Inactivo</span>';                                               

                                                if ($rowTodos['recomendado'] == 1)
                                                    $elRecomendado = "Si";
                                                else
                                                    $elRecomendado = "No";

                                                ?>
                                                <tr>   
                                                    <!--<td class="text-center">?php echo $rowTodos['id'];?></td>-->
                                                    <td class="text-center"><?php echo $rowTodos['id'];?></td>
                                                   
                                                    <td><?php echo busquedaEspecifica("nombre", "nombre", "marca_auto", "id = ".$rowTodos['marca']);?></td>
                                                    <td>
                                                        <ins class="text-primary" style="cursor:pointer;" data-toggle="modal" data-target="#modal_<?php echo $rowTodos['id'];?>"><?php echo $rowTodos['titulo'];?></ins>
                                                        <div id="modal_<?php echo $rowTodos['id'];?>" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                                                            <div class="modal-dialog">
                                                                <div class="modal-content p-0 b-0">
                                                                    <div class="panel panel-color panel-primary">
                                                                        <div class="panel-heading"> 
                                                                            <button type="button" class="close m-t-5" data-dismiss="modal" aria-hidden="true">×</button> 
                                                                            <h3 class="panel-title">Imagen Principal</h3> 
                                                                        </div> 
                                                                        <div class="panel-body"> 
                                                                            <img src="../public/img/auto/<?php echo $rowTodos['id'];?>/principal.jpg" style="max-width: 300px; max-height: 350px">
                                                                        </div> 
                                                                    </div>
                                                                </div>
                                                           </div>
                                                        </div>
                                                    </td>
                                                   
                                                    <td class="text-center"><?php echo $elEstado;?></td>
                                                    <td class="text-center">
                                                        
                                                        <a class="btn btn-inverse waves-effect waves-light btn-xs" href="inmobiliaria_galeria_auto.php?id=<?php echo $rowTodos['id'];?>"><i class="md-collections"></i> Galeria</a>
                                                        <a class="btn btn-inverse waves-effect waves-light btn-xs" href="automovil.php?id=<?php echo $rowTodos['id'];?>"><i class="ion-edit"></i> Editar</a>
                                                        <a class="btn btn-inverse waves-effect waves-light btn-xs" onclick="eliminar('auto', <?php echo $rowTodos['id'];?>, 'automovil.php')"><i class="glyphicon glyphicon-trash"></i> Eliminar</a>
                                                    </td>
                                                </tr>
                                            <?php } ?>                                            
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <footer class="footer">2016 © Creasotol</footer>
                </div>        
            </div>
        </div>
        <?php include("partials/footer.php"); ?>
        <script type="text/javascript">
            $('.selectpicker').selectpicker();
        </script>
        <script type="text/javascript">
            $(document).ready(function () {
                $("#txtDetalle").htmlarea();
            });
            function eliminarinmueble(tabla, id, pagina,tipo){
  if (confirm("Esta seguro que desea eliminar el dato?")) {
    $.ajax({
      method: "POST",
      url: "partials/metodos.php",
      data: { accion: "eliminari", laTab: tabla, elId: id ,tipo:tipo}
    })
    .done(function(msg) {
     
      setInterval(function(){ location.href = pagina }, 2000);
    });
  }
}
        </script>
        
          <script type="text/javascript">
// Solo permite ingresar numeros.
function soloNumeros(e){
	var key = window.Event ? e.which : e.keyCode
	return (key >= 48 && key <= 57 ||key == 8   )
}
</script>
    </body>
</html>
<?php 
if (isset($_REQUEST['btnGuardar'])) {
   
$id  = $_REQUEST['txtId'];

    if ($id == '') {
        $mensaje = "Se creo el automovil.";
        $campos  = "marca, valor, modelo, kilometraje, ciudad, cilindraje, titulo,  estado, detalle";
        $valores = "'".$_REQUEST['marca']."', '".$_REQUEST['txtValor']."', '".$_REQUEST['modelo']."',  '".$_REQUEST['kilometraje']."', '".$_REQUEST['selUbicacion']."', '".$_REQUEST['cilindraje']."', '".$_REQUEST['txtTitulo']."',  '".$_REQUEST['rdoActivo']."', '".$_REQUEST['txtDetalle']."'";
        $id = crearDato("auto", $campos, $valores);
    }else{
        $mensaje = "Se modificaron los datos del automovil.";
        $campos  = "marca = '".$_REQUEST['marca']."', valor = '".$_REQUEST['txtValor']."', modelo = '".$_REQUEST['modelo']."', kilometraje = '".$_REQUEST['kilometraje']."', ciudad = '".$_REQUEST['selUbicacion']."', cilindraje = '".$_REQUEST['cilindraje']."',  titulo = '".$_REQUEST['txtTitulo']."', estado = '".$_REQUEST['rdoActivo']."', detalle = '".$_REQUEST['txtDetalle']."'";
        actualizarDatos("auto", $campos, "id = ".$id);
    }

    $carpeta = "../public/img/auto/".$id."/";

    if(!file_exists($carpeta)){
        mkdir($carpeta, 0777);
        chmod($carpeta, 0777);
    }

    //SE CAMBIA EL LOGO SI SE CARGO UNO NUEVO
    if($_FILES['txtArchivo']['name'] != ''){
         $logo = "../public/img/auto/".$id."";
        
        subir($logo,427,342, $_FILES['txtArchivo']['tmp_name'],'principal.jpg');
    }

    echo "<script> 
            $.Notification.notify('success','top left', 'Exito!', '".$mensaje."');
            setInterval(function(){ location.href = 'automovil.php' }, 3000);
          </script>";
}
?>

 