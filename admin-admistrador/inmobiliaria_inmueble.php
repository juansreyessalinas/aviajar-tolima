<?php
include("partials/funciones.php");
//Verificando si esta logueado
if (!isset($_SESSION['usuPA_admin'])) {
    header('Location: index.php');
}

if (isset($_REQUEST['id'])) {
    $dato = busquedaIndividual("inmobiliaria_inmuebles", "id = ".$_REQUEST['id']);
}
$todos = busquedasGenerales("inmobiliaria_inmuebles", "", "ORDER BY id DESC");
?>
<!DOCTYPE html>
<html>
    <head>
        <?php include('partials/header.php');  ?>
        
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    </head>
    <body class="fixed-left">
        <div id="wrapper">
            <?php include('partials/menu_horizontal.php');  ?>
        
            <div class="content-page">
                <div class="content">
                    <div class="container">
   
                        <!-- Donde Estoy -->
                        <div class="row">
                            <div class="col-sm-12">
                                <h4 class="page-title">Inmuebles</h4>
                                <ol class="breadcrumb">
                                    <li><a href="index2.php">Inicio</a></li>
                                    <!-- <li><a href="inmobiliaria_seccion.php">Sección</a></li>
                                    <li><a href="inmobiliaria_categoria.php">Categorias</a></li> -->
                                    <li class="active">Inmuebles</li>
                                </ol>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-12">
                                <div class="card-box">
                                    <form action="#" method="post" enctype="multipart/form-data"  class="form-horizontal">
                                        <input type="hidden" name="txtId"      id="txtId"      value="<?php if(isset($dato['id'])) echo $dato['id'];?>" />
                                        <input type="hidden" name="selSeccion" id="selSeccion" value="2" />
                                      
                                        <div class="form-group">
                                            <div class="col-lg-6">
                                                <label>Articulo:</label>
                                                <input type="text" class="form-control" name="txtTitulo" id="txtTitulo" required="required" placeholder="Inmueble en Venta" value="<?php if(isset($dato['titulo'])) echo $dato['titulo'];?>">
                                            </div>
                                            <div class="col-lg-3">
                                                <label>Imágen de Muestra:</label>
                                                <input type="file" name="txtArchivo" id="txtArchivo" class="filestyle" data-buttontext="Seleccione" data-buttonname="btn-inverse"  >
                                            </div>
                                            <div class="col-lg-3">
                                                <label>Recomendado:</label>
                                                <div class="radio radio-primary">
                                                    <input type="radio" name="rdoRecomendado" id="rdoRecomendado" required value="1"<?php if(isset($dato['recomendado']) && $dato['recomendado'] == 1) echo "checked";?>>
                                                    <label for="radio3">Si</label> &nbsp; &nbsp; &nbsp; &nbsp;                                                  
                                                    <input type="radio" name="rdoRecomendado" id="rdoRecomendado" required value="2"<?php if(isset($dato['recomendado']) && $dato['recomendado'] == 2) echo "checked";?>>
                                                    <label for="radio3">No</label>
                                                </div>                                             
                                            </div>
                                            <div class="col-lg-3">
                                                <label>Turismo:</label>
                                                <div class="radio radio-primary">
                                                    <input type="radio" name="turismo" id="turismo"  value="1"<?php if(isset($dato['turismo']) && $dato['turismo'] == 1) echo "checked";?>>
                                                    <label for="radio3">Si</label> &nbsp; &nbsp; &nbsp; &nbsp;                                                  
                                                    <input type="radio" name="turismo" id="turismo"  value="2"<?php if(isset($dato['turismo']) && $dato['turismo'] == 0) echo "checked";?>>
                                                    <label for="radio3">No</label>
                                                </div>                                             
                                            </div>
                                            <div class="col-lg-3">
                                                <label>Vacaciones:</label>
                                                <div class="radio radio-primary">
                                                    <input type="radio" name="proyecto" id="proyecto" required value="1"<?php if(isset($dato['proyecto']) && $dato['proyecto'] == 1) echo "checked";?>>
                                                    <label for="radio3">Si</label> &nbsp; &nbsp; &nbsp; &nbsp;                                                  
                                                    <input type="radio" name="proyecto" id="proyecto" required value="0"<?php if(isset($dato['proyecto']) && $dato['proyecto'] == 0) echo "checked";?>>
                                                    <label for="radio3">No</label>
                                                </div>                                             
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-lg-2">
                                                <label>Categoria:</label>
                                                <select class="selectpicker" data-style="btn-white" name="selCategoria" id="selCategoria" required="required">
                                                    <?php optOrder($dato['id_categoria'], "inmobiliaria_categoria", "WHERE estado = 1 AND id_seccion = 2", "titulo", "", "ORDER BY titulo ASC"); ?>
                                                </select>
                                            </div>
                                            <div class="col-lg-2">
                                                <label>Tipo Inmueble:</label>
                                                <select class="selectpicker" data-style="btn-white" name="selTipo" id="selTipo" required="required">
                                                    <?php optOrder($dato['id_tipo'], "inmobiliaria_tipo_inmueble", "", "titulo", "", "ORDER BY titulo ASC"); ?>
                                                </select>
                                            </div>

                                            <div class="col-lg-2">
                                                <label>Antiguedad:</label>
                                                <input type="text" class="form-control" name="txtAntiguedad" id="txtAntiguedad" placeholder="2" value="<?php if(isset($dato['antiguo'])) echo $dato['antiguo'];?>" onKeyPress="return soloNumeros(event)">
                                            </div>
                                            
                                            <div class="col-lg-3">
                                                <label>Valor:</label>
                                                <input type="number" class="form-control" name="txtValor" id="txtValor" required="required" placeholder="350000" value="<?php if(isset($dato['valor'])) echo $dato['valor'];?>" onKeyPress="return soloNumeros(event)">
                                            </div>

                                            <div class="col-lg-3">
                                                <label>Estado:</label>
                                                <div class="radio radio-primary">
                                                    <input type="radio" name="rdoActivo" id="rdoActivo" required value="1"<?php if(isset($dato['estado']) && $dato['estado'] == 1) echo "checked";?>>
                                                    <label for="radio3">Activo</label> &nbsp; &nbsp; &nbsp; &nbsp;                                                  
                                                    <input type="radio" name="rdoActivo" id="rdoActivo" required value="2"<?php if(isset($dato['estado']) && $dato['estado'] == 2) echo "checked";?>>
                                                    <label for="radio3">Inactivo</label>
                                                </div>                                             
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-lg-2">
                                                <label>Área:</label>
                                                <input type="text" class="form-control" name="txtArea" id="txtArea" placeholder="300 mt2" value="<?php if(isset($dato['area'])) echo $dato['area'];?>" onKeyPress="return soloNumeros(event)">
                                            </div>
                                            <div class="col-lg-2">
                                                <label>Área Construida:</label>
                                                <input type="text" class="form-control" name="txtAreaC" id="txtAreaC"  placeholder="300 mt2" value="<?php if(isset($dato['area_con'])) echo $dato['area_con'];?>" onKeyPress="return soloNumeros(event)">
                                            </div>
                                            <!-- <div class="col-lg-2">
                                                <label>Código de Búsqueda:</label>
                                                <input type="text" class="form-control" name="txtCodigo" id="txtCodigo" required="required" placeholder="ABC01" value="<?php if(isset($dato['codigo'])) echo $dato['codigo'];?>">
                                            </div> -->

                                            


                                            <div class="col-lg-3">
                                                <label>Ubicación:</label>
                                                 <select data-placeholder="Seleccione un departamento" name="selUbicacion" id="selUbicacion" class=" form-control chosen-select txt-tienda"  tabindex="2" onchange="ciu()" required>
            		<option value=""></option>
            		<?php 
               		 $central = busquedasGenerales("ciudades", " ", "ORDER BY nombre ASC");
                        	while ($rowCentral = mysqli_fetch_array($central)) {
                        	
                        	$bus= busquedaIndividual("departamento",  "id=".$rowCentral['idDepartamento']);
                        	
                        	
                        	  
                        	  if($dato['ciudad']==$rowCentral['id']){
                        	  ?>
                        	     <option value="<?php echo $rowCentral['id'] ?>" selected><?php echo utf8_encode($rowCentral['nombre']."(".$bus['nombre'].")") ?></option>
                        	  <?php
                        	  } else{
                        	      ?>
                        	   <option value="<?php echo $rowCentral['id'] ?>"><?php echo utf8_encode($rowCentral['nombre']."(".$bus['nombre'].")") ?></option>
                        	 
                        	  
                        	
               			 <?php }  }  

            			 ?>
          			</select>
                                            </div>
                                            
                                         
                                
                                            
                                                                                        
                                        </div>

                                        <div class="form-group">
                                            <div class="col-lg-2">
                                                <label>Habitaciones:</label>
                                                <input type="number" class="form-control" name="txtHabitaciones" id="txtHabitaciones"  placeholder="2" value="<?php if(isset($dato['habitaciones'])) echo $dato['habitaciones'];?>" onKeyPress="return soloNumeros(event)">
                                            </div>
                                            <div class="col-lg-2">
                                                <label>Baños:</label>
                                                <input type="number" class="form-control" name="txtBanos" id="txtBanos" placeholder="2" value="<?php if(isset($dato['banos'])) echo $dato['banos'];?>" onKeyPress="return soloNumeros(event)">
                                            </div>
                                            <div class="col-lg-2">
                                                <label>Estrato:</label>
                                                <input type="number" class="form-control" name="txtEstrato" id="txtEstrato"  placeholder="3" value="<?php if(isset($dato['estrato'])) echo $dato['estrato'];?>" onKeyPress="return soloNumeros(event)">
                                            </div>
                                            <div class="col-lg-3">
                                                <label>Garage:</label>
                                                <input type="text" class="form-control" name="txtGarage" id="txtGarage"  placeholder="1 Externo" value="<?php if(isset($dato['garage'])) echo $dato['garage'];?>" onKeyPress="return soloNumeros(event)">
                                            </div> 
                                                                                    
                                        </div>
    
    
                                        
                                        <div class="form-group">
                                            <div class="col-lg-12">
                                                <textarea name="txtDetalle" id="txtDetalle" class="form-control" rows="15"><?php if(isset($dato['detalle'])) echo $dato['detalle'];?></textarea>
                                            </div>
                                            <div class="col-lg-12 text-center">
                                                <button class="btn btn-primary waves-effect waves-light" type="submit" name="btnGuardar" id="btnGuardar">Guardar</button>
                                                <a href="inmobiliaria_inmueble.php" class="btn btn-default waves-effect waves-light m-l-5">Cancel</a>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                       

                        <div class="row">
                            <div class="col-sm-12">
                                <div class="card-box">
                                    <table id="datatable" class="table table-striped table-bordered">
                                        <thead>
                                            <tr>
                                                <th width="50px" class="text-center">CODIGO</th>
                                              
                                                <th width="150px" class="text-center">Categoria</th>
                                                <th class="text-center">Inmuebles</th>
                                                <th width="100px" class="text-center">Recomendado</th>
                                                <th width="80px" class="text-center">Estado</th>
                                                <th  class="text-center">Acción</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php while ($rowTodos = mysqli_fetch_array($todos)){ 
                                                if ($rowTodos['estado'] == 1)
                                                    $elEstado = '<span class="label label-table label-success" onclick="inactivar('."'inmobiliaria_inmuebles'".', '."'".$rowTodos['id']."'".', 2, '."'inmobiliaria_inmueble.php'".')" style="cursor:pointer">Activo</span>';
                                                else
                                                    $elEstado = '<span class="label label-table label-danger"  onclick="inactivar('."'inmobiliaria_inmuebles'".', '."'".$rowTodos['id']."'".', 1, '."'inmobiliaria_inmueble.php'".')" style="cursor:pointer">Inactivo</span>';                                               

                                                if ($rowTodos['recomendado'] == 1)
                                                    $elRecomendado = "Si";
                                                else
                                                    $elRecomendado = "No";

                                                ?>
                                                <tr>   
                                                    <!--<td class="text-center">?php echo $rowTodos['id'];?></td>-->
                                                    <td class="text-center"><?php echo $rowTodos['id'];?></td>
                                                   
                                                    <td><?php echo busquedaEspecifica("titulo", "titulo", "inmobiliaria_categoria", "id = ".$rowTodos['id_categoria']);?></td>
                                                    <td>
                                                        <ins class="text-primary" style="cursor:pointer;" data-toggle="modal" data-target="#modal_<?php echo $rowTodos['id'];?>"><?php echo $rowTodos['titulo'];?></ins>
                                                        <div id="modal_<?php echo $rowTodos['id'];?>" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                                                            <div class="modal-dialog">
                                                                <div class="modal-content p-0 b-0">
                                                                    <div class="panel panel-color panel-primary">
                                                                        <div class="panel-heading"> 
                                                                            <button type="button" class="close m-t-5" data-dismiss="modal" aria-hidden="true">×</button> 
                                                                            <h3 class="panel-title">Imagen Principal</h3> 
                                                                        </div> 
                                                                        <div class="panel-body"> 
                                                                            <img src="../public/img/inmuebles/<?php echo $rowTodos['id'];?>/principal.jpg" style="max-width: 300px; max-height: 350px">
                                                                        </div> 
                                                                    </div>
                                                                </div>
                                                           </div>
                                                        </div>
                                                    </td>
                                                    <td class="text-center"><?php echo $elRecomendado;?></td>
                                                    <td class="text-center"><?php echo $elEstado;?></td>
                                                    <td class="text-center">
                                                        <a class="btn btn-inverse waves-effect waves-light btn-xs" href="inmobiliaria_mapa.php?id=<?php echo $rowTodos['id'];?>"><i class="md-collections"></i> Mapa</a>
                                                        <a class="btn btn-inverse waves-effect waves-light btn-xs" href="inmobiliaria_inmuebles_galeria.php?id=<?php echo $rowTodos['id'];?>"><i class="md-collections"></i> Galeria</a>
                                                        <a class="btn btn-inverse waves-effect waves-light btn-xs" href="inmobiliaria_inmueble.php?id=<?php echo $rowTodos['id'];?>"><i class="ion-edit"></i> Editar</a>
                                                        <a class="btn btn-inverse waves-effect waves-light btn-xs" onclick="eliminarinmueble('inmobiliaria_inmuebles', <?php echo $rowTodos['id'];?>, 'inmobiliaria_inmueble.php','inmueble')"><i class="glyphicon glyphicon-trash"></i> Eliminar</a>
                                                    </td>
                                                </tr>
                                            <?php } ?>                                            
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <footer class="footer">2016 © Creasotol</footer>
                </div>        
            </div>
        </div>
        <?php include("partials/footer.php"); ?>
        <script type="text/javascript">
            $('.selectpicker').selectpicker();
        </script>
        <script type="text/javascript">
            $(document).ready(function () {
                $("#txtDetalle").htmlarea();
            });
            function eliminarinmueble(tabla, id, pagina,tipo){
  if (confirm("Esta seguro que desea eliminar el dato?")) {
    $.ajax({
      method: "POST",
      url: "partials/metodos.php",
      data: { accion: "eliminari", laTab: tabla, elId: id ,tipo:tipo}
    })
    .done(function(msg) {
     
      setInterval(function(){ location.href = pagina }, 2000);
    });
  }
}
        </script>
        
          <script type="text/javascript">
// Solo permite ingresar numeros.
function soloNumeros(e){
	var key = window.Event ? e.which : e.keyCode
	return (key >= 48 && key <= 57 ||key == 8   )
}
</script>
    </body>
</html>
<?php 
if (isset($_REQUEST['btnGuardar'])) {
    $id  = $_REQUEST['txtId'];
    $sec = $_REQUEST['selSeccion'];
    $cat = $_REQUEST['selCategoria'];
    $tip = $_REQUEST['selTipo'];
    $tit = $_REQUEST['txtTitulo'];    
    $val = $_REQUEST['txtValor'];
    $vaA = $_REQUEST['txtAdministracion'];
    $bar = $_REQUEST['txtBarrio'];
    $are = $_REQUEST['txtArea'];
    $arC = $_REQUEST['txtAreaC'];
    $hab = $_REQUEST['txtHabitaciones'];
    $ban = $_REQUEST['txtBanos'];
    $est = $_REQUEST['txtEstrato'];
    $gar = $_REQUEST['txtGarage'];
    $map = $_REQUEST['txtMapa'];
    $tex = $_REQUEST['txtDetalle'];
    $sta = $_REQUEST['rdoActivo'];
    $rec = $_REQUEST['rdoRecomendado'];
    $ciu = $_REQUEST['selUbicacion'];
    $cod = $_REQUEST['txtCodigo'];
    $ant = $_REQUEST['txtAntiguedad'];
       $tur = $_REQUEST['turismo'];
    $conss = $_REQUEST['constructo'];
    $inmoo = $_REQUEST['inmob'];
    
    $proe = $_REQUEST['proyecto'];


    if ($id == '') {
        $mensaje = "Se creo el inmueble.";
        $campos  = "id_seccion, id_categoria, id_tipo, codigo, titulo, detalle, valor,  ciudad, barrio, area, area_con, habitaciones, banos, estrato, garage, estado,proyecto, recomendado, antiguo,id_constructora,id_inmobiliaria, turismo,fecha_registro";
        $valores = "'".$sec."', '".$cat."', '".$tip."',  '".$cod."', '".$tit."', '".$tex."', '".$val."',  '".$ciu."', '".$bar."', '".$are."', '".$arC."', '".$hab."', '".$ban."', '".$est."','".$proe."', '".$gar."',  '".$sta."', '".$rec."', '".$ant."','".$conss."','".$inmoo."','".$tur."', '".date("Y-m-d")."'";
        $id = crearDato("inmobiliaria_inmuebles", $campos, $valores);
    }else{
        $mensaje = "Se modificaron los datos del inmueble.";
        $campos  = "id_categoria = '".$cat."', id_tipo = '".$tip."', codigo = '".$cod."', titulo = '".$tit."', detalle = '".$tex."', valor = '".$val."',  ciudad = '".$ciu."', proyecto = '".$proe."', barrio = '".$bar."',id_constructora = '".$conss."',turismo = '".$tur."',id_inmobiliaria = '".$inmoo."', area = '".$are."', area_con = '".$arC."', antiguo = '".$ant."', habitaciones = '".$hab."', banos = '".$ban."', estrato = '".$est."', garage = '".$gar."',   estado = '".$sta."', recomendado = '".$rec."'";
        actualizarDatos("inmobiliaria_inmuebles", $campos, "id = ".$id);
    }

    $carpeta = "../public/img/inmuebles/".$id."/";

    if(!file_exists($carpeta)){
        mkdir($carpeta, 0777);
        chmod($carpeta, 0777);
    }

    //SE CAMBIA EL LOGO SI SE CARGO UNO NUEVO
    if($_FILES['txtArchivo']['name'] != ''){
         $logo = "../public/img/inmuebles/".$id."";
        
        subir($logo,427,342, $_FILES['txtArchivo']['tmp_name'],'principal.jpg');
    }

    echo "<script> 
            $.Notification.notify('success','top left', 'Exito!', '".$mensaje."');
            setInterval(function(){ location.href = 'inmobiliaria_inmueble.php' }, 3000);
          </script>";
}
?>

 