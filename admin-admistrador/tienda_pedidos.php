<?php
include("partials/funciones.php");
//Verificando si esta logueado
if (!isset($_SESSION['usuPA_admin'])) {
    header('Location: index.php');
}

if (isset($_REQUEST['id'])) {
    $dato = busquedaIndividual("tienda_pedido", "id = ".$_REQUEST['id']);
}
$todos = busquedasGenerales("tienda_pedido", "", "ORDER BY id ASC");
?>
<!DOCTYPE html>
<html>
    <head>
        <?php include('partials/header.php');  ?>
        <style type="text/css">
            div.jHtmlArea .ToolBar ul li a.custom_disk_button{
                background: url(images/disk.png) no-repeat;
                background-position: 0 0;
            }            
            div.jHtmlArea { border: solid 1px #ccc; }
        </style>
    </head>
    <body class="fixed-left">
        <div id="wrapper">
            <?php include('partials/menu_horizontal.php');  ?>
        
            <div class="content-page">
                <div class="content">
                    <div class="container">
   
                        <!-- Donde Estoy -->
                        <div class="row">
                            <div class="col-sm-12">
                                <h4 class="page-title">Pedidos</h4>
                                <ol class="breadcrumb">
                                    <li><a href="index2.php">Inicio</a></li>
                                    <li class="active">Pedidos</li>
                                </ol>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-12">
                                <div class="card-box">
                                    <table id="datatable" class="table table-striped table-bordered">
                                        <thead>
                                            <tr>
                                                <th width="50px" class="text-center">ID</th>
                                                <th width="150px" class="text-center">Fecha</th>
                                                <th class="text-center">Cliente</th>
                                                <th width="400px" class="text-center">Lugar Envio</th>
                                                <th width="100px" class="text-center">SubTotal</th>
                                                <th width="150px" class="text-center">Envio</th>
                                                <th width="150px" class="text-center">Total</th>
                                                <th width="150px" class="text-center">Estado</th>
                                                <th width="100px" class="text-center">Acción</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php while ($rowTodos = mysqli_fetch_array($todos)){ 
                                                if ($rowTodos['estado'] == 0)
                                                    $elEstado = 'Pendiente';
                                                elseif ($rowTodos['estado'] == 1)
                                                    $elEstado = 'Enviado';
                                                elseif ($rowTodos['estado'] == 2)
                                                    $elEstado = 'Calificado';
                                                elseif ($rowTodos['estado'] == 3)
                                                    $elEstado = 'Cancelado';
                                                ?>
                                                <tr>
                                                    <td class="text-center"><?php echo $rowTodos['id'];?></td>
                                                    <td class="text-center"><?php echo date("H:i:s d/m/Y", strtotime($rowTodos['fecha']));?></td>
                                                    <td>
                                                        <?php 
                                                        if($rowTodos['id_usuario'] != '')
                                                            echo busquedaEspecifica("nombre", "nombre", "usuario", "id = ".$rowTodos['id_usuario']);
                                                        else
                                                            echo $rowTodos['nombre'];
                                                        ?>
                                                    </td>
                                                    <td><?php echo $rowTodos['pais'].' - '.$rowTodos['departamento'].' - '.$rowTodos['ciudad'].' - '.$rowTodos['direccion'].' - '.$rowTodos['telefono']; ?></td>
                                                    <td class="text-center">$ <?php echo number_format($rowTodos['subtotal'], 0, '', '.');?></td>
                                                    <td class="text-center">$ <?php echo number_format($rowTodos['envio'], 0, '', '.');?></td>
                                                    <td class="text-center">$ <?php echo number_format($rowTodos['total'], 0, '', '.');?></td>
                                                    <td class="text-center"><?php echo $elEstado;?></td>
                                                    <td class="text-center">
                                                        <a class="btn btn-inverse waves-effect waves-light btn-xs" href="tienda_pedidos_productos.php?idP=<?php echo $rowTodos['id'];?>"><i class="fa fa-search"></i> Ver Productos</a>
                                                    </td>
                                                </tr>
                                            <?php } ?>                                            
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <footer class="footer">2016 © Creasotol</footer>
                </div>        
            </div>
        </div>
        <?php include("partials/footer.php"); ?>
        <script type="text/javascript">
            $('.selectpicker').selectpicker();
        </script>        
        <script type="text/javascript">
            $(document).ready(function () {
                $("#txtDetalle").htmlarea();
            });
        </script>
    </body>
</html>
<?php 
if (isset($_REQUEST['btnGuardar'])) {
    $id  = $_REQUEST['txtId'];
    $cat = $_REQUEST['selCategoria'];
    $tit = $_REQUEST['txtTitulo'];    
    $cod = $_REQUEST['txtCodigo'];
    $sto = $_REQUEST['txtStock'];
    $pAn = $_REQUEST['txtPrecioAnt'];
    $pAh = $_REQUEST['txtPrecioAho'];
    $des = $_REQUEST['txtDescuento'];
    $col = $_REQUEST['txtColor'];
    $tal = $_REQUEST['txtTalla'];
    $vid = $_REQUEST['txtVideo'];
    $tex = $_REQUEST['txtDetalle'];
    $sta = $_REQUEST['rdoActivo'];

    if ($id == '') {
        $mensaje = "Se creo el producto.";
        $campos  = "id_categoria, titulo, codigo, stock, precio_ant, precio_aho, descuento, color, talla, video, descripcion, estado, fecha_registro";
        $valores = "'".$cat."', '".$tit."', '".$cod."', '".$sto."', '".$pAn."', '".$pAh."', '".$des."', '".$col."', '".$tal."', '".$vid."', '".$tex."', '".$sta."', '".date("Y-m-d H:i:s")."'";
        $id = crearDato("tienda_pedido", $campos, $valores);
    }else{
        $mensaje = "Se modificaron los datos del producto.";
        $campos  = "id_categoria = '".$cat."', titulo = '".$tit."', codigo = '".$cod."', stock = '".$sto."', precio_ant = '".$pAn."', precio_aho = '".$pAh."', descuento = '".$des."', color = '".$col."', talla = '".$tal."', video = '".$vid."', descripcion = '".$tex."', estado = '".$sta."'";
        actualizarDatos("tienda_pedido", $campos, "id = ".$id);
    }

    $carpeta = "../public/img/productos/".$id."/";

    if(!file_exists($carpeta)){
        mkdir($carpeta, 0777);
        chmod($carpeta, 0777);
    }

    //SE CAMBIA LA IMAGEN PREDETERMINADA SI SE CARGO UNO NUEVO
    if($_FILES['txtArchivo']['name'] != ''){
        $principal = "../public/img/productos/".$id."/principal.png";
        if(copy($_FILES['txtArchivo']['tmp_name'], $principal) ){ 
            $mensaje .= ' Se cargo la imagen principal. ';
        }else{
            $mensaje .= ' Se produjo un error al cargar la imagen principal. ';
        }
    }

    //SE CAMBIA LA IMAGEN DE LOS COLORES SI SE CARGO UNO NUEVO
    if($_FILES['txtArchivo']['name'] != ''){
        $colores = "../public/img/productos/".$id."/colores.jpg";
        if(copy($_FILES['txtArchivo']['tmp_name'], $colores) ){ 
            $mensaje .= ' Se cargo la imagen de los colores. ';
        }else{
            $mensaje .= ' Se produjo un error al cargar la imagen de los colores. ';
        }
    }

    //SE CAMBIA LA IMAGEN DE LAS TALLAS SI SE CARGO UNO NUEVO
    if($_FILES['txtArchivo']['name'] != ''){
        $tallas = "../public/img/productos/".$id."/tallas.jpg";
        if(copy($_FILES['txtArchivo']['tmp_name'], $tallas) ){ 
            $mensaje .= ' Se cargo la imagen de las tallas. ';
        }else{
            $mensaje .= ' Se produjo un error al cargar la imagen de las tallas. ';
        }
    }

    echo "<script> 
            $.Notification.notify('success','top left', 'Exito!', '".$mensaje."');
            setInterval(function(){ location.href = 'tienda_pedidos.php' }, 3000);
          </script>";
}
?>