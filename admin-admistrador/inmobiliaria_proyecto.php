<?php
include("partials/funciones.php");
//Verificando si esta logueado
if (!isset($_SESSION['usuPA_admin'])) {
    header('Location: index.php');
}

if (isset($_REQUEST['id'])) {
    $dato = busquedaIndividual("inmobiliaria_articulo", "id = ".$_REQUEST['id']);
}

$todos = busquedasGenerales("inmobiliaria_articulo", "", "ORDER BY id ASC");
?>
<!DOCTYPE html>
<html>
    <head>
        <?php include('partials/header.php');  ?>
    </head>
    <body class="fixed-left">
        <div id="wrapper">
            <?php include('partials/menu_horizontal.php');  ?>
        
            <div class="content-page">
                <div class="content">
                    <div class="container">
    
                        <!-- Donde Estoy -->
                        <div class="row">
                            <div class="col-sm-12">
                                <h4 class="page-title">Proyectos</h4>
                                <ol class="breadcrumb">
                                    <li><a href="index2.php">Inicio</a></li>
                                    <!-- <li><a href="seccion.php">Sección</a></li> 
                                    <li><a href="categoria.php">Categorias</a></li>-->
                                    <li class="active">Proyectos</li>
                                </ol>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-12">
                                <div class="card-box">
                                    <form action="#" method="post" enctype="multipart/form-data"  class="form-horizontal">
                                        <input type="hidden" name="txtId"      id="txtId"           value="<?php if(isset($dato['id'])) echo $dato['id'];?>" />
                                        <input type="hidden" name="selSeccion" id="selSeccion"      value="1" />
                                        <input type="hidden" name="selCategoria" id="selCategoria"  value="1" />
                                        <input type="hidden" name="rdoTipo"    id="rdoTipo"         value="1" />
                                        <div class="form-group">
                                            <div class="col-lg-6">
                                                <label>Proyecto:</label>
                                                <input type="text" class="form-control" name="txtTitulo" id="txtTitulo" placeholder="Proyecto" value="<?php if(isset($dato['titulo'])) echo $dato['titulo'];?>">
                                                <br> 
                                            </div> 

                                            <div class="col-lg-3">
                                                <label>Imágen de Muestra:</label>
                                                <input type="file" name="txtArchivo" id="txtArchivo" class="filestyle" data-buttontext="Seleccione" data-buttonname="btn-inverse"  onchange="verificarjpg(this.value, 'txtArchivo')">
                                                <br> 
                                            </div>                                            

                                            <div class="col-lg-3">
                                                <label>Estado:</label>
                                                <div class="radio radio-primary">
                                                    <input type="radio" name="rdoActivo" id="rdoActivo" required value="1"<?php if(isset($dato['estado']) && $dato['estado'] == 1) echo "checked";?>>
                                                    <label for="radio3">Activo</label> &nbsp; &nbsp; &nbsp; &nbsp;
                                                    <input type="radio" name="rdoActivo" id="rdoActivo" required value="2"<?php if(isset($dato['estado']) && $dato['estado'] == 2) echo "checked";?>>
                                                    <label for="radio3">Inactivo</label>
                                                </div>                                                
                                            </div>                                         
                                            <div class="col-lg-12">
                                                <textarea name="txtDetalle" id="txtDetalle" class="form-control" rows="15" required="required"><?php if(isset($dato['detalle'])) echo $dato['detalle'];?></textarea><br>
                                            </div>                                        
                                            <div class="col-lg-12 text-center">
                                                <button class="btn btn-primary waves-effect waves-light" type="submit" name="btnGuardar" id="btnGuardar">Guardar</button>
                                                <a href="inmobiliaria_proyecto.php" class="btn btn-default waves-effect waves-light m-l-5">Cancel</a>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>

                       <div class="row">
                            <div class="col-sm-12">
                                <div class="card-box">
                                    <table id="datatable" class="table table-striped table-bordered">
                                        <thead>
                                            <tr>
                                                <th width="50px" class="text-center">ID</th>
                                                <th width="150px" class="text-center">Sección</th>
                                                <!-- <th width="150px" class="text-center">Categoria</th> -->
                                                <th class="text-center">Proyectos</th>
                                                <th width="80px" class="text-center">Estado</th>
                                                <th width="160px" class="text-center">Acción</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php while ($rowTodos = mysqli_fetch_array($todos)){ 
                                                if ($rowTodos['estado'] == 1)
                                                    $elEstado = '<span class="label label-table label-success" onclick="inactivar('."'inmobiliaria_articulo'".', '."'".$rowTodos['id']."'".', 2, '."'inmobiliaria_proyecto.php'".')" style="cursor:pointer">Activo</span>';
                                                else
                                                    $elEstado = '<span class="label label-table label-danger"  onclick="inactivar('."'inmobiliaria_articulo'".', '."'".$rowTodos['id']."'".', 1, '."'inmobiliaria_proyecto.php'".')" style="cursor:pointer">Inactivo</span>';
                                                ?>
                                                <tr>
                                                    <td class="text-center"><?php echo $rowTodos['id'];?></td>
                                                    <td><?php echo busquedaEspecifica("titulo", "titulo", "inmobiliaria_seccion", "id = ".$rowTodos['id_seccion']);?></td>
                                                    <td>
                                                        <ins class="text-primary" style="cursor:pointer;" data-toggle="modal" data-target="#modal_<?php echo $rowTodos['id'];?>">
                                                            <?php echo $rowTodos['titulo'].'<br>'.strip_tags(substr($rowTodos['detalle'], 0, 50));?>
                                                        </ins>
                                                       <div id="modal_<?php echo $rowTodos['id'];?>" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                                                            <div class="modal-dialog">
                                                                <div class="modal-content p-0 b-0">
                                                                    <div class="panel panel-color panel-primary">
                                                                        <div class="panel-heading"> 
                                                                            <button type="button" class="close m-t-5" data-dismiss="modal" aria-hidden="true">×</button> 
                                                                            <h3 class="panel-title">Imagen Principal</h3> 
                                                                        </div> 

                                                                       <div class="panel-body"> 
                                                                            <img src="../public/img/proyectos/<?php echo $rowTodos['id'];?>/principal.jpg" style="max-width: 300px; max-height: 350px">
                                                                        </div> 
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                   </td>
                                                    <td class="text-center"><?php echo $elEstado;?></td>
                                                    <td class="text-center">
                                                        <a class="btn btn-inverse waves-effect waves-light btn-xs" href="inmobiliaria_proyecto.php?id=<?php echo $rowTodos['id'];?>"><i class="ion-edit"></i> Editar</a>
                                                        <a class="btn btn-inverse waves-effect waves-light btn-xs" onclick="eliminar('inmobiliaria_articulo', <?php echo $rowTodos['id'];?>, 'inmobiliaria_proyecto.php')"><i class="glyphicon glyphicon-trash"></i> Eliminar</a>
                                                    </td>
                                                </tr>
                                           <?php } ?>                                            
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <footer class="footer">2016 © Creasotol</footer>
                </div>        
            </div>
        </div>
        <?php include("partials/footer.php"); ?>
        <script type="text/javascript">
            $('.selectpicker').selectpicker();
        </script>
        <script type="text/javascript">
            $(document).ready(function () {
                $("#txtDetalle").htmlarea();
            });
        </script>
    </body>
</html>

<?php 
if (isset($_REQUEST['btnGuardar'])) {
    $id = $_REQUEST['txtId'];

    if ($id == '') {
        $mensaje = "Se creo el proyecto.";
        $campos  = "id_seccion, id_categoria, tipo, titulo, detalle, estado, fecha_registro";
        $valores = "'".$_REQUEST['selSeccion']."', '".$_REQUEST['selCategoria']."', '".$_REQUEST['rdoTipo']."', '".$_REQUEST['txtTitulo']."', '".$_REQUEST['txtDetalle']."', '".$_REQUEST['rdoActivo']."', '".date("Y-m-d")."'";
        $id = crearDato("inmobiliaria_articulo", $campos, $valores);
    }else{
        $mensaje = "Se modificaron los datos del proyecto.";
        $campos  = "id_seccion = '".$_REQUEST['selSeccion']."', id_categoria = '".$_REQUEST['selCategoria']."', tipo = '".$_REQUEST['rdoTipo']."', titulo = '".$_REQUEST['txtTitulo']."', detalle = '".$_REQUEST['txtDetalle']."', estado = '".$_REQUEST['rdoActivo']."'";
        actualizarDatos("inmobiliaria_articulo", $campos, "id = ".$id);
    }

    $carpeta = "../public/img/proyectos/".$id."/";

    if(!file_exists($carpeta)){
        mkdir($carpeta, 0777);
        chmod($carpeta, 0777);
    }

   //SE CAMBIA EL LOGO SI SE CARGO UNO NUEVO
    if($_FILES['txtArchivo']['name'] != ''){
        $logo = "../public/img/proyectos/".$id."/principal.jpg";

        if(copy($_FILES['txtArchivo']['tmp_name'], $logo) ){ 
            $mensaje .= ' Se cargo la imagen. ';
        }else{
            $mensaje .= ' Se produjo un error al cargar la imagen. ';
        }
    }

    echo "<script> 
            $.Notification.notify('success','top left', 'Exito!', '".$mensaje."');
            setInterval(function(){ location.href = 'inmobiliaria_proyecto.php' }, 3000);
          </script>";
}
?>