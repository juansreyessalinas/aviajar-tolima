/*************************************************** METODOS DE ACCESO ***********************************************************/


/**
 * [FUNCION logout() REALIZA LA SALIDA DEL USUARIO]
 */
function logout(){
	$.ajax({
		method: "POST",
		url: "partials/metodos.php",
		data: { accion: "logout" }
	})
	.done(function() {
		location.href = "index.php";
	});
}






function eliminarinmueble(tabla, id, pagina,tipo){
  if (confirm("Esta seguro que desea eliminar el dato?")) {
    $.ajax({
      method: "POST",
      url: "partials/metodos.php",
      data: { accion: "eliminari", laTab: tabla, elId: id ,tipo:tipo}
    })
    .done(function(msg) {
     
      setInterval(function(){ location.href = pagina }, 2000);
    });
  }
}




function eliminarF(tabla, id, pagina,carpeta){
  if (confirm("Esta seguro que desea eliminar el dato?")) {
    $.ajax({
      method: "POST",
      url: "partials/metodos.php",
      data: { accion: "eliminarF", laTab: tabla, elId: id ,carpeta:carpeta}
    })
    .done(function(msg) {
     
      setInterval(function(){ location.href = pagina }, 2000);
    });
  }
}



/*************************************************** METODOS GENERALES ***********************************************************/


/**
 * [verificarpdf VERIFICA SI EL ARCHIVO SUBIDO ES UN JPG]
 * @param  archivo [VARIABLE QUE TRAE EL ARCHIVO A VERIFICAR]
 */
function verificarpdf(archivo, campo){
	var ext_valida = /\.(pdf)$/i.test(archivo);
	if (!ext_valida){
    $("#"+campo).val('');
    swal({ title: "ERROR AL CARGAR IMAGEN", text: "El tipo de formato del archivo no es pdf.", type: "error",  confirmButtonText: "Ok" });
  }
}


/**
 * [verificarjpg VERIFICA SI EL ARCHIVO SUBIDO ES UN JPG]
 * @param  archivo [VARIABLE QUE TRAE EL ARCHIVO A VERIFICAR]
 */
function verificarjpg(archivo, campo){
	var ext_valida = /\.(jpg)$/i.test(archivo);
	if (!ext_valida){
    $("#"+campo).val('');
    swal({ title: "ERROR AL CARGAR IMAGEN", text: "El tipo de formato del archivo no es jpg.", type: "error",  confirmButtonText: "Ok" });
  }
}


/**
 * [verificarpng VERIFICA SI EL ARCHIVO SUBIDO ES UN JPG]
 * @param  archivo [VARIABLE QUE TRAE EL ARCHIVO A VERIFICAR]
 */
function verificarpng(archivo, campo){
	var ext_valida = /\.(png)$/i.test(archivo);
	if (!ext_valida){
    $("#"+campo).val('');
    swal({ title: "ERROR AL CARGAR IMAGEN", text: "El tipo de formato del archivo no es png.", type: "error",  confirmButtonText: "Ok" });
  }
}


/**
 * [formatCurrency CONVIERTE UN VALOR EN MONEDA]
 * @param  total [VARIABLE QUE TRAE EL VALOR A CONVERTIR]
 */
function formatCurrency(total) {
    var neg = false;
    if(total < 0) {
        neg = true;
        total = Math.abs(total);
    }
    return (neg ? "-$" : '$') + parseFloat(total, 10).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1.").toString();
}


/**
 * [convertir FUNCION QUE CONVIERTE UN CAMPO EN FORMATO MD5]
 * @param  [strign] valor [VARIABLE QUE TRAE EL VALOR A CONVERTIR]
 * @param  [strign] campo [VARIABLE QUE TRAE EL CAMPO A COLOCAR DATO CONVERTIDO]
 */
 function convertir(valor, campo){
 	var clave = md5(valor);
 	$("#"+campo).val(clave);
 }


/**
 * [inactivar FUNCION QUE INACTIVA O ACTIVA UN REGISTRO]
 * @param  {[type]} tabla  	[VARIABLE QUE TRAE EL NOMBRE DE LA TABLA A IN-ACTIVAR]
 * @param  {[type]} id     	[VARIABLE QUE TRAE EL ID DEL REGISTRO A IN-ACTIVAR]
 * @param  {[type]} est    	[VARIABLE QUE TRAE EL ESTADO]
 * @param  {[type]} pagina 	[VARIABLE QUE TRAE LA PAGINA DONDE SE RECARGARA]
 */
function inactivar(tabla, id, est, pagina){
	$.ajax({
		method: "POST",
		url: "partials/metodos.php",
		data: { accion: "inactivar", laTab: tabla, elId: id, elEst: est  }
	})
	.done(function(msg) {
		$.Notification.notify('success','top left', 'Exito!', msg);
		setInterval(function(){ location.href = pagina }, 2500);
	});
	
}


/**
 * [eliminar FUNCION QUE ELIMINA UN DATO]
 * @param  {[type]} tabla  [VARIABLE QUE TRAE LA TABLA DEL DATO A ELIMINAR]
 * @param  {[type]} id     [VARIABLE QUE TRAE EL ID DEL DATO A ELIMINAR ]
 * @param  {[type]} pagina [VARIABLE QUE TRAE LA PAGINA A RECARGAR]
 */
function eliminar(tabla, id, pagina){
	swal({ 	title: "Esta seguro que desea eliminar los datos?", 
			showCancelButton: true,
			confirmButtonText: "Si",
			cancelButtonText: "No",
			closeOnConfirm: false,   
			showLoaderOnConfirm: true, 
		}, function(){   
			$.ajax({
				method: "POST",
				url: "partials/metodos.php",
				data: { accion: "eliminar", laTab: tabla, elId: id }
			})
			.done(function(msg) {
				if (msg != '') { var tipo = "error"; }else{ var tipo = "success"; }
				swal({ title: msg, type: tipo,  confirmButtonText: "Ok" });
				setInterval(function(){ location.href = pagina }, 2500);
			});
		}
	);
}


/**
 * [eliminarDato FUNCION QUE ELIMINA UN DATO CON ARCHIVO]
 * @param  {[type]} tabla  [VARIABLE QUE TRAE LA TABLA DEL DATO A ELIMINAR]
 * @param  {[type]} id     [VARIABLE QUE TRAE EL ID DEL DATO A ELIMINAR ]
 * @param  {[type]} url    [VARIABLE QUE TRAE LA URL DEL ARCHIVO A ELIMINAR]
 * @param  {[type]} pagina [VARIABLE QUE TRAE LA PAGINA A RECARGAR]
 */
function eliminarDato(tabla, id, url, pagina){
	swal({ 	title: "Esta seguro que desea eliminar el archivo?", 
			showCancelButton: true,   
			closeOnConfirm: false,   
			showLoaderOnConfirm: true, 
		}, function(){   
			$.ajax({
				method: "POST",
				url: "partials/metodos.php",
				data: { accion: "eliminarDato", laTab: tabla, elId: id, laUrl: url }
			})
			.done(function(msg) {
				swal({ title: msg, type: "success",  confirmButtonText: "Ok" });
				setInterval(function(){ location.href = pagina }, 2500);
			});
		}
	);
}


/**
 * [eliminarArchivo FUNCION QUE ELIMINA UN DATO CON ARCHIVO]
 * @param  {[type]} url    [VARIABLE QUE TRAE LA URL DEL ARCHIVO A ELIMINAR]
 * @param  {[type]} pagina [VARIABLE QUE TRAE LA PAGINA A RECARGAR]
 */
function eliminarArchivo(url, pagina){
	swal({ 	title: "Esta seguro que desea eliminar el archivo?", 
			showCancelButton: true,   
			closeOnConfirm: false,   
			showLoaderOnConfirm: true, 
		}, function(){   
			$.ajax({
				method: "POST",
				url: "partials/metodos.php",
				data: { accion: "eliminarArchivo", laUrl: url }
			})
			.done(function(msg) {
				$("#textoMensaje").html(msg);
           		$("#elMensaje").show();
				setInterval(function(){ location.href = pagina }, 4000);
			});
		}
	);
}


/**
 * [volver FUNCION QUE DEVUELVE A LA PAGINA ANTERIOR]
 */
function volver(){
	history.back();
}


/**
 * [atras FUNCION QUE LLEVA A UNA URL QUE SE PASA]
 * @param  {[type]} link [VARIABLE QUE TRAE LA URL A LA QUE SE REDIRECCIONA]
 */
function atras(link){
	location.href = link;
}


/**
 * [mayorCero FUNCION QUE VALIDA SI UN CAMPO ES MAYOR A CERO]
 * @param  {[type]} val   [VARIABLE QUE TRAE EL VALOR DEL CAMPO A VALIDAR]
 * @param  {[type]} campo [VARIABLE QUE TRAE EL CAMPO QUE SE VALIDA]
 */
function mayorCero(val, campo){
	if(val <= 0)
		$("#"+campo).val(1);
}


/**
 * [menorFecha FUNCION QUE VALIDA SI UNA FECHA ES MENOR A OTRA]
 * @param  {[type]} val1   [VARIABLE QUE TRAE EL VALOR A VALIDAR]
 * @param  {[type]} campo1 [VARIABLE QUE TRAE EL CAMPO 1 QUE SE VALIDA]
 * @param  {[type]} campo2 [VARIABLE QUE TRAE EL CAMPO 2 QUE SE VALIDA]
 */
function menorFecha(val1, campo1, campo2){	
	var val2 = $("#"+campo2).val();
	if(val1 < val2)
		$("#"+campo1).val(val2);
}


/**
 * [cargarDato FUNCION QUE CARGA UN DATO]
 * @param  {[type]} val   [VARIABLE QUE TRAE EL VALOR QUE IDENTIFICA LOS DATOS]
 * @param  {[type]} tabla [VARIABLE QUE TRAE LA TABLA QUE SE VA A BUSCAR]
 * @param  {[type]} campo [VARIABLE QUE TRAE EL CAMPO EN EL QUE SE A CARGAR]
 * @param  {[type]} busca [VARIABLE QUE TRAE EL VALOR QUE SE VA A BUSCAR]
 */
function cargarDato(val, tabla, campo, busca){
	$.ajax({
		method: "POST",
		url: "partials/metodos.php",
		data: { accion: "cargarDato", dat: val, tab: tabla, daB: busca }
	})
	.done(function(msg) {
		$("#"+campo).val(msg);
	});
}


/**
 * [cargarSel FUNCION QUE CARGA LOS DATOS BUSCADOS EN UN SELECT]
 * @param  {[type]} val   	 [VARIABLE QUE TRAE EL VALOR QUE IDENTIFICA LOS DATOS]
 * @param  {[type]} tabla 	 [VARIABLE QUE TRAE LA TABLA QUE SE VA A BUSCAR]
 * @param  {[type]} campo 	 [VARIABLE QUE TRAE EL CAMPO EN EL QUE SE A CARGAR]
 * @param  {[type]} busca 	 [VARIABLE QUE TRAE EL VALOR QUE SE VA A BUSCAR]
 * @param  {[type]} variable [VARIABLE QUE TRAE LA VARIABLE A BUSAR]
 */
function cargarSel(val, campo, tabla, busca, variable){
	$.ajax({
		method: "POST",
		url: "partials/metodos.php",
		data: { accion: "cargarSel", 'tabla': tabla, 'busca': busca, 'valor': val, 'campo': variable }
	})
	.done(function(msg) {
		$("#"+campo).html(msg);
	});
}


/**
 * [validarCorreo FUNCION QUE VALIDA UN CORREO]
 * @param  {[type]} email [VARIABLE QUE TRAE EL EMAIL A VALIDAR]
 */
function validarCorreo(email) {
    var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
	return regex.test(email);
};


/**
 * [abrirLink FUNCION QUE ABRE UN LINK]
 * @param  {[type]} url [VARIABLE QUE TRAE LA URL]
 */
function abrirLink(url){
	location.href = url;
}


/**
 * [abrirLinkOtra FUNCION QUE ABRE UN LINK EN UNA NUEVA PAGINA]
 * @param  {[type]} url [VARIABLE QUE TRAE LA URL]
 */
function abrirLinkOtra(url){
	open(url);
}


$(document).ready(function (){
	$('.solo-numero').keyup(function (){
		this.value = (this.value + '').replace(/[^0-9]/g, '');
	});
});


/**
 * [verificarjpg VERIFICA SI EL ARCHIVO SUBIDO ES UN JPG]
 * @param  archivo [VARIABLE QUE TRAE EL ARCHIVO A VERIFICAR]
 */
function cargarInmueble(archivo, campo){

		$("#formImg").submit();
	
}






/*************************************************** METODOS ESPECIFICOS ***********************************************************/


