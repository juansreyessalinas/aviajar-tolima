<!-- INICIO Menu Superior --> 
<div class="topbar">
    <div class="topbar-left">
        <div class="text-center">
            <a href="index2.php" class="logo"><i class="icon-magnet icon-c-logo"></i><span>Administrador</span></a>
        </div>
    </div>

    <!-- Button mobile view to collapse sidebar menu -->
    <div class="navbar navbar-default" role="navigation">
        <div class="container">
            <div class="">
                <div class="pull-left">
                    <button class="button-menu-mobile open-left">
                        <i class="ion-navicon"></i>
                    </button>
                    <span class="clearfix"></span>
                </div>

                <ul class="nav navbar-nav navbar-right pull-right">                     
                    <li class="dropdown">
                        <a href="" class="dropdown-toggle profile" data-toggle="dropdown" aria-expanded="true">
                            <?php echo $_SESSION['usuPA_admin']['nombre'];?>
                        </a>
                        <ul class="dropdown-menu">
                            <li><a href="index2.php"><i class="ti-user m-r-5"></i> Mis Datos</a></li>
                            <li><a href="javascript:void(0)" onclick="logout()"><i class="ti-power-off m-r-5"></i> Salir</a></li>
                        </ul>
                    </li>
                    <li class="hidden-xs">
                        <a href="#" id="btn-fullscreen" class="waves-effect waves-light"><i class="icon-size-fullscreen"></i></a>
                    </li>
                </ul>
            </div>
            <!--/.nav-collapse -->
        </div>
    </div>
</div>
<!-- FIN Menu Superior --> 


<!-- INICIO Menu Lateral Izquierda --> 
<div class="left side-menu">
    <div class="sidebar-inner slimscrollleft">
        <div id="sidebar-menu">
            <ul>
                <li class="has_sub">
                    <a href="#" class="waves-effect <?php echo $ul1;?>"><i class="md-home"></i> <span>Configuracion </span> </a>
                    <ul class="list-unstyled">
                        <li <?php echo $li2;?>><a href="posicionamiento.php"><i class="md md-dashboard"></i> <span>Posicionamiento</span></a></li>
                        <li <?php echo $li2;?>><a href="generales.php"><i class="md md-dashboard"></i> <span>Generales</span></a></li>
                         <li <?php echo $li7;?>><a href="paginas.php"><i class="md md-photo-library"></i> <span>Paginas</span></a></li>
                     
                        <li <?php echo $li7;?>><a href="imagenes.php"><i class="md md-photo-library"></i> <span>Imagenes</span></a></li>
                        
                       
                      
                        </li>
                    </ul>
                   
                </li> 
                <li><a href="tienda_categoria.php"><i class="ion-monitor"></i> Categoria</a></li>
                <li><a href="banner.php"><i class="ion-monitor"></i> Banner </a></li>
           
                 <li><a href="newsletter.php"><i class="md md-equalizer"></i> Suscribete</a></li>
          
         
                <li><a href="tienda_productos.php"><i class="md md-equalizer"></i> Planes</a></li>
            <!--
            <li><a href="inmobiliaria_ciudades.php"><i class="md md-assignment"></i> Ciudades</a></li>
        	<li><a href="marca.php"><i class="md md-assignment"></i> Constructora</a></li>
        	<li><a href="inmobiliarias.php"><i class="md md-assignment"></i> Inmobiliaria</a></li>
                <li><a href="noticias.php"><i class="md md-equalizer"></i> Noticias</a></li>
                <li><a href="inmobiliaria_inmueble.php"><i class="md md-home"></i> Inmuebles</a></li>
                <li><a href="usuarios.php"><i class="md md-home"></i> Usuarios</a></li>-->
            </ul>
            <div class="clearfix"></div>
        </div>
        <div class="clearfix"></div>
    </div>
</div>
<!-- FIN Menu Lateral Izquierda -->

