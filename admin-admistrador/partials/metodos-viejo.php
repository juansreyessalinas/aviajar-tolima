<?php
require("funciones.php");
require("PHPMailer/_lib/class.phpmailer.php");
$accion = $_REQUEST['accion'];


/**
 * [$accion METODO PARA INGRESAR A LA PLATAFORMA]
 */
if($accion == 'loginAdm'){
    $conexion = new conexion();
    $usuario  = $_REQUEST['usu'];
    $clave    = md5($_REQUEST['cla']);
    $mensaje  = '';

    //BUSQUEDA EN LA BD DEL ADMINISTRADOR
    $rowDatos = busquedaIndividual('administrador', "usuario = '".$usuario."' AND clave = '".$clave."'");
    if($rowDatos['estado'] == '1'){
        $_SESSION['usuPA_admin'] = array(
                                        'id'      => $rowDatos['id'], 
                                        'nombre'  => $rowDatos['nombre'], 
                                        'email'   => $rowDatos['email'],
                                        'usuario' => $rowDatos['usuario'],
                                        'permiso' => $rowDatos['permiso']
                                       );

        $mensaje  = '1';
    }elseif($rowDatos['estado'] == '0'){
        $mensaje = 'El usuario se encuentra inactivo. Para mayor información comuniquese con el administrador.';
    }else{
        $mensaje = 'El usuario no se encuentra registrado.';
    }

    echo $mensaje;
}


/**
 * [$accion METODO PARA SALIR DE LA PLATAFORMA]
 */
if($accion == 'logout'){
    unset($_SESSION['usuPA_admin']);
}


/**
 * [$accion METODO QUE REESTABLECE LA CONSTRASEÑA DE UN USUARIO]S
 */
if ($accion == 'restablecer') {
    $conexion  = new conexion();
    $correoRec = $_REQUEST['ema'];
    $datos     = buscarCorreo($correoRec);
    $fechaHoy  = date('Y-m-d H:i:s');
    $cadena    = generarNuevoTexto(10);
    $clave     = md5($cadena);

    if($datos == '0'){
        echo 0;
    }else{
        $datos   = explode('**', $datos);        
        $asunto  = '[RESTABLECER CLAVE DE ACCESO] - '.$_SERVER['HTTP_HOST'];
        $fechaS  = date('Y-m-d H:m:s');
        $mensaje = "<html>
                        <head>
                            <title>:.. Recordar Contraseña ..:</title>
                            <style>
                                .linkpag{
                                    text-decoration:none;
                                    color:#39F;
                                }
                            </style>
                        </head>
                        <body>    
                            Usted solicito restablecer la clave de acceso:<br><br>

                            Fecha Envio: ".$fechaS."<br><br>

                            <b>DATOS DE ACCESO:</b><br>
                            <b>USUARIO:</b> ".$datos[1]."<br>
                            <b>CONTRASEÑA:</b> ".$cadena."<br><br><br>

                            Enviado desde, Pagina Web<br />

                            <b>SISTEMA EDUCATIVO DE CONTROL DE NOTAS/b><br />

                            <a href='http://".$_SERVER['HTTP_HOST']."' target='_blank' class='linkpag'>www.".$_SERVER['HTTP_HOST']."</a>
                            <br /><br /><br />  

                            <b>NOTA:</b> Por favor, no responder a este correo automatico.
                        </body> 
                    </html>";
        $mail = new PHPMailer();
        $mail->Host     = "localhost";
        $mail->From     = "seguridad@creasotol.com";
        $mail->Subject  = $asunto;
        $mail->AddAddress($correoRec, "SISTEMA EDUCATIVO DE CONTROL DE NOTAS");
        $mail->Body     = $mensaje;
        $mail->AltBody  = "Restablecer Contraseña";
        $mail->Send();

        if ($datos[0] == 1) {
            actualizarDatos("administrador", "clave = '".$clave."'", "id = ".$datos[1]);
            $conexion->insertar('administrador_historial', "id_administrador, tabla, tipo_accion, observacion, fecha_registro", " '".$datos[1]."', 'administrador', '3', 'Se restablecio la clave: ".$cadena."', '".$fechaHoy."' ");

        }elseif ($datos[0] == 2) {
            actualizarDatos("docente", "clave = '".$clave."'", "id = ".$datos[1]);
            $conexion->insertar('docente_historial', "id_docente, tabla, tipo_accion, observacion, fecha_registro", " '".$datos[1]."', 'docente', '3', 'Se restablecio la clave: ".$cadena."', '".$fechaHoy."' ");

        }elseif ($datos[0] == 3) {
            actualizarDatos("estudiante", "clave = '".$clave."'", "id = ".$datos[1]);
            $conexion->insertar('estudiante_historial', "id_estudiante, tabla, tipo_accion, observacion, fecha_registro", " '".$datos[1]."', 'estudiante', '3', 'Se restablecio la clave: ".$cadena."', '".$fechaHoy."' ");

        }      

        echo 1;
    }
}


/**
 * [$accion METODO QUE INACTIVA O ACTIVA UN REGISTRO]
 */
if($accion == 'inactivar'){
    $conexion = new conexion();
    $tabla    = $_REQUEST['laTab'];
    $elId     = $_REQUEST['elId'];
    $estado   = $_REQUEST['elEst'];
    $sqlUpdEs = "UPDATE $tabla SET estado = $estado WHERE id = $elId";
    $conexion->ejecutar($sqlUpdEs);

    if($estado == '1')
        echo "Se activaron los datos.";
    else
        echo "Se inactivaron los datos.";
        
}
if($accion == 'eliminari'){
    $conexion = new conexion();
    $tabla    = $_REQUEST['laTab'];
    $elId     = $_REQUEST['elId'];
     $tipo     = $_REQUEST['tipo'];
    $sqlBorra = "DELETE FROM $tabla WHERE id = $elId";
    $conexion->ejecutar($sqlBorra);
    
    if($tipo=="inmueble"){
        $carpeta="../../public/img/inmuebles/".$elId;
        foreach(glob($carpeta . "/*") as $archivos_carpeta)
    {
        echo $archivos_carpeta;
 
        if (is_dir($archivos_carpeta))
        {
            eliminarDir($archivos_carpeta);
        }
        else
        {
            unlink($archivos_carpeta);
        }
    }
 
    rmdir($carpeta);
    }
}




if($accion == 'eliminarF'){
    
     $conexion = new conexion();
    $tabla    = $_REQUEST['laTab'];
    $elId     = $_REQUEST['elId'];
     $car    = $_REQUEST['carpeta'];
    $sqlBorra = "DELETE FROM $tabla WHERE id = $elId";
    $conexion->ejecutar($sqlBorra);
    
   
    
    function eliminarDir($carpeta)
{
    foreach(glob($carpeta . "/*") as $archivos_carpeta)
    {
        echo $archivos_carpeta;
 
        if (is_dir($archivos_carpeta))
        {
            eliminarDir($archivos_carpeta);
        }
        else
        {
            unlink($archivos_carpeta);
        }
    }
 
    rmdir($carpeta);
}
    
    
   eliminarDir($car); 
    
}

 
/**
 * [$accion METODO QUE ELIMINA UN REGISTRO]
 */
if($accion == 'eliminar'){
    $conexion = new conexion();
    $tabla    = $_REQUEST['laTab'];
    $elId     = $_REQUEST['elId'];
    $sqlBorra = "DELETE FROM $tabla WHERE id = $elId";
    $conexion->ejecutar($sqlBorra);
}


/**
 * [$accion METODO QUE ELIMINA UN REGISTRO]
 */
if($accion == 'eliminarDato'){
    $conexion = new conexion();
    $tabla    = $_REQUEST['laTab'];
    $elId     = $_REQUEST['elId'];
    $laUrl    = '../'.$_REQUEST['laUrl'];
    $sqlBorra = "DELETE FROM $tabla WHERE id = $elId";
    $conexion->ejecutar($sqlBorra);
    echo "Se eliminaron los datos.";
    unlink($laUrl);
}


/**
 * [$accion METODO QUE ELIMINA UN ARCHIVO]
 */
if($accion == 'eliminarArchivo'){
    $laUrl = '../'.$_REQUEST['laUrl'];
    echo "Se elimino el archivo.";
    unlink($laUrl);
}


/**
 * [$accion METODO QUE ELIMINA UN REGISTRO]
 */
if($accion == 'cargarDato'){
    $conexion = new conexion();
    $dato     = $_REQUEST['dat'];
    $tabla    = $_REQUEST['tab'];
    $busca    = $_REQUEST['daB'];
    $sqlBusca = "SELECT $busca FROM $tabla WHERE id = $dato";
    $resBusca = $conexion->filtro($sqlBusca);
    $rowBusca = $conexion->filas($resBusca);
    echo $rowBusca[$busca];
}


/**
 * [$accion METODO QUE ELIMINA UN REGISTRO]
 */
if($accion == 'cargarSel'){
    $conexion = new conexion();
    $tabla    = $_REQUEST['tabla'];
    $busca    = $_REQUEST['busca'];
    $valor    = $_REQUEST['valor'];
    $campo    = $_REQUEST['campo'];

    $sql      = "SELECT * FROM $tabla WHERE $busca = '$valor' ORDER BY $campo";
    $res      = $conexion->filtro($sql);
    $html     = '<option value=""></option>';

    while ($row = mysqli_fetch_array($res)) {
        $html .= '<option value="'.$row['id'].'">'.$row[$campo].'</option>';
    }

    $conexion->cerrarfiltro($res);
    echo $html;
}


/**
 * [METODO QUE BUSQUEDA EN UN AUTOCOMPLETA]
 */
if($accion == 'autoBusqueda'){
    $conexion = new conexion();
    $tabla    = $_REQUEST['tabla'];
    $campo1   = $_REQUEST['campo1'];
    $campo2   = $_REQUEST['campo2'];
    $palabra  = $_REQUEST['palabra'];

    if($campo2 != ''){
        $cam1 = ", ".$campo2;
        $cam2 = " OR $campo2 like '%$palabra%' ";
    }
    
    $sqlBusca = "SELECT id, $campo1 $cam1 
                   FROM $tabla 
                  WHERE $campo1 LIKE '%$palabra%' $cam2 OR id = '$palabra'";
    $resBusca = $conexion->filtro($sqlBusca);
    $canBusca = $conexion->numeroRegistros($resBusca);
    $datosBus = array();

    if($canBusca > 0){
        while ($rowBusca = mysqli_fetch_array($resBusca)){
            if($campo2 != ''){
                array_push($datosBus, $rowBusca['id'].'**'.$rowBusca[$campo1].' '.$rowBusca[$campo2]);
            }else{
                array_push($datosBus, $rowBusca['id'].'**'.$rowBusca[$campo1]);
            }
        }
    }else{
        array_push($datosBus, '**No se encontraron registros');
    }

    echo json_encode($datosBus);
}




?>