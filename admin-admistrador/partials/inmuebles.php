<?php
include("admin/partials/funciones.php");
include 'header.php';

$where = '';

if (isset($_REQUEST['btnBuscar'])) {
    
    if($_REQUEST['txtBPal'] != '')
        $where .= " AND ( titulo LIKE '%".$_REQUEST['txtBPal']."%' OR detalle LIKE '%".$_REQUEST['txtBPal']."%' )";

    if($_REQUEST['selBCat'] != '')
        $where .= " AND id_categoria = ".$_REQUEST['selBCat'];
    
    if($_REQUEST['selBCiu'] != '')
        $where .= " AND ciudad = ".$_REQUEST['selBCiu'];

    $titulo = "BUSQUEDA";
}


if (isset($_REQUEST['btnBuscar2'])) {

    if($_REQUEST['txtBCod'] != '')
        $where .= " AND id = ".$_REQUEST['txtBCod'];

    $titulo = "BUSQUEDA";
}

if (isset($_REQUEST['selOrd'])) {
    $order = $_REQUEST['selOrd'];
}else{
    $order = "";
}


if (isset($_REQUEST['idT'])) {    
    $datoCat = busquedaIndividual("inmobiliaria_categoria", "id = ".$_REQUEST['idT']);
    $titulo  = "INMUEBLES ".strtoupper($datoCat['titulo']);
    $where  .= " AND id_categoria = ".$_REQUEST['idT'];
}

echo $sqlInm = "SELECT * FROM inmobiliaria_inmuebles WHERE 1 = 1 $where $order";
$resInm = busquedaSQL($sqlInm);
$canInm = 0;

$proyectoNue  = busquedasGenerales("inmobiliaria_articulo", "WHERE id_seccion = 1 AND id_categoria = 1 AND estado = 1", "ORDER BY fecha_registro DESC LIMIT 0, 5");

?>


<div id="seccCont" class="divAlineado">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="row">
            <div class="container">

                <div id="barrBusq">
                    <h2><?php echo $titulo; ?></h2>
                    <form name="formInm" id="formInm" method="post" action="inmuebles.php">
                        
                        <input type="hidden" name="idT"     value="<?php echo $_REQUEST['idT'];?>">
                        <input type="hidden" name="txtBPal" value="<?php echo $_REQUEST['txtBPal'];?>">
                        <input type="hidden" name="selBCat" value="<?php echo $_REQUEST['selBCat'];?>">
                        <input type="hidden" name="selBCiu" value="<?php echo $_REQUEST['selBCiu'];?>">
                        <input type="hidden" name="txtBCod" value="<?php echo $_REQUEST['txtBCod'];?>">
                        <select name="selOrd" onchange="ordenar()" style="width: 450px">
                            <option value="">Ordenar inmuebles por: </option>                    
                            <option value="ORDER BY valor ASC">Menor Precio </option>
                            <option value="ORDER BY valor DESC">Mayor Precio</option>
                            <option value="ORDER BY id DESC">Mas Reciente</option>
                            <option value="ORDER BY id ASC">Menos Reciente</option>
                        </select>
                    </form>
                </div>

                <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12" id="contIzqIndex">

                    <div id="divContInmuVenta" class="divAlineado">
                        <?php while ($rowInm = mysqli_fetch_array($resInm)) { 
                            $cat = busquedaEspecifica("titulo", "titulo", "inmobiliaria_categoria",     "id = ".$rowInm['id_categoria']);
                            $tip = busquedaEspecifica("titulo", "titulo", "inmobiliaria_tipo_inmueble", "id = ".$rowInm['id_tipo']);
                            $ubi = busquedaEspecifica("titulo", "titulo", "inmobiliaria_ciudad",        "id = ".$rowInm['ciudad']); 
                            $canInm++; ?>                        

                            <div class="col-lg-4 col-md-4 col-xs-12 contInmu">
                                <a href="detalle-inmueble.php?idI=<?php echo $rowInm['id'];?>" title="">
                                    <div class="text-center divHovImg">
                                        <img src="public/img/inmuebles/<?php echo $rowInm['id'];?>/principal.jpg" alt="<?php echo $rowInm['titulo'];?>">
                                    </div>
                                </a>

                                <h3 class="text-center">$ <?php echo number_format($rowInm['valor'], 0, '', '.');?></h3>
                                <div class="descInmu">
                                    <p><?php echo $tip.' '.$cat.' en '.$ubi;?></p>
                                    <p><?php echo $rowInm['area'];?> - Estrato <?php echo $rowInm['estrato'];?></p>
                                    <hr>
                                    <p><?php echo $rowInm['titulo'];?></p>
                                </div>
                            </div>

                        <?php } 

                        if($canInm == 0){ ?>
                            <div class="col-lg-12 col-md-12 col-xs-12 contInmu">
                                <a href="detalle-inmueble.php" title="">NO HAY INMUEBLES DISPONIBLES</a>
                            </div>
                        <?php } ?>

                    </div>
                </div>

                <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12" id="contDerVen">
                    <h2 class="tituContDer">PUBLICIDAD</h2>
                    <div class="text-center">
                        <img src="public/img/publi1.png" alt="">
                    </div>


                    <h2 class="tituContDer">PUBLICIDAD</h2>

                    <div class="text-center">
                        <img src="public/img/publi2.png" alt="">
                    </div>

                    <h2 style="border-bottom: 1px solid;">PROYECTOS NUEVOS</h2>

                    <div class="text-center">
                        <?php include 'carruPubli.php'; ?>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

<?php include 'footer.php'; ?>
<script type="text/javascript">
    function ordenar(){
        $("#formInm").submit();
    }
</script>