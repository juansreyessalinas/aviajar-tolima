<?php
session_start();
require("conexion.php");


/********************************************************************************************************************/
/******************************************* FUNCIONES GENERALES ****************************************************/
/********************************************************************************************************************/

function subir($dir,$ancho,$alto,$archivo,$nombre){
    
    
    $sImagen=$archivo;
    $nWidth=$ancho;
     $nHeight =$alto;

    // Variables
    $sNombre = null;
    $sPath = null;
    $sExt = null;
    $aImage = null;
    $aThumb = null;
    $aImageMarco = null;
    $ImTransparente = null;
    $aSize = null;
    $nWidthMarco = false;
    $nWidthHeight = false;
    $nX = false;
    $nY = false;

    // Obtenemos el nombre de la imagen
    $sNombre = basename( $sImagen );
    // Obtenemos la ruta especificada para buscar la imagen
    $sPath = dirname( $sImagen );
    // Obtenemos la extension de la imagen
     $info_imagen = getimagesize($sImagen);

    $sExt = $info_imagen['mime'];

    // Creamos el directorio thumbs
    if( ! is_dir( $dir ) )
        @mkdir( $dir , 0777, true ) or die( 'The directory could not be created "' . $dir  );

    // Creamos la imagen a partir del tipo
    switch( $sExt )
    {
        // Imagen JPG
        case 'image/jpeg':
            $aImage = @imageCreateFromJpeg( $sImagen );
            break;
        // Imagen GIF
        case 'image/gif':
            $aImage = @imageCreateFromGif( $sImagen );
            break;
        // Imagen PNG
        case 'image/png':
            $aImage = @imageCreateFromPng( $sImagen );
            break;
        // Imagen BMP
        case 'image/wbmp':
            $aImage = @imageCreateFromWbmp( $sImagen );
            break;
        default:
            return 'The type of image sent is not known, please change the format. There are only photos *.jpg, *.gif, *.png 贸 *.bmp.';
            break;
    }

    // Obtenemos el tama帽o de la imagen original
    $aSize = getImageSize( $sImagen );

    // Calculamos las proporciones de la imagen //

    // Obteniendo el alto (Recogiendo ancho y no alto)
    if( $nWidth !== false && $nHeight === false )
        $nHeight = round( ( $aSize[1] * $nWidth ) / $aSize[0] );
    // Obteniendo el ancho (Recogiendo alto y no ancho)
    elseif( $nWidth === false && $nHeight !== false )
        $nWidth = round( ( $aSize[0] * $nHeight ) / $aSize[1] );
    // Obteniendo proporciones (Recogiendo alto y ancho)
    elseif( $nWidth !== false && $nHeight !== false )
    {
        // Guardamos las dimensiones del marco
        $nWidthMarco = $nWidth;
        $nHeightMarco = $nHeight;

      
            $nWidth = round( ( $aSize[0] * $nHeight ) / $aSize[1] );
            $nX = round( ( $nWidthMarco - $nWidth ) / 2 );;
            $nY = 0;
        
    }
    // El ancho y el alto no se han enviado, informamos del error
    elseif( $nWidth === false && $nHeight === false )
        return 'No value specified for width and height of image.';

    // La nueva imagen reescalada
    $aThumb = imageCreateTrueColor( $nWidth, $nHeight );

    // Reescalamos
    imageCopyResampled( $aThumb, $aImage, 0, 0, 0, 0, $nWidth, $nHeight, $aSize[0], $aSize[1] );

    // Si tenemos que crear el marco
    if( $nWidthMarco !== false && $nHeightMarco !== false )
    {
        // El marco
        $aImageMarco = imageCreateTrueColor( $nWidthMarco, $nHeightMarco );

        // Establecemos la imagen de fondo transparente
        imageAlphaBlending( $aImageMarco, false );
        imageSaveAlpha( $aImageMarco, true );

        // Establecemos el color transparente (negro)
        $ImTransparente = imageColorAllocateAlpha( $aImageMarco, 0, 0, 0, 0xff/2 );

        // Ponemos el fondo transparente
        imageFilledRectangle( $aImageMarco, 0, 0, $nWidthMarco, $nHeightMarco, $ImTransparente );

        // Combinamos las imagenes
        imageCopyResampled( $aImageMarco, $aThumb, $nX, $nY, 0, 0, $nWidth, $nHeight, $nWidth, $nHeight );

        // Cambiamos la instancia
        $aThumb = $aImageMarco;
    }

    // Salvamos
    imagePng( $aThumb, $dir.'/'.$nombre );

    // Liberamos
    imageDestroy( $aImage );
    imageDestroy( $aThumb );
    
    
}


/**
 * [FUNCION elRandom QUE RETORNA UN VALOR RANDOM]
 */
function elRandom(){ 
    return '?'.rand(1,656565); 
}


/**
 * [crearDato FUNCION QUE CREAR UN REGISTRO]
 * @param  [type] $tabla   [VARIABLE QUE TRAE EL NOMBRE DE LA TABLA]
 * @param  [type] $campos  [VARIABLE QUE TRAE LOS CAMPOS QUE SE VAN A REGISTRAR]
 * @param  [type] $valores [VARIABLE QUE TRAE LOS VALORES QUE SE VAN A REGISTRAR]
 */
function crearDato($tabla, $campos, $valores){
    $conexion = new conexion();
    $sqlInser = "INSERT INTO $tabla($campos) VALUES($valores)";
    $resInser = $conexion->ejecutaId($sqlInser);
    return $resInser;
}


/**
 * [actualizarDatos FUNCION QUE ACTUALIZA LOS DATOS DE UNA TABLA]
 * @param  [type] $tabla  [VARIABLE QUE TRAE EL NOMBRE DE LA TABLA]
 * @param  [type] $campos [VARIABLE QUE TRAE LOS CAMPOS A MODIFICAR]
 * @param  [type] $where  [VARIABLE QUE TRAE EL CAMPO DE BUSQUEDA]
 */
function actualizarDatos($tabla, $campos, $where){
    $conexion = new conexion();
    $conexion->actualizar($tabla, $campos, $where);
}


/**
 * [busquedasGenerales FUNCION QUE BUSCA Y DEVUELVE TODOS LOS RESULTADOS DE UNA CONSULTA]
 * @param  [string] $table [VARIABLE QUE TRAE EL NOMBRE DE LA TABLA]
 * @param  [string] $where [VARIABLE QUE TRAE LOS CAMPOS DE BUSQUEDA]
 * @param  [string] $order [VARIABLE QUE TRAE EL ORDEN EN QUE SE VAN A MOSTRAR LOS RESULTADOS]
 */
function busquedasGenerales($table, $where = '', $order = ''){
    $conexion = new conexion();
    $sqlBusca = "SELECT * FROM $table $where $order";
    $resBusca = $conexion->filtro($sqlBusca);
    return $resBusca;
}


/**
 * [busquedaIndividual FUNCION QUE BUSCA Y DEVUELVE UN SOLO REGISTRO DE UNA CONSULTA]
 * @param  [string] $table [VARIABLE QUE TRAE EL NOMBRE DE LA TABLA]
 * @param  [string] $where [VARIABLE QUE TRAE LOS CAMPOS DE BUSQUEDA]
 * @param  [string] $order [VARIABLE QUE TRAE EL ORDEN EN QUE SE VAN A MOSTRAR LOS RESULTADOS]
 */
function busquedaIndividual($table, $where = '', $order = ''){
    $conexion = new conexion();
    $rowBusca = $conexion->consulta($table, $where, $order);
    return $rowBusca;
}


/**
 * [busquedaEspecifica FUNCION QUE BUSCA Y DEVUELVE UN DATO EN ESPECIFICO]
 * @param  [string] $campo1 [VARIABLE QUE TRAE LOS CAMPOS DE LA TABLA A BUSCAR]
 * @param  [string] $campo2 [VARIABLE QUE TRAE LOS CAMPOS A SACAR DE LA TABLA A BUSCAR]
 * @param  [string] $table  [VARIABLE QUE TRAE EL NOMBRE DE LA TABLA]
 * @param  [string] $where  [VARIABLE QUE TRAE LOS CAMPOS DE BUSQUEDA]
 * @param  [string] $order  [VARIABLE QUE TRAE EL ORDEN EN QUE SE VAN A MOSTRAR LOS RESULTADOS]
 */
function busquedaEspecifica($campo1, $campo2, $table, $where = ''){
    $conexion = new conexion();
    $sqlBusca = "SELECT $campo1 FROM $table WHERE $where";
    $resBusca = $conexion->filtro($sqlBusca);
    $rowBusca = $conexion->filas($resBusca);
    return $rowBusca[$campo2];
}


/**
 * [busquedaSQL FUNCION QUE BUSCA Y DEVUELVE UN SOLO REGISTRO DE UNA CONSULTA]
 * @param  [string] $sql [VARIABLE QUE TRAE EL SQL A BUSCAR]
 */
function busquedaSQL($sql){
    $conexion = new conexion();
    $rowBusca = $conexion->filtro($sql);
    return $rowBusca;
}


/**
 * [busquedaCantidad FUNCION QUE DEVUELVE LA CANTIDAD DE REGISTROS ENCONTRADOS]
 * @param  [string] $table [VARIABLE QUE TRAE EL NOMBRE DE LA TABLA]
 * @param  [string] $where [VARIABLE QUE TRAE LOS CAMPOS DE BUSQUEDA]
 * @param  [string] $order [VARIABLE QUE TRAE EL ORDEN EN QUE SE VAN A MOSTRAR LOS RESULTADOS]
 */
function busquedaCantidad($table, $where = '', $order = ''){
    $conexion = new conexion();
    $sqlBusca = "SELECT * FROM $table $where $order";
    $resBusca = $conexion->filtro($sqlBusca);
    $canBusca = $conexion->numeroRegistros($resBusca);
    return $canBusca;
}


/**
 * [FUNCION options PARA SACAR LA INFORMACION DE UNA TABLA PARA COLOCARLOS EN LOS OPTION DE UN SELECT]
 */
function options($seleccion, $tabla, $where, $nombre, $nombre2 = ''){
    $conexion = new conexion();
    $sql      = "SELECT * FROM $tabla $where ORDER BY $nombre ASC";
    $res      = $conexion->filtro($sql);
    $html     = '<option value=""></option>';

    while ($row = mysqli_fetch_array($res)) {

        if($nombre2 != '')
            $sacar = $row[$nombre].' '.$row[$nombre2];
        else
            $sacar = $row[$nombre];



        if( $row['id'] == $seleccion){
            $html .= '<option value="'.$row['id'].'" selected>'.($sacar).'</option>';
        }else{
            $html .= '<option value="'.$row['id'].'">'.($sacar).'</option>';
        }
    }

    $conexion->cerrarfiltro($res);

    echo $html;
}


/**
 * [FUNCION optionsSQL PARA SACAR LA INFORMACION DE UNA TABLA PARA COLOCARLOS EN LOS OPTION DE UN SELECT]
 */
function optionsSQL($seleccion, $sql, $codigo, $nombre, $nombre2 = ''){
    $conexion = new conexion();
    $res      = $conexion->filtro($sql);
    $html     = '<option value=""></option>';

    while ($row = mysqli_fetch_array($res)) {

        if($nombre2 != '')
            $sacar = $row[$nombre].' '.$row[$nombre2];
        else
            $sacar = $row[$nombre];


        if( $row[$codigo] == $seleccion){
            $html .= '<option value="'.$row[$codigo].'" selected>'.($sacar).'</option>';
        }else{
            $html .= '<option value="'.$row[$codigo].'">'.($sacar).'</option>';
        }
    }

    $conexion->cerrarfiltro($res);

    echo $html;
}


/**
 * [FUNCION optOrder PARA SACAR LA INFORMACION DE UNA TABLA PARA COLOCARLOS EN LOS OPTION DE UN SELECT]
 */
function optOrder($seleccion, $tabla, $where, $nombre, $nombre2 = '', $order = ''){
    $conexion = new conexion();
    $sql      = "SELECT * FROM $tabla $where $order";
    $res      = $conexion->filtro($sql);
    $html     = '<option value=""></option>';

    while ($row = mysqli_fetch_array($res)) {

        if($nombre2 != '')
            $sacar = $row[$nombre].' '.$row[$nombre2];
        else
            $sacar = $row[$nombre];



        if( $row['id'] == $seleccion){
            $html .= '<option value="'.$row['id'].'" selected>'.utf8_encode(($sacar)).'</option>';
        }else{
            $html .= '<option value="'.$row['id'].'">'.utf8_encode(($sacar)).'</option>';
        }
    }

    $conexion->cerrarfiltro($res);

    echo $html;
}


/**
 * [buscarCorreo description]
 * @param  [type] $correo [description]
 * @return [type]         [description]
 */
function buscarCorreo($correo){
    $conexion = new conexion();
    $sqlAdm   = "SELECT * FROM administrador WHERE email = '$correo' ";
    $resAdm   = $conexion->filtro($sqlAdm);
    $numAdm   = $conexion->numeroRegistros($resAdm);

    if($numAdm > 0){        
        $rowAdm = $conexion->filas($resAdm);
        $elMensaje = "1**".$rowAdm['id'].'**'.$correo;

    }else{
        
        $sqlDoc = "SELECT * FROM docente WHERE email = '$correo' ";
        $resDoc = $conexion->filtro($sqlDoc);
        $numDoc = $conexion->numeroRegistros($resDoc);

        if($numDoc > 0){
            $rowDoc = $conexion->filas($resDoc);
            $elMensaje = "2**".$rowDoc['id'].'**'.$correo;
        }else{

            $sqlEst = "SELECT * FROM estudiante WHERE email = '$correo' ";
            $resEst = $conexion->filtro($sqlEst);
            $numEst = $conexion->numeroRegistros($resEst);

            if($numEst > 0){  
                $rowEst = $conexion->filas($resEst);
                $elMensaje = "3**".$rowEst['id'].'**'.$correo;
            }else{

                $elMensaje = '0';
            }
        }
    }

    return $elMensaje;
}


/**
 * [generarNuevoTexto FUNCION QUE GENERA UN TEXTO RANDOM]
 * @param  [type] $numerodeletras [VARIABLE QUE TRAE LA CANTIDAD DE LETRAS A GENERAR]
 */
function generarNuevoTexto($numerodeletras){
    $caracteres = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890_-"; //posibles caracteres a usartexto
    $cadena     = "";  //variable para almacenar la cadena generada

    for($i = 0; $i < $numerodeletras; $i++){
        $cadena .= substr($caracteres, rand(0, strlen($caracteres)), 1); /*Extraemos 1 caracter de los caracteres entre el rango 0 a Numero de letras que tiene la cadena */
    }

    return $cadena;
}


/**
 * [sumarCon funcion que suma la cantidad de visitas a un inmueble]
 * @param  [type] $idInm [variable que trae el id del inmueble a sumar la visita]
 */
function sumarCon($idInm){
    $conexion = new conexion();
    $sqlAdm   = "SELECT * FROM inmobiliaria_inmuebles WHERE id = '$idInm' ";
    $resAdm   = $conexion->filtro($sqlAdm);
    $resAdm   = $conexion->filas($resAdm);
    $contador = $resAdm['contador'] + 1;
    actualizarDatos("inmobiliaria_inmuebles", "contador = ".$contador, "id = ".$idInm);
    return $contador;
}







?>