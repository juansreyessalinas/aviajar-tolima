<?php
include("partials/funciones.php");
//Verificando si esta logueado
if (!isset($_SESSION['usuPA_admin'])) {
    header('Location: index.php');
}

//echo md5("123456");

if (isset($_REQUEST['id'])) {
    $dato = busquedaIndividual("tienda_producto", "id = ".$_REQUEST['id']);
}
$todos = busquedasGenerales("tienda_producto", "", "ORDER BY id ASC");
?>
<!DOCTYPE html>
<html>
    <head>
        <?php include('partials/header.php');  ?>
        <style type="text/css">
            div.jHtmlArea .ToolBar ul li a.custom_disk_button{
                background: url(images/disk.png) no-repeat;
                background-position: 0 0;
            }            
            div.jHtmlArea { border: solid 1px #ccc; }
        </style>
    </head>
    
    <body class="fixed-left">
        <div id="wrapper">
            <?php include('partials/menu_horizontal.php');  ?>
        
            <div class="content-page">
                <div class="content">
                    <div class="container">
   
                        <!-- Donde Estoy -->
                        <div class="row">
                            <div class="col-sm-12">
                                <h4 class="page-title">Planes</h4>
                                <ol class="breadcrumb">
                                    <li><a href="index2.php">Inicio</a></li>
                                    <!-- <li><a href="inmobiliaria_seccion.php">Sección</a></li> -->
                                    <li><a href="tienda_categoria.php">Categorias</a></li>
                                    <li class="active">Planes</li>
                                </ol>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-12">
                                <div class="card-box">
                                    <form action="#" method="post" enctype="multipart/form-data"  class="form-horizontal">
                                        <input type="hidden" name="txtId"      id="txtId"      value="<?php if(isset($dato['id'])) echo $dato['id'];?>" />
                                      
                                        <div class="form-group">
                                         

                                            <div class="col-lg-3">
                                                <label>Nombre del Plan:</label>
                                                <input type="text" class="form-control" name="txtTitulo" id="txtTitulo" required="required" placeholder="Plan 1" value="<?php if(isset($dato['titulo'])) echo $dato['titulo'];?>">
                                            </div>
                                            <div class="col-lg-3">
                                                <label>Valor:</label>
                                                <input type="text" class="form-control" name="valor" id="valor" required="required"  value="<?php if(isset($dato['valor'])) echo $dato['valor'];?>">
                                            </div>
                                            <div class="col-lg-3">
                                                <label>Categoria:</label>
                                                <select class="selectpicker" data-style="btn-white" name="id_categoria" id="id_categoria" required="required">
                                                    <?php optOrder($dato['id_categoria'], "tienda_categoria", "WHERE estado = 1", "titulo", "", "ORDER BY titulo ASC"); ?>
                                                </select>
                                            </div>
                                             <div class="col-lg-3">
                                                <label>Duracion:</label>
                                                <input type="text" class="form-control" name="duracion" id="duracion" required="required"  value="<?php if(isset($dato['duracion'])) echo $dato['duracion'];?>">
                                            </div>
                                            <div class="col-lg-3">
                                                <label>Informes:</label>
                                                <input type="text" class="form-control" name="informes" id="informes" required="required"   value="<?php if(isset($dato['informes'])) echo $dato['informes'];?>">
                                            </div>
                                        </div>
                                      

                                        <div class="form-group">
                                           
                                            
                                             <div class="col-lg-3">
                                                <label>Imágen del Plan:</label> <br> <label>(350ancho x 520alto)</label>
                                                <input type="file" name="txtArchivo" id="txtArchivo" class="filestyle" data-buttontext="Seleccione" data-buttonname="btn-inverse"  >
                                            </div>
                                            
                                            <div class="col-lg-3">
                                                <label>Imágen Interna 1:</label> <br> <label>(1600ancho x 530alto)</label>
                                                <input type="file" name="txtArchivo1" id="txtArchivo1" class="filestyle" data-buttontext="Seleccione" data-buttonname="btn-inverse"  >
                                            </div>
                                            
                                            <div class="col-lg-3">
                                                <label>Imágen  Interna 2:</label> <br> <label>(700ancho x alto deseado)</label>
                                                <input type="file" name="txtArchivo2" id="txtArchivo2" class="filestyle" data-buttontext="Seleccione" data-buttonname="btn-inverse"  >
                                            </div>
                                            
                                          
                                            <div class="col-lg-3">
                                                <label>Estado:</label>
                                                <div class="radio radio-primary">
                                                    <input type="radio" name="rdoActivo" id="rdoActivo" required value="1" <?php if(isset($dato['estado']) && $dato['estado'] == 1) echo "checked";?>>
                                                    <label for="radio3">Activo</label> &nbsp; &nbsp; &nbsp; &nbsp;                                                  
                                                    <input type="radio" name="rdoActivo" id="rdoActivo" required value="2" <?php if(isset($dato['estado']) && $dato['estado'] == 2) echo "checked";?>>
                                                    <label for="radio3">Inactivo</label>
                                                </div>                                             
                                            </div>
                                            <div class="col-lg-3">
                                                <label>Recomendado:</label>
                                                <div class="radio radio-primary">
                                                    <input type="radio" name="recomendado" id="recomendado" required value="1" <?php if(isset($dato['recomendado']) && $dato['recomendado'] == 1) echo "checked";?>>
                                                    <label for="radio3">Si</label> &nbsp; &nbsp; &nbsp; &nbsp;                                                  
                                                    <input type="radio" name="recomendado" id="recomendado" required value="2" <?php if(isset($dato['recomendado']) && $dato['recomendado'] == 2) echo "checked";?>>
                                                    <label for="radio3">No</label>
                                                </div>                                             
                                            </div>
                                            <div class="col-lg-3">
                                                <label>Destacado:</label>
                                                <div class="radio radio-primary">
                                                    <input type="radio" name="destacado" id="destacado" required value="1" <?php if(isset($dato['destacado']) && $dato['destacado'] == 1) echo "checked";?>>
                                                    <label for="radio3">Si</label> &nbsp; &nbsp; &nbsp; &nbsp;                                                  
                                                    <input type="radio" name="destacado" id="destacado" required value="2" <?php if(isset($dato['destacado']) && $dato['destacado'] == 2) echo "checked";?>>
                                                    <label for="radio3">No</label>
                                                </div>                                             
                                            </div>
                                             <div class="col-lg-3">
                                                <label>Mi Tierra:</label>
                                                <div class="radio radio-primary">
                                                    <input type="radio" name="tierra" id="tierra" required value="1" <?php if(isset($dato['tierra']) && $dato['tierra'] == 1) echo "checked";?>>
                                                    <label for="radio3">Si</label> &nbsp; &nbsp; &nbsp; &nbsp;                                                  
                                                    <input type="radio" name="tierra" id="tierra" required value="2" <?php if(isset($dato['tierra']) && $dato['tierra'] == 2) echo "checked";?>>
                                                    <label for="radio3">No</label>
                                                </div>                                             
                                            </div>
                                        </div>

                               
    
                                        <div class="form-group">
                                     
                                            <div class="col-lg-12">
                                                <textarea name="txtDetalle" id="txtDetalle" style="width:100%" rows="15"><?php if(isset($dato['descripcion'])) echo $dato['descripcion'];?></textarea>
                                            </div>
                                            <div class="col-lg-12 text-center">
                                                <button class="btn btn-primary waves-effect waves-light" type="submit" name="btnGuardar" id="btnGuardar">Guardar</button>
                                                <a href="tienda_productos.php" class="btn btn-default waves-effect waves-light m-l-5">Cancel</a>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-12">
                                <div class="card-box">
                                    <table id="datatable" class="table table-striped table-bordered">
                                        <thead>
                                            <tr>
                                                <th width="50px" class="text-center">ID</th>
                                              
                                                <th class="text-center">Plan</th>
                                                
                                                <th width="80px" class="text-center">Estado</th>
                                                <th width="250px" class="text-center">Acción</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php while ($rowTodos = mysqli_fetch_array($todos)){ 
                                                if ($rowTodos['estado'] == 1)
                                                    $elEstado = '<span class="label label-table label-success" onclick="inactivar('."'tienda_producto'".', '."'".$rowTodos['id']."'".', 2, '."'tienda_productos.php'".')" style="cursor:pointer">Activo</span>';
                                                else
                                                    $elEstado = '<span class="label label-table label-danger"  onclick="inactivar('."'tienda_producto'".', '."'".$rowTodos['id']."'".', 1, '."'tienda_productos.php'".')" style="cursor:pointer">Inactivo</span>';                                               

                                                if ($rowTodos['recomendado'] == 1)
                                                    $recomen1 = '<span class="label label-table label-success">Si</span>';
                                                else
                                                    $recomen1 = '<span class="label label-table label-danger">No</span>';                                               

                                                if ($rowTodos['recomendado2'] == 1)
                                                    $recomen2 = '<span class="label label-table label-success">Si</span>';
                                                else
                                                    $recomen2 = '<span class="label label-table label-danger">No</span>';                                               
                                                ?>
                                                <tr>
                                                    <td class="text-center"><?php echo $rowTodos['id'];?></td>
                                                   
                                                    <td>
                                                        <ins class="text-primary" style="cursor:pointer;" data-toggle="modal" data-target="#modal_<?php echo $rowTodos['id'];?>"><?php echo $rowTodos['titulo'];?></ins>
                                                        <div id="modal_<?php echo $rowTodos['id'];?>" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                                                            <div class="modal-dialog">
                                                                <div class="modal-content p-0 b-0">
                                                                    <div class="panel panel-color panel-primary">
                                                                        <div class="panel-heading"> 
                                                                            <button type="button" class="close m-t-5" data-dismiss="modal" aria-hidden="true">×</button> 
                                                                            <h3 class="panel-title">Imagen Principal</h3> 
                                                                        </div> 
                                                                        <div class="panel-body"> 
                                                                            <img src="../public/img/productos/<?php echo $rowTodos['id'];?>/principal.png" style="max-width: 300px; max-height: 350px">
                                                                        </div> 
                                                                    </div>
                                                                </div>
                                                           </div>
                                                        </div>
                                                    </td>
                                                   
                                                    <td class="text-center"><?php echo $elEstado;?></td>
                                                    <td class="text-center">
                                                        
                                                        <a class="btn btn-inverse waves-effect waves-light btn-xs" href="tienda_productos.php?id=<?php echo $rowTodos['id'];?>"><i class="ion-edit"></i> Editar</a>
                                                        <a class="btn btn-inverse waves-effect waves-light btn-xs" onclick="eliminarF('tienda_producto', <?php echo $rowTodos['id'];?>, 'tienda_productos.php','../../public/img/productos/<?php echo $rowTodos['id'];?>')"><i class="glyphicon glyphicon-trash"></i> Eliminar</a>
                                                    </td>
                                                </tr>
                                            <?php } ?>                                            
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <footer class="footer">2016 © Creasotol</footer>
                </div>        
            </div>
        </div>
        <?php include("partials/footer.php"); ?>
        <script type="text/javascript">
            $('.selectpicker').selectpicker();
        </script>  
        <script>
         $(document).ready(function (){
          $('#valor').keyup(function (){
            this.value = (this.value + '').replace(/[^0-9]/g, '');
          });
        });
    </script>
        <script type="text/javascript">
        
        
        
            $(document).ready(function () {
                $("#txtDetalle").htmlarea();
            });
            function eliminarF(tabla, id, pagina,carpeta){
  if (confirm("Esta seguro que desea eliminar el dato?")) {
    $.ajax({
      method: "POST",
      url: "partials/metodos.php",
      data: { accion: "eliminarF", laTab: tabla, elId: id ,carpeta:carpeta}
    })
    .done(function(msg) {
     
      setInterval(function(){ location.href = pagina }, 2000);
    });
  }
}

        </script>
        
    </body>
</html>
<?php 
if (isset($_REQUEST['btnGuardar'])) {
    $id  = $_REQUEST['txtId'];
    $cat = $_REQUEST['id_categoria'];
    $tit = $_REQUEST['txtTitulo'];    
    $cod = $_REQUEST['txtCodigo'];
    $sto = $_REQUEST['txtStock'];
    $pAn = $_REQUEST['txtPrecioAnt'];
    $pAh = $_REQUEST['txtPrecioAho'];
    $des = $_REQUEST['txtDescuento'];
    $col = $_REQUEST['txtColor'];
    $tal = $_REQUEST['txtTalla'];
    $vid = $_REQUEST['txtVideo'];
    $tex = $_REQUEST['txtDetalle'];
    $sta = $_REQUEST['rdoActivo'];
    $rec = $_REQUEST['rdoReco'];
    $re2 = $_REQUEST['rdoReco2'];
    $pro = $_REQUEST['rdoPromo'];
    $valor = $_REQUEST['valor'];
    
    $duracion = $_REQUEST['duracion'];
    $informes = $_REQUEST['informes'];
     $tierra = $_REQUEST['tierra'];
    
     $recomendado = $_REQUEST['recomendado'];
     $destacado = $_REQUEST['destacado'];

    if ($id == '') {
        $mensaje = "Se creo el producto.";
        $campos  = " titulo, descripcion, estado, fecha_registro,id_categoria,valor,informes,duracion,recomendado,destacado,tierra";
        $valores = " '".$tit."', '".$tex."', '".$sta."', '".date("Y-m-d H:i:s")."', '".$cat."', '".$valor."', '".$duracion."', '".$informes."', '".$recomendado."','".$destacado."','".$tierra."'";
        $id = crearDato("tienda_producto", $campos, $valores);
    }else{
        $mensaje = "Se modificaron los datos del producto.";
        $campos  = " titulo = '".$tit."',  descripcion = '".$tex."', estado = '".$sta."', id_categoria = '".$cat."', valor = '".$valor."', duracion = '".$duracion."', informes = '".$informes."', recomendado = '".$recomendado."', destacado = '".$destacado."', tierra = '".$tierra."'";
        actualizarDatos("tienda_producto", $campos, "id = ".$id);
    }

    $carpeta = "../public/img/productos/".$id."/";

    if(!file_exists($carpeta)){
        mkdir($carpeta, 0777);
        chmod($carpeta, 0777);
    }

    //SE CAMBIA LA IMAGEN PREDETERMINADA SI SE CARGO UNO NUEVO
    if($_FILES['txtArchivo']['name'] != ''){
        $principal = "../public/img/productos/".$id."/principal.png";
        if(copy($_FILES['txtArchivo']['tmp_name'], $principal) ){ 
            $mensaje .= ' Se cargo la imagen principal. ';
        }else{
            $mensaje .= ' Se produjo un error al cargar la imagen principal. ';
        }
    }
    
    
    if($_FILES['txtArchivo1']['name'] != ''){
        $principal = "../public/img/productos/".$id."/principal1.png";
        if(copy($_FILES['txtArchivo1']['tmp_name'], $principal) ){ 
            $mensaje .= ' Se cargo la imagen principal. ';
        }else{
            $mensaje .= ' Se produjo un error al cargar la imagen principal. ';
        }
    }
    
    if($_FILES['txtArchivo2']['name'] != ''){
        $principal = "../public/img/productos/".$id."/principal2.png";
        if(copy($_FILES['txtArchivo2']['tmp_name'], $principal) ){ 
            $mensaje .= ' Se cargo la imagen principal. ';
        }else{
            $mensaje .= ' Se produjo un error al cargar la imagen principal. ';
        }
    }
    


    echo "<script> 
            $.Notification.notify('success','top left', 'Exito!', '".$mensaje."');
            setInterval(function(){ location.href = 'tienda_productos.php' }, 3000);
          </script>";
}
?>