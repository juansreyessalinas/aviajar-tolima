<?php

error_reporting(E_ALL);

include("partials/funciones.php");

//Verificando si esta logueado

if (!isset($_SESSION['usuPA_admin'])) {

    header('Location: index.php');

}



if (isset($_REQUEST['id'])) {

    $id      = $_REQUEST['id'];

    $dato    = busquedaIndividual("inmobiliaria_inmuebles", "id = ".$id);

    $carpeta = "../public/img/inmuebles/".$id."/";

}

?>

<!DOCTYPE html>

<html>

    <head>

        <?php include('partials/header.php');  ?>

    </head>

    <body class="fixed-left">

        <div id="wrapper">

            <?php include('partials/menu_horizontal.php');  ?>       

            <div class="content-page">

                <div class="content">

                    <div class="container">

                        <div class="row">

                            <div class="col-sm-12">

                                <h4 class="page-title">Imágenes de: <i><?php echo $dato['titulo'];?></i></h4>

                                <ol class="breadcrumb">

                                    <li><a href="index2.php">Inicio</a></li>

                                    <li class="active"><a href="inmobiliaria_inmueble.php?id=<?php echo $id;?>"><?php echo $dato['titulo'];?></a></li>

                                </ol>

                            </div>

                        </div>



                        <div class="row">

                            <div class="col-lg-12">

                                <div class="card-box">

                                    <form action="#" name="formImg" id="formImg" method="post" enctype="multipart/form-data">

                                        <input type="hidden" name="id" id="id" value="<?php echo $id;?>" />

                                        <div class="form-group">

                                            <div class="col-lg-1"><label>Imágen:</label></div>

                                            <div class="col-lg-11">                                                
                                                900px ancho - 600px alto
                                                <input type="file" name="txtArchivo" id="txtArchivo" class="filestyle" accept="image/jpeg" data-buttontext="Seleccione" data-buttonname="btn-inverse" onchange="cargarInmueble(this.value, 'txtArchivo')">

                                            </div>

                                            <br>

                                        </div>

                                    </form>

                                </div>

                            </div>

                        </div>



                        <div class="row">

                            <?php if(file_exists($carpeta)){ 

                                $dir = dir($carpeta);

                                while ($archivo = $dir->read()){

                                    if($archivo != '' && $archivo != '.' && $archivo != '..' && $archivo != 'principal.png' && $archivo != 'principal.jpg'){ ?>

                                        <div class="col-md-3">

                                            <div class="thumbnail">

                                                <img src="../public/img/inmuebles/<?php echo $id;?>/<?php echo $archivo;?>" class="img-responsive">

                                                <div class="caption text-center">

                                                    <a class="btn btn-danger waves-effect waves-light" role="button" onclick="eliminarArchivo('../public/img/inmuebles/<?php echo $id;?>/<?php echo $archivo;?>', 'inmobiliaria_inmuebles_galeria.php?id=<?php echo $id;?>')"><i class="glyphicon glyphicon-trash"></i> Eliminar</a>

                                                </div>

                                            </div>

                                        </div>

                                    <?php

                                    }

                                }

                            } ?>

                        </div>



                    </div>

                    <footer class="footer">2016 © Creasotol</footer>

                </div>        

            </div>

        </div>

        <?php include("partials/footer.php"); ?>

    </body>

</html>



<?php  



//SE GUARDA LA IMAGEN

if($_FILES['txtArchivo']['name'] != ''){

    $id  = $_REQUEST['id'];

    $laF = date("Y-m-d-H-i-s").".jpg";

    $img = "../public/img/inmuebles/".$id."/".$laF.".jpg";

    
     $logo = "../public/img/inmuebles/".$id."";
        
     subir($logo,900,600, $_FILES['txtArchivo']['tmp_name'],$laF);
    
    
    



    echo "<script>

            $.Notification.notify('success','top left', 'Exito!', '".$mensaje."');

            setInterval(function(){ location.href = 'inmobiliaria_inmuebles_galeria.php?id=".$id."' }, 3000);

          </script>";

}



?>