<?php
include("partials/funciones.php");

//Verificando si esta logueado
if (!isset($_SESSION['usuPA_admin'])) {
    header('Location: index.php');
}

if (isset($_REQUEST['id'])) {
    $dato = busquedaIndividual("marcas", "id = ".$_REQUEST['id']);
}
$todos = busquedasGenerales("marcas", "", "ORDER BY id ASC");
?>
<!DOCTYPE html>
<html>
    <head>
        <?php include('partials/header.php');  ?>
    </head>
    <body class="fixed-left">
        <div id="wrapper">
            <?php include('partials/menu_horizontal.php');  ?>       
            <div class="content-page">
                <div class="content">
                    <div class="container">  

                        <!-- Donde Estoy -->
                        <div class="row">
                            <div class="col-sm-12">
                                <h4 class="page-title">Servicio</h4>
                                <ol class="breadcrumb">
                                    <li><a href="index2.php">Inicio</a></li>
                                    <!-- <li><a href="inmobiliaria_seccion.php">Sección</a></li> -->
                                    <li class="active">Servicio</li>
                                </ol>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="card-box">
                                    <form action="#" method="post" enctype="multipart/form-data">
                                        <input type="hidden" name="txtId" id="txtId" value="<?php if(isset($dato['id'])) echo $dato['id'];?>" />
                                        <div class="form-group">
                                            
                                            <div class="col-lg-4">
                                                <label>Servicio:</label>
                                                <input type="text" class="form-control" name="txtTitulo" id="txtTitulo" placeholder="Nombre" required="required" value="<?php if(isset($dato['titulo'])) echo $dato['titulo'];?>">
                                            </div>
                                            <div class="col-lg-4">
                                                <label>Imagen de peque�0�9a: </label>
                                                <input type="file" name="txtArchivo" id="txtArchivo" class="filestyle" data-buttontext="Seleccione" data-buttonname="btn-inverse" >
                                            </div>
                                            <div class="col-lg-4">
                                                <label>Imagen de grande: </label>
                                                <input type="file" name="txtArchivo1" id="txtArchivo1" class="filestyle" data-buttontext="Seleccione" data-buttonname="btn-inverse" >
                                            </div>
                                      
                                       
                                           
                                     
                                                 
                                        </div>
                                        <div class="form-group">
                                             <div class="col-lg-12">
                                                <label>Descripcion:</label>
                                                <textarea class="form-control" name="txtDescripcion" id="txtDescripcion"><?php if(isset($dato['descr'])) echo $dato['descr'];?></textarea>
                                            </div>
                                         </div>                                        
                                        <div class="form-group text-center ">
                                            
                                            <button class="btn btn-primary waves-effect waves-light" type="submit" name="btnGuardar" id="btnGuardar">Guardar</button>
                                            <a href="marca.php" class="btn btn-default waves-effect waves-light m-l-5">Cancel</a>
                                        </div>
                                    </form>
                               
                            </div>
                        </div>


                       <div class="row">
                            <div class="col-sm-12">
                                <div class="card-box">
                                    <table id="datatable" class="table table-striped table-bordered">
                                        <thead>
                                            <tr>
                                                <th width="80px" class="text-center">ID</th>
                                               
                                                 <th width="150px" class="text-center">Servicio </th>
                                                 <th class="text-center">Descripcion</th>
                                           
                                                <th width="160px" class="text-center">Accion</th>
                                            </tr>
                                        </thead>

                                        <tbody>
                                            <?php while ($rowTodos = mysqli_fetch_array($todos)){ 
                                               if ($rowTodos['estado'] == 1)
                                                    $elEstado = '<span class="label label-table label-success" onclick="inactivar('."'marcas'".', '."'".$rowTodos['id']."'".', 2, '."'marca.php'".')" style="cursor:pointer">Activo</span>';
                                                else
                                                    $elEstado = '<span class="label label-table label-danger"  onclick="inactivar('."'marcas'".', '."'".$rowTodos['id']."'".', 1, '."'marca.php'".')" style="cursor:pointer">Inactivo</span>';
                                                ?>
                                                <tr>
                                                    <td class="text-center"><?php echo $rowTodos['id'];?></td>
                                                    <td><?php echo $rowTodos['titulo'];?></td>
                                                    
                                                     <td><?php echo $rowTodos['descr'];?></td>
                                          
                                                    <td class="text-center">
                                                        <a class="btn btn-inverse waves-effect waves-light btn-xs" href="marca.php?id=<?php echo $rowTodos['id'];?>"><i class="ion-edit"></i> Editar</a>
                                                        <a class="btn btn-inverse waves-effect waves-light btn-xs" onclick="eliminar('marcas', <?php echo $rowTodos['id'];?>, 'marca.php')"><i class="glyphicon glyphicon-trash"></i> Eliminar</a>
                                                    </td>
                                                </tr>
                                            <?php } ?> 
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <footer class="footer">2016 - Creasotol</footer>
                </div>        
            </div>
        </div>
        <?php include("partials/footer.php"); ?>
        <script type="text/javascript">
            $('.selectpicker').selectpicker();
        </script>
    </body>
</html>
<?php 
if (isset($_REQUEST['btnGuardar'])) {
    $id = $_REQUEST['txtId'];

    if ($id == '') {
        $mensaje = "Se creo el servicio.";
        $campos  = "titulo,  descr";
        $valores = "'".$_REQUEST['txtTitulo']."', '".$_REQUEST['txtDescripcion']."'";
        $id=crearDato("marcas", $campos, $valores);
    }else{
        $mensaje = "Se modificaron los datos del el servicio.";
        $campos  = "titulo = '".$_REQUEST['txtTitulo']."', descr= '".$_REQUEST['txtDescripcion']."'";
        actualizarDatos("marcas", $campos, "id = ".$id);
    }

     $carpeta = "../public/img/constructora/".$id."/";

    if(!file_exists($carpeta)){
        mkdir($carpeta, 0777);
        chmod($carpeta, 0777);
    }

    //SE CAMBIA LA IMAGEN PREDETERMINADA SI SE CARGO UNO NUEVO
    if($_FILES['txtArchivo']['name'] != ''){
        $principal = "../public/img/constructora/".$id."/principal.jpg";
        
        if(copy($_FILES['txtArchivo']['tmp_name'], $principal) ){ 
            $mensaje .= ' Se cargo la imagen principal. ';
        }else{
            $mensaje .= ' Se produjo un error al cargar la imagen principal. ';
        }
    }
    if($_FILES['txtArchivo1']['name'] != ''){
        $principal = "../public/img/constructora/".$id."/principal1.jpg";
        
        if(copy($_FILES['txtArchivo1']['tmp_name'], $principal) ){ 
            $mensaje .= ' Se cargo la imagen principal. ';
        }else{
            $mensaje .= ' Se produjo un error al cargar la imagen principal1. ';
        }
    }
   echo "<script> 
            $.Notification.notify('success','top left', 'Exito!', '".$mensaje."');
            setInterval(function(){ location.href = 'marca.php' }, 3000);
          </script>";

}
?>