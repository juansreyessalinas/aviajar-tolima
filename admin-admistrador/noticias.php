<?php
include("partials/funciones.php");
//Verificando si esta logueado
if (!isset($_SESSION['usuPA_admin'])) {
    header('Location: index.php');
}

if (isset($_REQUEST['id'])) {
    $dato = busquedaIndividual("noticias", "id = ".$_REQUEST['id']);
}

$todos = busquedasGenerales("noticias", "", "ORDER BY id ASC");
?>
<!DOCTYPE html>
<html>
    <head>
        <?php include('partials/header.php');  ?>
    </head>
    <body class="fixed-left">
        <div id="wrapper">
            <?php include('partials/menu_horizontal.php');  ?>
        
            <div class="content-page">
                <div class="content">
                    <div class="container">
    
                        <!-- Donde Estoy -->
                        <div class="row">
                            <div class="col-sm-12">
                                <h4 class="page-title">Publicaciones</h4>
                                <ol class="breadcrumb">
                                    <li><a href="index2.php">Inicio</a></li>
                                    <li class="active">Publicaciones</li>
                                </ol>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-12">
                                <div class="card-box">
                                    <form action="#" method="post" enctype="multipart/form-data"  class="form-horizontal">
                                        <input type="hidden" name="txtId" id="txtId" value="<?php if(isset($dato['id'])) echo $dato['id'];?>" />
                                        <div class="form-group">
                                            <div class="col-lg-6">
                                                <label>Titulo:</label>
                                                <input type="text" class="form-control" name="txtTitulo" id="txtTitulo" placeholder="Noticia 1" value="<?php if(isset($dato['titulo'])) echo $dato['titulo'];?>">
                                            </div>
                                            
                                            
                                        </div>
                                        <div class="form-group">
                                            <div class="col-lg-6">
                                                <label>Imágen de Muestra: Ancho maximo 1000. Alto cualquiera</label>
                                                <input type="file" name="txtArchivo" id="txtArchivo" class="filestyle" data-buttontext="Seleccione" data-buttonname="btn-inverse"  onchange="verificarjpg(this.value, 'txtArchivo')">
                                                <br> 
                                            </div>
                                            
                                            <div class="col-lg-3">
                                                <label>Estado:</label>
                                                <div class="radio radio-primary">
                                                    <input type="radio" name="rdoActivo" id="rdoActivo" required value="1"<?php if(isset($dato['estado']) && $dato['estado'] == 1) echo "checked";?>>
                                                    <label for="radio3">Activo</label> &nbsp; &nbsp; &nbsp; &nbsp;
                                                    <input type="radio" name="rdoActivo" id="rdoActivo" required value="2"<?php if(isset($dato['estado']) && $dato['estado'] == 2) echo "checked";?>>
                                                    <label for="radio3">Inactivo</label>
                                                </div>                                                
                                            </div>                                         
                                            <div class="col-lg-12">
                                                <textarea name="txtDetalle" id="txtDetalle" class="form-control" rows="15" required="required"><?php if(isset($dato['detalle'])) echo $dato['detalle'];?></textarea><br>
                                            </div>                                        
                                            <div class="col-lg-12 text-center">
                                                <button class="btn btn-primary waves-effect waves-light" type="submit" name="btnGuardar" id="btnGuardar">Guardar</button>
                                                <a href="noticias.php" class="btn btn-default waves-effect waves-light m-l-5">Cancel</a>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>

                       <div class="row">
                            <div class="col-sm-12">
                                <div class="card-box">
                                    <table id="datatable" class="table table-striped table-bordered">
                                        <thead>
                                            <tr>
                                                <th width="50px" class="text-center">ID</th>
                                                <th class="text-center">Publicaciones</th>
                                                <th width="80px" class="text-center">Estado</th>
                                                <th width="250px" class="text-center">Acción</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php while ($rowTodos = mysqli_fetch_array($todos)){ 
                                                if ($rowTodos['estado'] == 1)
                                                    $elEstado = '<span class="label label-table label-success" onclick="inactivar('."'noticias'".', '."'".$rowTodos['id']."'".', 2, '."'noticias.php'".')" style="cursor:pointer">Activo</span>';
                                                else
                                                    $elEstado = '<span class="label label-table label-danger"  onclick="inactivar('."'noticias'".', '."'".$rowTodos['id']."'".', 1, '."'noticias.php'".')" style="cursor:pointer">Inactivo</span>';
                                                ?>
                                                <tr>
                                                    <td class="text-center"><?php echo $rowTodos['id'];?></td>
                                                    <td>
                                                        <ins class="text-primary" style="cursor:pointer;" data-toggle="modal" data-target="#modal_<?php echo $rowTodos['id'];?>">
                                                            <?php echo $rowTodos['titulo'].'<br>'.strip_tags(substr($rowTodos['detalle'], 0, 250));?>
                                                        </ins>
                                                       <div id="modal_<?php echo $rowTodos['id'];?>" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                                                            <div class="modal-dialog">
                                                                <div class="modal-content p-0 b-0">
                                                                    <div class="panel panel-color panel-primary">
                                                                        <div class="panel-heading"> 
                                                                            <button type="button" class="close m-t-5" data-dismiss="modal" aria-hidden="true">×</button> 
                                                                            <h3 class="panel-title">Imagen Principal</h3> 
                                                                        </div> 

                                                                       <div class="panel-body"> 
                                                                            <img src="../public/img/noticias/<?php echo $rowTodos['id'];?>/principal.png?<?php echo elRandom();?>" style="max-width: 300px; max-height: 350px">
                                                                        </div> 
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                   </td>
                                                    <td class="text-center"><?php echo $elEstado;?></td>
                                                    <td class="text-center">
                                                        <!-- <a class="btn btn-inverse waves-effect waves-light btn-xs" href="noticias_galeria.php?id=<?php echo $rowTodos['id'];?>"><i class="md-collections"></i> Galeria</a> -->
                                                        <a class="btn btn-inverse waves-effect waves-light btn-xs" href="noticias.php?id=<?php echo $rowTodos['id'];?>"><i class="ion-edit"></i> Editar</a>
                                                        <a class="btn btn-inverse waves-effect waves-light btn-xs" onclick="eliminar('noticias', <?php echo $rowTodos['id'];?>, 'noticias.php')"><i class="glyphicon glyphicon-trash"></i> Eliminar</a>
                                                    </td>
                                                </tr>
                                           <?php } ?>                                            
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <footer class="footer">2016 © Creasotol</footer>
                </div>        
            </div>
        </div>
        <?php include("partials/footer.php"); ?>
        <script type="text/javascript">
            $('.selectpicker').selectpicker();
        </script>
        <script type="text/javascript">
            $(document).ready(function () {
                $("#txtDetalle").htmlarea();
            });
        </script>
    </body>
</html>

<?php 
if (isset($_REQUEST['btnGuardar'])) {
    $id = $_REQUEST['txtId'];

    if ($id == '') {
        $mensaje = "Se creo la noticia.";
        $campos  = " titulo, detalle, estado, fecha_registro";
        $valores = " '".$_REQUEST['txtTitulo']."', '".$_REQUEST['txtDetalle']."', '".$_REQUEST['rdoActivo']."', '".date("Y-m-d")."'";
        $id = crearDato("noticias", $campos, $valores);
    }else{
        $mensaje = "Se modificaron los datos de la noticia.";
        $campos  = " titulo = '".$_REQUEST['txtTitulo']."', detalle = '".$_REQUEST['txtDetalle']."', estado = '".$_REQUEST['rdoActivo']."'";
        actualizarDatos("noticias", $campos, "id = ".$id);
    }

    $carpeta = "../public/img/noticias/".$id."/";

    if(!file_exists($carpeta)){
        mkdir($carpeta, 0777);
        chmod($carpeta, 0777);
    }

   //SE CAMBIA EL LOGO SI SE CARGO UNO NUEVO
    if($_FILES['txtArchivo']['name'] != ''){
        $logo = "../public/img/noticias/".$id."/principal.png";

        if(copy($_FILES['txtArchivo']['tmp_name'], $logo) ){ 
            $mensaje .= ' Se cargo la imagen. ';
        }else{
            $mensaje .= ' Se produjo un error al cargar la imagen. ';
        }
    }

    echo "<script> 
            $.Notification.notify('success','top left', 'Exito!', '".$mensaje."');
            setInterval(function(){ location.href = 'noticias.php' }, 3000);
          </script>";
}
?>