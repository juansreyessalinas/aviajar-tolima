<?php
include("partials/funciones.php");

//Verificando si esta logueado
if (!isset($_SESSION['usuPA_admin'])) {
    header('Location: index.php');
}

if (isset($_REQUEST['id'])) {
    $dato = busquedaIndividual("inmobiliaria_categoria", "id = ".$_REQUEST['id']);
}
$todos = busquedasGenerales("inmobiliaria_categoria", "", "ORDER BY id ASC");
?>
<!DOCTYPE html>
<html>
    <head>
        <?php include('partials/header.php');  ?>
    </head>
    <body class="fixed-left">
        <div id="wrapper">
            <?php include('partials/menu_horizontal.php');  ?>       
            <div class="content-page">
                <div class="content">
                    <div class="container">  

                        <!-- Donde Estoy -->
                        <div class="row">
                            <div class="col-sm-12">
                                <h4 class="page-title">Categorias</h4>
                                <ol class="breadcrumb">
                                    <li><a href="index2.php">Inicio</a></li>
                                    <!-- <li><a href="inmobiliaria_seccion.php">Sección</a></li> -->
                                    <li class="active">Categorias</li>
                                </ol>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="card-box">
                                    <form action="#" method="post">
                                        <input type="hidden" name="txtId" id="txtId" value="<?php if(isset($dato['id'])) echo $dato['id'];?>" />
                                        <div class="form-group">
                                            <div class="col-lg-4">
                                                <label>Categoria:</label>
                                                <input type="text" class="form-control" name="txtTitulo" id="txtTitulo" placeholder="Editorial" required="required" value="<?php if(isset($dato['titulo'])) echo $dato['titulo'];?>">
                                            </div>
                                            
                                            <div class="col-lg-5">
                                                <label>Descripción:</label>
                                                <select class="selectpicker" data-live-search="true" data-style="btn-white" name="selSeccion" id="selSeccion" required="required">
                                                    <?php optOrder($dato['id_seccion'], "seccion", "WHERE estado = 1", "titulo", "", "ORDER BY titulo ASC"); ?>
                                                </select>
                                            </div>
                                            
                                            <div class="col-lg-3">
                                                <label>Estado:</label>
                                                <div class="radio radio-primary">
                                                    <input type="radio" name="rdoActivo" id="rdoActivo" value="1"<?php if(isset($dato['estado']) && $dato['estado'] == 1) echo "checked";?>>
                                                    <label for="radio3">Activo</label> &nbsp; &nbsp; &nbsp; &nbsp;                                                 
                                                    <input type="radio" name="rdoActivo" id="rdoActivo" value="2"<?php if(isset($dato['estado']) && $dato['estado'] == 2) echo "checked";?>>
                                                    <label for="radio3">Inactivo</label>
                                                </div>
                                                <br>
                                            </div>
                                        </div>
                                        <div class="form-group text-center">
                                            <button class="btn btn-primary waves-effect waves-light" type="submit" name="btnGuardar" id="btnGuardar">Guardar</button>
                                            <button type="reset" class="btn btn-default waves-effect waves-light m-l-5">Cancel</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>


                       <div class="row">
                            <div class="col-sm-12">
                                <div class="card-box">
                                    <table id="datatable" class="table table-striped table-bordered">
                                        <thead>
                                            <tr>
                                                <th width="80px" class="text-center">ID</th>
                                                <th width="250px" class="text-center">Sección</th>
                                                <th class="text-center">Categoria</th>
                                                <th width="80px" class="text-center">Estado</th>
                                                <th width="160px" class="text-center">Acción</th>
                                            </tr>
                                        </thead>

                                        <tbody>
                                            <?php while ($rowTodos = mysqli_fetch_array($todos)){ 
                                               if ($rowTodos['estado'] == 1)
                                                    $elEstado = '<span class="label label-table label-success" onclick="inactivar('."'inmobiliaria_categoria'".', '."'".$rowTodos['id']."'".', 2, '."'inmobiliaria_categoria.php'".')" style="cursor:pointer">Activo</span>';
                                                else
                                                    $elEstado = '<span class="label label-table label-danger"  onclick="inactivar('."'inmobiliaria_categoria'".', '."'".$rowTodos['id']."'".', 1, '."'inmobiliaria_categoria.php'".')" style="cursor:pointer">Inactivo</span>';
                                                ?>
                                                <tr>
                                                    <td class="text-center"><?php echo $rowTodos['id'];?></td>
                                                    <td><?php echo busquedaEspecifica("titulo", "titulo", "inmobiliaria_seccion", "id = ".$rowTodos['id_seccion']);?></td>
                                                    <td><?php echo $rowTodos['titulo'];?></td>
                                                    <td class="text-center"><?php echo $elEstado;?></td>
                                                    <td class="text-center">
                                                        <a class="btn btn-inverse waves-effect waves-light btn-xs" href="inmobiliaria_categoria.php?id=<?php echo $rowTodos['id'];?>"><i class="ion-edit"></i> Editar</a>
                                                        <a class="btn btn-inverse waves-effect waves-light btn-xs" onclick="eliminar('inmobiliaria_categoria', <?php echo $rowTodos['id'];?>, 'inmobiliaria_categoria.php')"><i class="glyphicon glyphicon-trash"></i> Eliminar</a>
                                                    </td>
                                                </tr>
                                            <?php } ?> 
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <footer class="footer">2016 © Creasotol</footer>
                </div>        
            </div>
        </div>
        <?php include("partials/footer.php"); ?>
        <script type="text/javascript">
            $('.selectpicker').selectpicker();
        </script>
    </body>
</html>
<?php 
if (isset($_REQUEST['btnGuardar'])) {
    $id = $_REQUEST['txtId'];

    if ($id == '') {
        $mensaje = "Se creo la categoria.";
        $campos  = "id_seccion, titulo, estado, fecha_registro";
        $valores = "'".$_REQUEST['selSeccion']."', '".$_REQUEST['txtTitulo']."', '".$_REQUEST['rdoActivo']."', '".date("Y-m-d")."'";
        crearDato("inmobiliaria_categoria", $campos, $valores);
    }else{
        $mensaje = "Se modificaron los datos de la categoria.";
        $campos  = "id_seccion = '".$_REQUEST['selSeccion']."', titulo = '".$_REQUEST['txtTitulo']."', estado = '".$_REQUEST['rdoActivo']."'";
        actualizarDatos("inmobiliaria_categoria", $campos, "id = ".$id);
    }

    echo "<script> 
            $.Notification.notify('success','top left', 'Exito!', '".$mensaje."');
            setInterval(function(){ location.href = 'inmobiliaria_categoria.php' }, 3000);
          </script>";
}
?>