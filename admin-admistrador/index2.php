<?php
include("partials/funciones.php");

//Verificando si esta logueado
if (!isset($_SESSION['usuPA_admin'])) {
    header('Location: index.php');
}

?>
<!DOCTYPE html>
<html>
    <head>
        <?php include('partials/header.php');  ?>
    </head>
    <body class="fixed-left">
        <div id="wrapper">
            <?php include('partials/menu_horizontal.php');  ?>
        
            <div class="content-page">
                <div class="content">
                    <div class="container">
    
                        <!-- Donde Estoy -->
                        <div class="row">
                            <div class="col-sm-12">
                                <!--<h4 class="page-title">Modificar mis Datos</h4>
                                <ol class="breadcrumb">
                                    <li class="active">Mis Datos</li>
                                </ol> -->
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-12">
                                <div class="card-box">
                                    <h4 class="m-t-0 header-title"><b>Mis Datos</b></h4>
                                    <p class="text-muted font-13 m-b-30">Modifique sus datos.</p>

                                    <form action="#" method="post">
                                        <div class="form-group">
                                            <label for="userName" class="col-md-1 control-label">Nombre*</label>
                                            <div class="col-md-5">
                                                <input type="text" name="txtNombre" id="txtNombre" class="form-control" required="required" placeholder="Ingrese su nombre" parsley-trigger="change" value="<?php echo $_SESSION['usuPA_admin']['nombre'];?>">
                                            </div>                                            

                                            <label for="emailAddress" class="col-md-1 control-label">Email*</label>
                                            <div class="col-md-5">
                                                <input type="email" name="txtEmail" id="txtEmail" class="form-control" required="required" placeholder="Ingrese su correo electronico" parsley-trigger="change" value="<?php echo $_SESSION['usuPA_admin']['email'];?>">
                                            </div>
                                            <br><br>
                                        </div>                                        

                                        <div class="form-group">
                                            <label for="userName" class="col-md-1 control-label">Usuario*</label>
                                            <div class="col-md-5">
                                                <input type="text" name="txtUsuario" id="txtUsuario" class="form-control" required="required" placeholder="Ingrese su usuario" parsley-trigger="change" value="<?php echo $_SESSION['usuPA_admin']['usuario'];?>">
                                            </div>

                                            <label for="pass1" class="col-md-1 control-label">Password</label>
                                            <div class="col-md-5">
                                                <input type="password" name="txtClave1" id="txtClave1" placeholder="Contraseña" class="form-control">
                                            </div>
                                            <br><br>
                                        </div>

                                        <div class="form-group text-center m-b-0">
                                            <button class="btn btn-primary waves-effect waves-light" type="submit" name="btnGuardar" id="btnGuardar">Guardar Datos</button>
                                            <!-- <button type="reset" class="btn btn-default waves-effect waves-light m-l-5">Cancel</button> -->
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <footer class="footer">2016 © Creasotol</footer>
                </div>        
            </div>
        </div>
        <?php include("partials/footer.php"); ?>
        <script type="text/javascript">
            
        </script>
    </body>
</html>

<?php 
if (isset($_REQUEST['btnGuardar'])) {
    $nombre  = $_REQUEST['txtNombre'];
    $usuario = $_REQUEST['txtUsuario'];
    $email   = $_REQUEST['txtEmail'];
    $campos  = " nombre = '".$nombre."', usuario = '".$usuario."', email = '".$email."' ";

    if ($_REQUEST['txtClave1'] != ''){
        $clave   = md5($_REQUEST['txtClave1']);
        $campos .= ", clave = '".$clave."'";
    }

    actualizarDatos("administrador", $campos, "id = ".$_SESSION['usuPA_admin']['id']);

    $_SESSION['usuPA_admin'] = array(
                                        'id'      => $_SESSION['usuPA_admin']['id'], 
                                        'nombre'  => $nombre,
                                        'email'   => $email,
                                        'usuario' => $usuario,
                                        'permiso' => $_SESSION['usuPA_admin']['permiso']
                                    );

    echo "<script> 
            $.Notification.notify('success','top left', 'Exito!', 'Se modificaron los datos del usuario.');
            setInterval(function(){ location.href = 'index2.php' }, 2500);
          </script>";

}



?>