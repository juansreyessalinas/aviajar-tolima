<?php

include("partials/funciones.php");



//Verificando si esta logueado

if (!isset($_SESSION['usuPA_admin'])) {

    header('Location: index.php');

}



if (isset($_REQUEST['id'])) {

    $dato = busquedaIndividual("banner", "id = ".$_REQUEST['id']);

}



$todos = busquedasGenerales("banner", "", "ORDER BY id ASC");

?>

<!DOCTYPE html>

<html>

    <head>

        <?php include('partials/header.php');  ?>

    </head>

    <body class="fixed-left">

        <div id="wrapper">

            <?php include('partials/menu_horizontal.php');  ?>

       

            <div class="content-page">

                <div class="content">

                    <div class="container">   

                        <!-- Donde Estoy -->

                        <div class="row">

                            <div class="col-sm-12">

                                <h4 class="page-title">Banner</h4>

                                <ol class="breadcrumb">

                                    <li><a href="index2.php">Inicio</a></li>

                                    <li class="active">Banner</li>

                                </ol>

                            </div>

                        </div>

                        <div class="row">

                            <div class="col-lg-12">

                                <div class="card-box">

                                    <form action="#" method="post" enctype="multipart/form-data" class="form-horizontal">

                                        <input type="hidden" name="txtId" id="txtId" value="<?php if(isset($dato['id'])) echo $dato['id'];?>" />

                                        <div class="form-group">                                          

                                            <div class="col-lg-3">

                                                <label>Titulo:</label>

                                                <input type="text" class="form-control" name="txtTitulo" id="txtTitulo" placeholder="Titulo" value="<?php if(isset($dato['titulo'])) echo $dato['titulo'];?>">

                                                <br>

                                            </div>
                                            <div class="col-lg-3">

                                                <label>Sub Titulo:</label>

                                                <input type="text" class="form-control" name="txtDetalle" id="txtDetalle" placeholder="Sub Titulo" value="<?php if(isset($dato['subtitulo'])) echo $dato['subtitulo'];?>">

                                                <br>

                                            </div>



                                            <!-- <div class="col-lg-6">

                                                <label>Subtitulo:</label>

                                                <input type="text" class="form-control" name="txtSubTitulo" id="txtSubTitulo" placeholder="Subtitulo" value="<?php if(isset($dato['subtitulo'])) echo $dato['subtitulo'];?>">

                                                <br>

                                            </div> -->



                                           <div class="col-lg-6">

                                                <label>URL:</label>

                                                <input type="text" class="form-control" name="txtUrl" id="txtUrl" placeholder="http://.com/detalle-inmueble.php?idI=324" value="<?php if(isset($dato['url'])) echo $dato['url'];?>">

                                                <br>

                                            </div>    



                                            <div class="col-lg-6">

                                                <label>Imágen:</label>

                                                <input type="file" name="txtArchivo" id="txtArchivo" class="filestyle" data-buttontext="Seleccione" data-buttonname="btn-inverse" onchange="verificarjpg(this.value, 'txtArchivo')">

                                                <br>

                                            </div>
                                            



                                            <div class="col-lg-6">

                                                <label>Posición:</label>

                                                <input type="number" class="form-control solo-numero" name="txtPosicion" id="txtPosicion" placeholder="Subtitulo" value="<?php if(isset($dato['posicion'])) echo $dato['posicion'];?>">

                                                <br>

                                            </div>



                                            <div class="col-lg-3">

                                                <label>Tipo:</label>

                                                <select  class="form-control" name="selTipo" id="selTipo" required="required">

                                                    <?php optOrder($dato['tipo'], "banner_tipo", "", "titulo", "", "ORDER BY titulo ASC"); ?>

                                                </select>                                                

                                            </div>



                                            <div class="col-lg-3">

                                                <label>Estado:</label>

                                                <div class="radio radio-primary">

                                                    <input type="radio" name="rdoActivo" id="rdoActivo" value="1"<?php if(isset($dato['estado']) && $dato['estado'] == 1) echo "checked";?>>

                                                    <label for="radio3">Activo</label> &nbsp; &nbsp; &nbsp; &nbsp;                                                

                                                    <input type="radio" name="rdoActivo" id="rdoActivo" value="2"<?php if(isset($dato['estado']) && $dato['estado'] == 2) echo "checked";?>>

                                                    <label for="radio3">Inactivo</label>

                                                </div>

                                            </div>
                                            <br>
                                            <div class="form-group">
                                               
                                            </div>

                                        </div>

                                        <div class="form-group text-center">

                                            <div class="col-lg-12">

                                                <button class="btn btn-primary waves-effect waves-light" type="submit" name="btnGuardar" id="btnGuardar">Guardar</button>

                                                 <a href="banner.php" class="btn btn-default waves-effect waves-light m-l-5">Cancel</a>

                                            </div>

                                        </div>

                                    </form>

                                </div>

                            </div>

                        </div>



                        <div class="row">

                            <div class="col-sm-12">

                                <div class="card-box">

                                    <table id="datatable" class="table table-striped table-bordered">

                                        <thead>

                                            <tr>

                                                <th width="80px" class="text-center">ID</th>

                                                <th width="250px" class="text-center">Foto</th>

                                                <th class="text-center">Imágen</th>

                                                <th width="200px" class="text-center">Tipo</th>

                                                <th width="50px" class="text-center">Posición</th>

                                                <th width="80px" class="text-center">Estado</th>

                                                <th width="160px" class="text-center">Acción</th>

                                            </tr>

                                        </thead>



                                        <tbody>

                                            <?php while ($rowTodos = mysqli_fetch_array($todos)){ 

                                                if (file_exists('../public/img/banner/'.$rowTodos['id'].'.jpg'))

                                                    $img = '<img src="../public/img/banner/'.$rowTodos['id'].'.jpg" height="90px">' ;

                                                else

                                                    $img = '';



                                                if ($rowTodos['estado'] == 1)

                                                    $elEstado = '<span class="label label-table label-success" onclick="inactivar('."'banner'".', '."'".$rowTodos['id']."'".', 2, '."'banner.php'".')" style="cursor:pointer">Activo</span>';

                                                else

                                                   $elEstado = '<span class="label label-table label-danger"  onclick="inactivar('."'banner'".', '."'".$rowTodos['id']."'".', 1, '."'banner.php'".')" style="cursor:pointer">Inactivo</span>';



                                                if ($rowTodos['estado'] == 1){

                                                }

                                                ?>

                                                <tr>

                                                    <td class="text-center"><?php echo $rowTodos['id'];?></td>

                                                    <td class="text-center">

                                                        <img src="../public/img/banner/<?php echo $rowTodos['id'];?>.jpg?<?php echo elRandom();?>" height="90px" data-toggle="modal" data-target="#modal_<?php echo $rowTodos['id'];?>" style="cursor:pointer">

                                                        <div id="modal_<?php echo $rowTodos['id'];?>" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="full-width-modalLabel" aria-hidden="true" style="display: none;">

                                                            <div class="modal-dialog modal-full">

                                                                <div class="modal-content">

                                                                    <div class="modal-header"> 

                                                                        <button type="button" class="close m-t-5" data-dismiss="modal" aria-hidden="true">×</button> 

                                                                        <h3 class="modal-title text-primary" id="full-width-modalLabel"><?php echo $rowTodos['titulo']; ?></h3> 

                                                                    </div> 

                                                                    <div class="modal-body"> 

                                                                        <img src="../public/img/banner/<?php echo $rowTodos['id'];?>.jpg?<?php echo elRandom();?>" height="350px">

                                                                    </div> 

                                                                </div>

                                                            </div>

                                                        </div>

                                                    </td>

                                                    <td>

                                                        <?php echo '<b>'.$rowTodos['titulo'].'</b><br>'.substr(strip_tags($rowTodos['subtitulo']), 0, 80).'<br>'.$rowTodos['url']; ?>

                                                    </td>

                                                    <td><?php echo busquedaEspecifica("titulo", "titulo", "banner_tipo", "id = ".$rowTodos['tipo']);?></td>

                                                    <td class="text-center"><?php echo $rowTodos['posicion'];?></td>

                                                    <td class="text-center"><?php echo $elEstado;?></td>

                                                    <td class="text-center">

                                                        <a class="btn btn-inverse waves-effect waves-light btn-xs" href="banner.php?id=<?php echo $rowTodos['id'];?>"><i class="ion-edit"></i> Editar</a>

                                                        <a class="btn btn-inverse waves-effect waves-light btn-xs" onclick="eliminarDato('banner', <?php echo $rowTodos['id'];?>, '../public/img/banner/<?php echo $rowTodos['id'];?>.jpg', 'banner.php');"><i class="glyphicon glyphicon-trash"></i> Eliminar</a>

                                                    </td>

                                                </tr>

                                            <?php } ?>                                            

                                        </tbody>

                                    </table>

                                </div>

                            </div>

                        </div>

                    </div>

                    <footer class="footer">2016 © Creasotol</footer>

                </div>        

            </div>

        </div>

        <?php include("partials/footer.php"); ?>

    </body>

</html>

<?php 

if (isset($_REQUEST['btnGuardar'])) {

    $elId = $_REQUEST['txtId'];



    if ($elId == '') {

        $mensaje = "Se creo el banner.";

        $campos  = "titulo, subtitulo, url, posicion, estado, tipo, fecha_registro";

        $valores = "'".$_REQUEST['txtTitulo']."', '".$_REQUEST['txtDetalle']."', '".$_REQUEST['txtUrl']."', '".$_REQUEST['txtPosicion']."', '".$_REQUEST['rdoActivo']."', '".$_REQUEST['selTipo']."', '".date("Y-m-d")."'";

        $elId = crearDato("banner", $campos, $valores);

    }else{

        $mensaje = "Se modificaron los datos del banner.";

        $campos  = "titulo = '".$_REQUEST['txtTitulo']."', subtitulo = '".$_REQUEST['txtDetalle']."', url = '".$_REQUEST['txtUrl']."', posicion = '".$_REQUEST['txtPosicion']."', estado = '".$_REQUEST['rdoActivo']."', tipo = '".$_REQUEST['selTipo']."'";

        actualizarDatos("banner", $campos, "id = ".$elId);

    }



    //SE CAMBIA EL LOGO SI SE CARGO UNO NUEVO

    if($_FILES['txtArchivo']['name'] != ''){

        $logo = "../public/img/banner/".$elId.".jpg";

        if(copy($_FILES['txtArchivo']['tmp_name'], $logo) ){ 

            $mensaje .= ' Se cargo el banner. ';

        }else{

            $mensaje .= ' Se produjo un error al cargar el banner. ';

        }

    }



    echo "<script> 

            $.Notification.notify('success','top left', 'Exito!', '".$mensaje."');

            setInterval(function(){ location.href = 'banner.php' }, 3000);

          </script>";

} ?>