<?php
include("partials/funciones.php");

//Verificando si esta logueado
if (!isset($_SESSION['usuPA_admin'])) {
    header('Location: index.php');
}

if (isset($_REQUEST['id'])) {
    $dato = busquedaIndividual("ciudades", "id = ".$_REQUEST['id']);
}
$todos = busquedasGenerales("ciudades", "", "ORDER BY id ASC");
header('Content-Type: text/html; charset=utf-8');
?>
<!DOCTYPE html>
<html>
    <head><meta http-equiv="Content-Type" content="text/html; charset=gb18030">
        <?php include('partials/header.php');  ?>
 
    </head>
    <body class="fixed-left">
        <div id="wrapper">
            <?php include('partials/menu_horizontal.php');  ?>       
            <div class="content-page">
                <div class="content">
                    <div class="container">  

                        <!-- Donde Estoy -->
                        <div class="row">
                            <div class="col-sm-12">
                                <h4 class="page-title">Ciudades</h4>
                                <ol class="breadcrumb">
                                    <li><a href="index2.php">Inicio</a></li>
                                    <!-- <li><a href="inmobiliaria_seccion.php">Sección</a></li> -->
                                    <li class="active">Ciudades</li>
                                </ol>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="card-box">
                                    <form action="#" method="post">
                                        <input type="hidden" name="txtId" id="txtId" value="<?php if(isset($dato['id'])) echo $dato['id'];?>" />
                                        <div class="form-group">
                                            <div class="col-lg-6">
                                                <label>Titulo:</label>
                                                <input type="text" class="form-control" name="txtTitulo" id="txtTitulo" placeholder="Editorial" required="required" value="<?php if(isset($dato['nombre'])) echo $dato['nombre'];?>">
                                            </div>
                                            <div class="col-lg-3">
                                                <label>Departamento:</label>
                                                <select class="selectpicker" data-style="btn-white" name="departamento" id="departamento" required="required">
                                                    <?php optOrder($dato['idDepartamento'], "departamento", "", "nombre", "", "ORDER BY nombre ASC"); ?>
                                                </select>
                                            </div>
                                        </div>
                                       
                                        <div class="form-group text-center su">
                                            <button class="btn btn-primary waves-effect waves-light" type="submit" name="btnGuardar" id="btnGuardar">Guardar</button>
                                            <button type="reset" class="btn btn-default waves-effect waves-light m-l-5">Cancel</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>


                       <div class="row">
                            <div class="col-sm-12">
                                <div class="card-box">
                                    <table id="datatable" class="table table-striped table-bordered">
                                        <thead>
                                            <tr>
                                                <th width="80px" class="text-center">ID</th>
                                                <th class="text-center">Titulo</th>
                                                 <th class="text-center">Departamento</th>
                                                <th width="160px" class="text-center">Accion</th>
                                            </tr>
                                        </thead>

                                        <tbody>
                                            <?php while ($rowTodos = mysqli_fetch_array($todos)){ ?>
                                                <tr>
                                                    <td class="text-center"><?php echo $rowTodos['id'];?></td>
                                                    <td><?php echo utf8_encode($rowTodos['nombre']);?></td>
                                                    <td><?php $dep = busquedaIndividual("departamento", "id = ".$rowTodos['idDepartamento']);  echo utf8_encode( $dep['nombre']) ?></td>
                                                    
                                                    <td class="text-center">
                                                        <a class="btn btn-inverse waves-effect waves-light btn-xs" href="inmobiliaria_mapac.php?id=<?php echo $rowTodos['id'];?>"><i class="md-collections"></i> Mapa</a>
                                                        <a class="btn btn-inverse waves-effect waves-light btn-xs" href="inmobiliaria_ciudades.php?id=<?php echo $rowTodos['id'];?>"><i class="ion-edit"></i> Editar</a>
                                                    </td>
                                                </tr>
                                            <?php } ?> 
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <footer class="footer">2016 © Creasotol</footer>
                </div>        
            </div>
        </div>
        <?php include("partials/footer.php"); ?>
        <script type="text/javascript">
            $('.selectpicker').selectpicker();
        </script>
    </body>
</html>
<?php 
if (isset($_REQUEST['btnGuardar'])) {
    $id = $_REQUEST['txtId'];

    if ($id == '') {
        $mensaje = "Se creo la ciudad.";
        $campos  = "nombre,idDepartamento";
        $valores = "'".$_REQUEST['txtTitulo']."',".$_REQUEST['departamento']."";
        crearDato("ciudades", $campos, $valores);
    }else{
        $mensaje = "Se modificaron los datos de la ciudad.";
        $campos  = "titulo = '".$_REQUEST['txtTitulo']."',idDepartamento = ".$_REQUEST['departamento']."";
        actualizarDatos("ciudades", $campos, "id = ".$id);
    }

    echo "<script> 
            $.Notification.notify('success','top left', 'Exito!', '".$mensaje."');
            setInterval(function(){ location.href = 'inmobiliaria_ciudades.php' }, 3000);
          </script>";
}
?>