<?php
include("partials/funciones.php");

//Verificando si esta logueado
if (!isset($_SESSION['usuPA_admin'])) {
    header('Location: index.php');
}

if (isset($_REQUEST['id'])) {
    $dato = busquedaIndividual("inmobiliaria", "id = ".$_REQUEST['id']);
}
$todos = busquedasGenerales("inmobiliaria", "", "ORDER BY id ASC");
?>
<!DOCTYPE html>
<html>
    <head>
        <?php include('partials/header.php');  ?>
    </head>
    <body class="fixed-left">
        <div id="wrapper">
            <?php include('partials/menu_horizontal.php');  ?>       
            <div class="content-page">
                <div class="content">
                    <div class="container">  

                        <!-- Donde Estoy -->
                        <div class="row">
                            <div class="col-sm-12">
                                <h4 class="page-title">Inmobiliaria</h4>
                                <ol class="breadcrumb">
                                    <li><a href="index2.php">Inicio</a></li>
                                    <!-- <li><a href="inmobiliaria_seccion.php">Seccion</a></li> -->
                                    <li class="active">inmobiliaria</li>
                                </ol>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="card-box">
                                    <form action="#" method="post" enctype="multipart/form-data">
                                        <input type="hidden" name="txtId" id="txtId" value="<?php if(isset($dato['id'])) echo $dato['id'];?>" />
                                        <div class="form-group">
                                            <div class="col-lg-4">
                                                <label>Titulo:</label>
                                                <input type="text" class="form-control" name="txtTitulo" id="txtTitulo" placeholder="Editorial" required="required" value="<?php if(isset($dato['titulo'])) echo $dato['titulo'];?>">
                                            </div>
                                             <div class="col-lg-4">
                                                <label>Imágen Inmobiliaria: </label>
                                                <input type="file" name="txtArchivo" id="txtArchivo" class="filestyle" data-buttontext="Seleccione" data-buttonname="btn-inverse" >
                                            </div>
                                            <div class="col-lg-4">
                                                <label>Ciudad:</label>
                                                <select class="selectpicker" data-style="btn-white" name="ciudad" id="ciudad" required="required">
                                                    <?php optOrder($dato['ciudad'], "inmobiliaria_ciudad", "", "titulo", "", "ORDER BY titulo ASC"); ?>
                                                </select>
                                            </div>
                                            <div class="col-lg-4">
                                                <label>Estado:</label>
                                                <div class="radio radio-primary">
                                                    <input type="radio" name="rdoActivo" id="rdoActivo" value="1"<?php if(isset($dato['estado']) && $dato['estado'] == 1) echo "checked";?>>
                                                    <label for="radio3">Activo</label> &nbsp; &nbsp; &nbsp; &nbsp;                                                 
                                                    <input type="radio" name="rdoActivo" id="rdoActivo" value="2"<?php if(isset($dato['estado']) && $dato['estado'] == 2) echo "checked";?>>
                                                    <label for="radio3">Inactivo</label>
                                                </div>
                                               

                                            </div>
                                        </div>
                                        <div class="form-group text-center">
                                            <button class="btn btn-primary waves-effect waves-light" type="submit" name="btnGuardar" id="btnGuardar">Guardar</button>
                                            <button type="reset" class="btn btn-default waves-effect waves-light m-l-5">Cancel</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>


                       <div class="row">
                            <div class="col-sm-12">
                                <div class="card-box">
                                    <table id="datatable" class="table table-striped table-bordered">
                                        <thead>
                                            <tr>
                                                <th width="80px" class="text-center">ID</th>
                                                <th class="text-center">Titulo</th>
                                                <th class="text-center">Ciudad</th>
                                                <th width="160px" class="text-center">Accion</th>
                                            </tr>
                                        </thead>

                                        <tbody>
                                            <?php while ($rowTodos = mysqli_fetch_array($todos)){ ?>
                                                <tr>
                                                    <td class="text-center"><?php echo $rowTodos['id'];?></td>
                                                    <td><?php echo $rowTodos['titulo'];?></td>
                                                     <?php
                                                     $dato1 = busquedaIndividual("inmobiliaria_ciudad", "id = ".$rowTodos['ciudad']);
                                                    ?>
                                                     <td><?php echo $dato1['titulo'];?></td>
                                                    <td class="text-center">
                                                        <a class="btn btn-inverse waves-effect waves-light btn-xs" href="inmobiliarias.php?id=<?php echo $rowTodos['id'];?>"><i class="ion-edit"></i> Editar</a>
                                                    </td>
                                                </tr>
                                            <?php } ?> 
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <footer class="footer">2016 - Creasotol</footer>
                </div>        
            </div>
        </div>
        <?php include("partials/footer.php"); ?>
        <script type="text/javascript">
            $('.selectpicker').selectpicker();
        </script>
    </body>
</html>
<?php 
if (isset($_REQUEST['btnGuardar'])) {
    $id = $_REQUEST['txtId'];

    if ($id == '') {
        $mensaje = "Se creo la constructora.";
        $campos  = "titulo,  estado,ciudad";
        $valores = "'".$_REQUEST['txtTitulo']."', '".$_REQUEST['rdoActivo']."','".$_REQUEST['ciudad']."'";
        $id=crearDato("inmobiliaria", $campos, $valores);
    }else{
        $mensaje = "Se modificaron los datos de la constructora.";
        $campos  = "titulo = '".$_REQUEST['txtTitulo']."', estado = '".$_REQUEST['rdoActivo']."', ciudad= '".$_REQUEST['ciudad']."'";
        actualizarDatos("inmobiliaria", $campos, "id = ".$id);
    }

     $carpeta = "../public/img/inmobiliaria/".$id."/";

    if(!file_exists($carpeta)){
        mkdir($carpeta, 0777);
        chmod($carpeta, 0777);
    }

    //SE CAMBIA LA IMAGEN PREDETERMINADA SI SE CARGO UNO NUEVO
    if($_FILES['txtArchivo']['name'] != ''){
        $principal = "../public/img/inmobiliaria/".$id."/principal.jpg";
        
        if(copy($_FILES['txtArchivo']['tmp_name'], $principal) ){ 
            $mensaje .= ' Se cargo la imagen principal. ';
        }else{
            $mensaje .= ' Se produjo un error al cargar la imagen principal. ';
        }
    }
   echo "<script> 
            $.Notification.notify('success','top left', 'Exito!', '".$mensaje."');
            setInterval(function(){ location.href = 'inmobiliarias.php' }, 3000);
          </script>";

}
?>