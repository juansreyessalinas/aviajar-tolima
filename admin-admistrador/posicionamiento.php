<?php
include("partials/funciones.php");
//Verificando si esta logueado
if (!isset($_SESSION['usuPA_admin'])) {
    header('Location: index.php');
}
$dato = busquedaIndividual("metatags", "id = 1");
?>
<!DOCTYPE html>
<html>
    <head>
        <?php include('partials/header.php');  ?>
    </head>
    <body class="fixed-left">
        <div id="wrapper">
            <?php include('partials/menu_horizontal.php');  ?>
            <div class="content-page">
                <div class="content">
                    <div class="container">
    
                        <!-- Donde Estoy -->
                        <div class="row">
                            <div class="col-sm-12">
                                <h4 class="page-title">Posicionamiento</h4>
                                <ol class="breadcrumb">
                                    <li><a href="index2.php">Inicio</a></li>
                                    <li class="active">Posicionamiento</li>
                                </ol>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-12">
                                <div class="card-box">
                                    <h4 class="m-t-0 header-title"><b>Mis Datos</b></h4>
                                    <p class="text-muted font-13 m-b-30">Modifique sus datos.</p>

                                    <form action="#" method="post">
                                        <div class="form-group">
                                            <label for="userName" class="col-md-2 control-label">Titulo*</label>
                                            <div class="col-md-4">
                                                <textarea name="txtTitulo" id="txtTitulo" class="form-control" required="required"><?php echo $dato['titulo'];?></textarea>
                                            </div>                                            

                                            <label for="emailAddress" class="col-md-2 control-label">Tema*</label>
                                            <div class="col-md-4">
                                                <textarea name="txtTema" id="txtTema" class="form-control" required="required"><?php echo $dato['tema'];?></textarea>
                                            </div>
                                            <br><br><br><br><br>
                                        </div>                                        

                                        <div class="form-group">
                                            <label for="userName" class="col-md-2 control-label">Descripción*</label>
                                            <div class="col-md-4">
                                                <textarea name="txtDescripcion" id="txtDescripcion" class="form-control" required="required"><?php echo $dato['descripcion'];?></textarea>
                                            </div>

                                            <label for="pass1" class="col-md-2 control-label">Palabras Claves</label>
                                            <div class="col-md-4">
                                                <textarea name="txtClaves" id="txtClaves" class="form-control" required="required"><?php echo $dato['palabras'];?></textarea>
                                            </div>
                                            <br><br><br><br><br>
                                        </div>

                                        <div class="form-group text-center m-b-0">
                                            <button class="btn btn-primary waves-effect waves-light" type="submit" name="btnGuardar" id="btnGuardar">Guardar Datos</button>
                                            <!-- <button type="reset" class="btn btn-default waves-effect waves-light m-l-5">Cancel</button> -->
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <footer class="footer">2016 © Creasotol</footer>
                </div>        
            </div>
        </div>
        <?php include("partials/footer.php"); ?>
    </body>
</html>
<?php 
if (isset($_REQUEST['btnGuardar'])) {
    $campos  = " titulo = '".$_REQUEST['txtTitulo']."', tema = '".$_REQUEST['txtTema']."', descripcion = '".$_REQUEST['txtDescripcion']."', palabras = '".$_REQUEST['txtClaves']."' ";
    actualizarDatos("metatags", $campos, "id = 1");

    echo "<script> 
            $.Notification.notify('success','top left', 'Exito!', 'Se modificaron los datos del posicionamiento.');
            setInterval(function(){ location.href = 'posicionamiento.php' }, 3000);
          </script>";
}
?>