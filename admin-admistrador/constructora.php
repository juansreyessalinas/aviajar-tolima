<?php
include("partials/funciones.php");

//Verificando si esta logueado
if (!isset($_SESSION['usuPA_admin'])) {
    header('Location: index.php');
}

if (isset($_REQUEST['id'])) {
    $dato = busquedaIndividual("constructora", "id = ".$_REQUEST['id']);
}
$todos = busquedasGenerales("constructora", "", "ORDER BY id ASC");
?>
<!DOCTYPE html>
<html>
    <head>
        <?php include('partials/header.php');  ?>
    </head>
    <body class="fixed-left">
        <div id="wrapper">
            <?php include('partials/menu_horizontal.php');  ?>       
            <div class="content-page">
                <div class="content">
                    <div class="container">  

                        <!-- Donde Estoy -->
                        <div class="row">
                            <div class="col-sm-12">
                                <h4 class="page-title">Constructora</h4>
                                <ol class="breadcrumb">
                                    <li><a href="index2.php">Inicio</a></li>
                                    <!-- <li><a href="inmobiliaria_seccion.php">Secci贸n</a></li> -->
                                    <li class="active">Constructora</li>
                                </ol>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="card-box">
                                    <form action="#" method="post">
                                        <input type="hidden" name="txtId" id="txtId" value="<?php if(isset($dato['id'])) echo $dato['id'];?>" />
                                        <div class="form-group">
                                            <div class="col-lg-12">
                                                <label>Titulo:</label>
                                                <input type="text" class="form-control" name="txtTitulo" id="txtTitulo" placeholder="Editorial" required="required" value="<?php if(isset($dato['titulo'])) echo $dato['titulo'];?>">
                                            </div>
                                        </div>
                                        <div class="form-group text-center">
                                            <button class="btn btn-primary waves-effect waves-light" type="submit" name="btnGuardar" id="btnGuardar">Guardar</button>
                                            <button type="reset" class="btn btn-default waves-effect waves-light m-l-5">Cancel</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>


                       <div class="row">
                            <div class="col-sm-12">
                                <div class="card-box">
                                    <table id="datatable" class="table table-striped table-bordered">
                                        <thead>
                                            <tr>
                                                <th width="80px" class="text-center">ID</th>
                                                <th class="text-center">Titulo</th>
                                                <th width="160px" class="text-center">Acci贸n</th>
                                            </tr>
                                        </thead>

                                        <tbody>
                                            <?php while ($rowTodos = mysqli_fetch_array($todos)){ ?>
                                                <tr>
                                                    <td class="text-center"><?php echo $rowTodos['id'];?></td>
                                                    <td><?php echo $rowTodos['titulo'];?></td>
                                                    <td class="text-center">
                                                        <a class="btn btn-inverse waves-effect waves-light btn-xs" href="constructora.php?id=<?php echo $rowTodos['id'];?>"><i class="ion-edit"></i> Editar</a>
                                                    </td>
                                                </tr>
                                            <?php } ?> 
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <footer class="footer">2016 漏 Creasotol</footer>
                </div>        
            </div>
        </div>
        <?php include("partials/footer.php"); ?>
        <script type="text/javascript">
            $('.selectpicker').selectpicker();
        </script>
    </body>
</html>
<?php 
if (isset($_REQUEST['btnGuardar'])) {
    $id = $_REQUEST['txtId'];

    if ($id == '') {
        $mensaje = "Se creo la constructora.";
        $campos  = "titulo";
        $valores = "'".$_REQUEST['txtTitulo']."'";
        crearDato("constructora", $campos, $valores);
    }else{
        $mensaje = "Se modificaron los datos de la constructora.";
        $campos  = "titulo = '".$_REQUEST['txtTitulo']."'";
        actualizarDatos("constructora", $campos, "id = ".$id);
    }

    echo "<script> 
            $.Notification.notify('success','top left', 'Exito!', '".$mensaje."');
            setInterval(function(){ location.href = 'constructora.php' }, 3000);
          </script>";
}
?>