<?php include("partials/funciones.php");

$publicidad1= busquedaIndividual("imagenes", "id = 46");
$not= busquedasGenerales("noticias", "WHERE estado = 1 ", "ORDER BY id DESC LIMIT 0,3");
$metas       = busquedaIndividual("metatags", "id = 1"); 
$dato1 = busquedaIndividual("paginas", "id=1");
$dato2 = busquedaIndividual("paginas", "id=2");
$dato3 = busquedaIndividual("paginas", "id=3");



$img1 = busquedaIndividual("imagenes", "id=103");
$img2 = busquedaIndividual("imagenes", "id=109");
$img3 = busquedaIndividual("imagenes", "id=94");
$img4 = busquedaIndividual("imagenes", "id=95");
$img5 = busquedaIndividual("imagenes", "id=96");
$img6 = busquedaIndividual("imagenes", "id=97");

?>
<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8" />
        <title>MI TIERRA - A VIAJAR TOLIMA OPERADORES TURISTICOS</title>
        <meta name="description" content="MI TIERRA - A VIAJAR TOLIMA OPERADORES TURISTICOS">
        <meta name="author" content="www.creasotol.com, contactos@creasotol.com">
        <meta name="Subject" content="MI TIERRA - A VIAJAR TOLIMA OPERADORES TURISTICOS. ">
        <meta name="author" content="www.creasotol.com, contactos@creasotol.com">
        <meta name="robots" content="index,follow">
        <meta NAME="Language" CONTENT="Spanish">
        <meta NAME="Revisit" CONTENT="1 day">
        <meta NAME="Distribution" CONTENT="Global">
        <!--para desactivar el cache de todos los navegadores-->
        <meta http-equiv="cache-control" content="max-age=0" />
        <meta http-equiv="cache-control" content="no-cache, no-store, must-revalidate" />
        <meta http-equiv="pragma" content="no-cache" />
        <meta http-equiv="Expires" content="0">
        <meta http-equiv="Last-Modified" content="0">
        <!--cierra-->
        <!-- Mobile Specific Metas
        ================================================== -->
        
        <?php include 'partials/scriptsGlobal.php';  ?>
        
    </head>
    <body id="pageMiTierra" class="">                
        <?php include 'partials/header.php'; ?>


        <div id="cont-slider">
            <img src="public/img/logoblanco2.png" class="img-abs" alt="a viajar tolima - operadores turisticos">
            <img src="public/img/imagenes/109.jpg<?php echo elRandom()?>" class="img-interna" alt="a viajar tolima - operadores turisticos">
            <h3 class="texto">
               <?php echo $img2['titulo']; ?>
            </h3>

            <h2 class="tolima">Tolima</h2>
        </div>


        <div class="informacion disTable">
            <div class="container">
                <div class="row">
                    <h1>de turismo por mi tierra</h1>


                    <?php
             $central = busquedasGenerales("tienda_producto", " WHERE estado =1  AND tierra=1", "ORDER BY id DESC ");
                while ($rowCentral = mysqli_fetch_array($central)) {  ?>
      
                

                 
                 
                 
                 <div class="info-turismo">
                        <div class="titulo">
                            <h4><?php echo $rowCentral['titulo']; ?></h4>
                            
                            <p class="precio">$ <?php echo number_format($rowCentral['valor'], 0, '', '.'); ?></p>
                        </div>
                         <a  href="detalle-planes.php?id=<?php echo $rowCentral['id']; ?>"> <img src="public/img/productos/<?php echo $rowCentral['id']; ?>/principal2.png" alt="<?php echo $rowCentral['titulo']; ?>" class="principal"></a>
                        <a class="btnVerde btnBoxShadow" href="detalle-planes.php?id=<?php echo $rowCentral['id']; ?>">Ver detalle</a>
                    </div>
	            <?php }  ?>  

      



                </div>
            </div>
        </div>     

        
        <?php include 'partials/footer.php'; ?>        

    </body>
</html>