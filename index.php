<?php include("partials/funciones.php");

$publicidad1= busquedaIndividual("imagenes", "id = 46");
$not= busquedasGenerales("noticias", "WHERE estado = 1 ", "ORDER BY id DESC LIMIT 0,3");
$metas       = busquedaIndividual("metatags", "id = 1"); 
$dato1 = busquedaIndividual("paginas", "id=1");
$dato2 = busquedaIndividual("paginas", "id=2");
$dato3 = busquedaIndividual("paginas", "id=3");



$img1 = busquedaIndividual("imagenes", "id=103");
$img2 = busquedaIndividual("imagenes", "id=93");
$img3 = busquedaIndividual("imagenes", "id=94");
$img4 = busquedaIndividual("imagenes", "id=95");
$img5 = busquedaIndividual("imagenes", "id=96");
$img6 = busquedaIndividual("imagenes", "id=97");

?>
<!DOCTYPE html>
<html lang="es">
    <head>
    <?php  include "partials/metas.php" ?>    
        <meta NAME="Distribution" CONTENT="Global">
        <!--para desactivar el cache de todos los navegadores-->
        <meta http-equiv="cache-control" content="max-age=0" />
        <meta http-equiv="cache-control" content="no-cache, no-store, must-revalidate" />
        <meta http-equiv="pragma" content="no-cache" />
        <meta http-equiv="Expires" content="0">
        <meta http-equiv="Last-Modified" content="0">
        <!--cierra-->
        <!-- Mobile Specific Metas
        ================================================== -->
        
        <?php include 'partials/scriptsGlobal.php';  ?>
        
    </head>
    <body id="pageHome" class="">                
        <?php include 'partials/header.php'; ?>
        <?php //include 'modal-javascript.php'; ?>

        <div id="cont-slider">
            <?php include 'public/slider/sliderHome.php'; ?>
            <img src="public/img/logoblanco2.png" class="img-abs" alt="a viajar tolima - operadores turisticos">
        </div>

        <div class="secc-recomendados disTable">
            <div class="container">
                <div class="row">
                    <div class="tit-img">
                        <img src="public/img/logogris.png" alt="sitios turisticos tolima">
                        <h2>Recomendados</h2>
                    </div>


                     <?php
		    $central = busquedasGenerales("tienda_producto", " WHERE recomendado=1 and estado=1", "ORDER BY id DESC LIMIT 0,4");
                while ($rowCentral = mysqli_fetch_array($central)) {  ?>
      
                 

             
                 <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="info-viaje">
                            <a href="detalle-planes.php?id=<?php echo $rowCentral['id']; ?>"><div class="contImgz"><img src="public/img/productos/<?php echo $rowCentral['id']; ?>/principal.png<?php echo elRandom()?>" alt=""></div></a>
                            <a href="detalle-planes.php?id=<?php echo $rowCentral['id']; ?>"><h3 class="titVerde btnBoxShadow"><?php echo $rowCentral['titulo']; ?></h3></a>
                            <div class="precio">
                                <span><sup>Desde</sup></span>
                                <p>$ <?php echo number_format($rowCentral['valor'], 0, '', '.'); ?></p>
                            </div>
                            <div class="horario">
                                <i class="fa fa-clock-o" aria-hidden="true"></i>
                                <p><?php echo $rowCentral['duracion']; ?></p>
                            </div>
                            
                        </div>
                    </div>
	            <?php }  ?>  

                </div>
            </div>
        </div>

        <div class="secc-experiencia disTable" style="background-image: url('public/img/imagenes/103.jpg<?php echo elRandom()?>');">
            <div class="container">
                <div class="row">
                    <div class="informacion">
                        <h2><?php echo $img1['titulo']; ?></h2>
                    </div>

                    <a href="<?php echo $img1['url']; ?>" class="btnTransBlanco btnBoxShadow">click aquí</a>

                    <img src="public/img/logoblanco2.png" class="img-abs" alt="a viajar tolima - operadores turisticos">
                </div>
            </div>
        </div>
        
        <div class="cont-destacados disTable" style="background-image: url('public/img/imagenes/104.jpg<?php echo elRandom()?>');">
            <div class="container">
                <div class="row">
                    <div class="col-md-4 col-sm-12 col-xs-12">
                        <div class="info-destacados">
                            <div class="tit-img">
                                <img src="public/img/logogris.png" alt="sitios turisticos tolima">
                                <h2><?php echo $dato1['titulo']; ?></h2>
                            </div>

                            <p>
                                <?php echo substr($dato1['detalle'],0,228); ?>
                            </p>

                            <a href="destacados.php" class="btnTransNaranja btnBoxShadow">click aquí</a>
                        </div>
                    </div>
                    <div class="informacion-destacados col-md-8 col-sm-12 col-xs-12">
                        <?php
                		    $central = busquedasGenerales("tienda_producto", " WHERE destacado=1 and estado =1", "ORDER BY id DESC LIMIT 0,3");
                                while ($rowCentral = mysqli_fetch_array($central)) {  ?>
                      
                                 <div class="col-md-4 col-sm-6 col-xs-12">
                                            <div class="info-viaje">
                                                <a href="detalle-planes.php?id=<?php echo $rowCentral['id']; ?>"><div class="contImgz"><img src="public/img/productos/<?php echo $rowCentral['id']; ?>/principal.png<?php echo elRandom()?>" alt=""></div></a>
                                                <a href="detalle-planes.php?id=<?php echo $rowCentral['id']; ?>"><h3 class="titVerde btnBoxShadow"><?php echo $rowCentral['titulo']; ?><br>$ <?php echo number_format($rowCentral['valor'], 0, '', '.'); ?></h3></a>
                                            </div>
                                        </div>

                             
                                 
                	   <?php }  ?>  
                        
                        
                        
                        
                        
                       

                    </div>
                </div>
            </div>
        </div>

        <?php include 'partials/implementacion.php'; ?>        

        <?php include 'partials/suscripcion.php'; ?>        
        
        <?php include 'partials/footer.php'; ?>        

    </body>
</html>