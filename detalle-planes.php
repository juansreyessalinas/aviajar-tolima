<?php include("partials/funciones.php");

$publicidad1= busquedaIndividual("imagenes", "id = 46");
$not= busquedasGenerales("noticias", "WHERE estado = 1 ", "ORDER BY id DESC LIMIT 0,3");
$metas       = busquedaIndividual("metatags", "id = 1"); 
$dato1 = busquedaIndividual("paginas", "id=1");
$dato2 = busquedaIndividual("paginas", "id=2");
$dato3 = busquedaIndividual("paginas", "id=3");

$documen = busquedaIndividual("tienda_producto", "id=".$_REQUEST['id']);

$img1 = busquedaIndividual("imagenes", "id=103");
$img2 = busquedaIndividual("imagenes", "id=93");
$img3 = busquedaIndividual("imagenes", "id=94");
$img4 = busquedaIndividual("imagenes", "id=95");
$img5 = busquedaIndividual("imagenes", "id=96");
$img6 = busquedaIndividual("imagenes", "id=97");

?>
<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8" />
        <title><?php echo $documen['titulo']; ?> - A VIAJAR TOLIMA OPERADORES TURISTICOS</title>
        <meta name="description" content="<?php echo $documen['titulo']; ?> - A VIAJAR TOLIMA OPERADORES TURISTICOS">
        <meta name="author" content="www.creasotol.com, contactos@creasotol.com">
        <meta name="Subject" content="DETALLE PLANES - A VIAJAR TOLIMA OPERADORES TURISTICOS. ">
        <meta name="author" content="www.creasotol.com, contactos@creasotol.com">
        <meta name="robots" content="index,follow">
        <meta NAME="Language" CONTENT="Spanish">
        <meta NAME="Revisit" CONTENT="1 day">
        <meta NAME="Distribution" CONTENT="Global">
        <!--para desactivar el cache de todos los navegadores-->
        <meta http-equiv="cache-control" content="max-age=0" />
        <meta http-equiv="cache-control" content="no-cache, no-store, must-revalidate" />
        <meta http-equiv="pragma" content="no-cache" />
        <meta http-equiv="Expires" content="0">
        <meta http-equiv="Last-Modified" content="0">
        <!--cierra-->
        <!-- Mobile Specific Metas
        ================================================== -->
        
        <?php include 'partials/scriptsGlobal.php';  ?>
        
    </head>
    <body id="pageDetaPlan" class="">                
        <?php include 'partials/header.php'; ?>

        <div class="tit-detaplan">
            <div class="img-full"><img src="public/img/productos/<?php echo $documen['id']; ?>/principal1.png<?php echo elRandom()?>" alt="<?php echo $documen['titulo']; ?>"></div>
        </div>

        <div class="info-detaPlan disTable">

            <div class="container">
                <div class="row">
                    <div class="titulo">
                        <h4><?php echo $documen['titulo']; ?></h4>
                       
                    </div>

                    <div class="clearfix"></div>

                    <div class="col-izq">
                        <?php echo $documen['descripcion']; ?>
                    </div>

                    <div class="col-der">
                        <img src="public/img/productos/<?php echo $documen['id']; ?>/principal2.png<?php echo elRandom()?>" alt="<?php echo $documen['titulo']; ?>">
                    </div>

                    <div class="cont-infocontacto">
                        <div class="info-contacto">
                            <p>Informes:  (57 8) 270 1105 - 312 485 7386</p>
                            <img src="public/img/logo.png" alt="">
                        </div>

                        <a class="btnVerde btnBoxShadow" href="solicitar.php?id=<?php echo $documen['id']; ?>">Solicitar</a> &nbsp; &nbsp;
                        <a class="btnVerde btnBoxShadow" href="formulariopago.php?t=<?php echo $documen['valor']; ?>&com=<?php echo $documen['titulo']; ?>" target="_blank">Comprar</a>
                    </div>
                </div>
            </div>
        </div>

        
        <?php include 'partials/footer.php'; ?>        

    </body>
</html>