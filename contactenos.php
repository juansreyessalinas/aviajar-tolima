<?php include("partials/funciones.php");

$publicidad1= busquedaIndividual("imagenes", "id = 46");
$not= busquedasGenerales("noticias", "WHERE estado = 1 ", "ORDER BY id DESC LIMIT 0,3");
$metas       = busquedaIndividual("metatags", "id = 1"); 
$dato1 = busquedaIndividual("paginas", "id=1");
$dato2 = busquedaIndividual("paginas", "id=2");
$dato3 = busquedaIndividual("paginas", "id=3");



$img1 = busquedaIndividual("imagenes", "id=103");
$img2 = busquedaIndividual("imagenes", "id=110");
$img3 = busquedaIndividual("imagenes", "id=94");
$img4 = busquedaIndividual("imagenes", "id=95");
$img5 = busquedaIndividual("imagenes", "id=96");
$img6 = busquedaIndividual("imagenes", "id=97");

?>
<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8" />
        <title>CONTACTENOS - A VIAJAR TOLIMA OPERADORES TURISTICOS</title>
        <meta name="description" content="CONTACTENOS - A VIAJAR TOLIMA OPERADORES TURISTICOS">
        <meta name="author" content="www.creasotol.com, contactos@creasotol.com">
        <meta name="Subject" content="CONTACTENOS - A VIAJAR TOLIMA OPERADORES TURISTICOS. ">
        <meta name="author" content="www.creasotol.com, contactos@creasotol.com">
        <meta name="robots" content="index,follow">
        <meta NAME="Language" CONTENT="Spanish">
        <meta NAME="Revisit" CONTENT="1 day">
        <meta NAME="Distribution" CONTENT="Global">
        <!--para desactivar el cache de todos los navegadores-->
        <meta http-equiv="cache-control" content="max-age=0" />
        <meta http-equiv="cache-control" content="no-cache, no-store, must-revalidate" />
        <meta http-equiv="pragma" content="no-cache" />
        <meta http-equiv="Expires" content="0">
        <meta http-equiv="Last-Modified" content="0">
        <!--cierra-->
        <!-- Mobile Specific Metas
        ================================================== -->
        
        <?php include 'partials/scriptsGlobal.php';  ?>
        
    </head>
    <body id="pageContactenos" class="">                
        <?php include 'partials/header.php'; ?>
       
        <div class="info-contactenos disTable">

            <div class="container">
                <div class="row">
                    <h1>Contáctenos</h1>
                    <div class="clearfix"></div>
                    
                    <div class="col-md-7 col-sm-12 col-xs-12">    

                        <p><?php echo $dato3['detalle']; ?></p>                 

                        <form id="formContacto" class="form-envio">
                            <div class="form-group">
                                <input type="text" class="form-control" id="txtNombre" name="txtNombre" placeholder="Nombre *" required>
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" id="txtMail" name="txtMail" placeholder="E-mail *" required>
                            </div>
                            <div class="form-group form-2">
                                <input type="text" class="form-control" id="txtTelefono" name="txtTelefono" placeholder="Teléfono *" required>
                                <input type="text" class="form-control" id="txtCiudad" name="txtCiudad" placeholder="Ciudad *" required>
                            </div>            
                            <div class="form-group">
                                <textarea id="txtComentario" name="txtComentario" cols="30" rows="10" class="form-control" placeholder="Comentario *" required></textarea>
                            </div>
                            
                            <div id="contBtnEnv">
                                <label class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" required>
                                    <span class="custom-control-indicator"></span>
                                    <span class="custom-control-description">Al registrarte estás aceptando <a href="terminos-y-condiciones.php">términos y condiciones</a> y <a href="politica-de-datos.php">Política Tratamiento de Datos</a> www.aviajartolima.com cuenta con rigurosos estándares de seguridad. Todos tus datos se mantendrán en estricta confidencialidad. <a href="politica-datos.php">Ver política de seguridad.</a></span>
                                </label>
                                <input value="enviar mensaje" class="btnNaranja btnBoxShadow" id="btnEnviar" name="btnEnviar" type="submit">
                            </div>
                        </form>
                    </div>

                    <div class="col-md-4 col-md-offset-1 col-sm-12 col-xs-12">
                        <div class="texto-contacto">
                           <p>Teléfono: <?php echo $generales['telefonos'] ?></p>
					
						<p>Email: <?php echo $generales['email'] ?></p>
						<p>Dirección: <?php echo $generales['direccion'] ?></p>
                            <img src="public/img/iconocontacto.png" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="img-contacto">
            <div class="img-full"><img src="public/img/imagenes/110.jpg<?php echo elRandom()?>" alt="PAGINA CONTACTO"></div>           

            <img src="public/img/logo.png" alt="PAGINA CONTACTO" class="img-abs">
            <h2 class="texto"><?php echo $img2['titulo']; ?></h2>
        </div>
        
        <?php include 'partials/footer.php'; ?>    
    </body>
</html>

<?php
require("partials/PHPMailer/_lib/class.phpmailer.php");
if(isset($_REQUEST['btnEnviar'])){
    //$captcha = json_decode(file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret=6LfWRCAUAAAAAOdHudxMNGtiUeiAdpnXfM-rxy1i&response='.$_REQUEST['g-recaptcha-response'].'&remoteip='.$_SERVER['REMOTE_ADDR']),TRUE);
    /*if($captcha['success']){*/
    $nombre         = $_REQUEST['txtNombre'];
    $telefo         = $_REQUEST['txtTelefono'];
    $ciudad         = $_REQUEST['txtCiudad'];
    $correo         = $_REQUEST['txtMail'];
    $comentario     = $_REQUEST['txtComentario'];
    $fechaS         = date('Y-m-d H:m:s');
    //$para         = $elCorreo;
    $para         = 'aviajartolima.com@gmail.com';
    $asunto         = '[CONTACTENOS] - '.$_SERVER['HTTP_HOST'];
    $mensaje        = "<html>
        <head>
            <title>:.. CONTACTENOS ..:</title>
            <style>
            .linkpag{
            text-decoration:none;
            color:#39F;
            }
            </style>
        </head>
        Una persona ha enviado un mensaje a traves de la pagina web:<br><br>
        <body>
            
            Fecha Envio: ".$fechaS."<br><br>
            
            <b>DATOS PERSONALES:</b><br><br>
            <b>NOMBRE:</b> ".$nombre."<br>
            <b>TELEFONO:</b> ".$telefo."<br>
            <b>CIUDAD / MUNICIPIO:</b> ".$ciudad."<br>
            <b>CORREO ELECTRONICO:</b> ".$correo."<br>
            <b>COMENTARIO O SOLICITUD:</b> ".$comentario."<br>
            <br>
            <br>
            <br>
            Enviado desde, Pagina Web
            <br />
            <b>AVIAJAR TOLIMA</b>
            <br />
            <a href='http://".$_SERVER['HTTP_HOST']."' target='_blank' class='linkpag'>".$_SERVER['HTTP_HOST']."</a>
            <br />
            <br />
            <br />
            <b>NOTA:</b> Por favor, no responder a este correo automatico.
        </body>
    </html>";
    $mail = new PHPMailer();
    $mail->Host     = "localhost";
    $mail->From     = "seguridad@creasotol.com";
    $mail->FromName = $correo;
    $mail->Subject  = $asunto;
    $mail->AddAddress($para, "AVIAJAR TOLIMA");
    $mail->Body     = $mensaje;
    $mail->AltBody  = "CONTACTENOS";
    $mail->Send();
    echo '<script>
    alert("Su mensaje se ha enviado correctamente. Pronto nos comunicaremos con usted.");
    location.href = "index.php";
    </script>';
    /*}else{
        echo "<script> alert(':.. Error al ingresar el codigo ..:'); </script>";
    }*/
}
?>