<?php $dato4 = busquedaIndividual("paginas", "id=4"); 
$dato8 = busquedaIndividual("paginas", "id=8");

?>
<footer class="disTable">	
	<div class="info-web disInlineB">
		<div class="container">
			<div class="row">
				<div class="col-md-3 col-sm-12 col-xs-12">

					<div class="redes">
						<h3>Síguenos</h3>
						<?php include 'partials/redes.php'; ?>
					</div>
					<div class="informacion">
						<p>Teléfono: <?php echo $generales['telefonos'] ?></p>
					
						<p>Email: <?php echo $generales['email'] ?></p>
						<p>Dirección: <?php echo $generales['direccion'] ?></p>
					
					</div>
					
				</div>

				<div class="col-md-6 col-sm-12 col-xs-12">
					<img src="public/img/logoblanco1.png" alt="a viajar tolima viajes turisticos">
					<div class="texto">
						<p>A VIAJAR TOLIMA <?php echo date("Y"); ?> All copyright</p>
						<span>-</span>
						<p class="creasotol"><a href="https://creasotol.com/" target="_blank" title="Diseño de paginas Web" rel="follow">DISEÑO WEB </a>:  <a href="https://creasotol.com/" target="_blank" title="Diseño Web" rel="follow">www.creasotol.com</a></p>
					</div>
					
				</div>

				<div class="col-md-3 col-sm-12 col-xs-12">
					<div class="sostenibilidad">
						<h4>POLÍTICA DE SOSTENIBILIDAD</h4>
						<p>
						    <?php echo substr($dato4['detalle'],0,200); ?>...
						</p>

						<a href="politicas.php" class="btnNaranja btnBoxShadow">leer mas</a><br><br>
						<h4><?php echo $dato8['titulo'] ?></h4>
						<p>
						    <?php echo substr($dato8['detalle'],0,200); ?>...
						</p>

						<a href="politicasin.php" class="btnNaranja btnBoxShadow">read more</a>
					</div>
				</div>

			</div>
		</div>
	</div>
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA2b3YrlPVAmGm7ebHuYvMUTqoUELZFN0Q&callback=initMap" ></script>
		<div class="divMapa mapa">
			<div class="antiClic" onclick="style.display='none'"></div>
			<input type="hidden" class="form-control" name="lati" id="lati1" required="required" readonly value="<?php echo $generales['lati'];?>">
			<input type="hidden" class="form-control" name="longi" id="longi1" required="required" readonly value="<?php  echo $generales['longi'];?>">
			<div id="map1" class="mapa"  ></div>
			<br>
			
			<script type="text/javascript">
			
			
			$(document).ready(function() {
				initMap();
				var map = null;
				var infoWindow = null;
			
				function openInfoWindow(marker) {
					var markerLatLng = marker.getPosition();
					infoWindow.setContent([
					'<strong>La posicion del marcador es:</strong><br/>',
					markerLatLng.lat(),
					', ',
					markerLatLng.lng(),
					'<br/>Arrastrame para actualizar la posicion.'
					].join(''));
					infoWindow.open(map, marker);
					document.getElementById('lati1').value= markerLatLng.lat() ;
					document.getElementById('longi1').value= markerLatLng.lng() ;
				
				}
			
				function initMap() {
					var myLatlng = new google.maps.LatLng(document.getElementById('lati1').value ,document.getElementById('longi1').value);
					var myOptions = {
					zoom: 16,
					center: myLatlng,
					mapTypeId: google.maps.MapTypeId.ROADMAP
					};
					
					map = new google.maps.Map(document.getElementById('map1'), myOptions);
					
					infoWindow = new google.maps.InfoWindow();
					
					var marker = new google.maps.Marker({
					position: myLatlng,
					draggable: true,
					map: map,
					title:"Ejemplo marcador arrastrable"
					});
					google.maps.event.addListener(marker, 'dragend', function(){ openInfoWindow(marker); });
					google.maps.event.addListener(marker, 'click', function(){ openInfoWindow(marker); });
				}
			
			});
			</script>
			
		</div>
</footer>

<script src="https://ajax.googleapis.com/ajax/libs/webfont/1/webfont.js"></script>
<script>
	WebFont.load({
	google: {
	families: ['Lato', 'Droid Serif', 'Roboto', 'Open+Sans', 'Reem Kufi', 'Questrial', 'Montserrat','Source+Sans+Pro']
	}
	});
</script>


<script>
	jQuery(document).ready(function($) {
		
		$('.btnBoxShadow').hover(function() {

			if ($(this).hasClass('btnNaranja') ) {
				colTrans = "#D8630D";
			}			
			if ($(this).hasClass('btnTransBlanco')) {
				colTrans = "#000000";
			}
			if ($(this).hasClass('btnTransNaranja')) {
				colTrans = "#f58634";
			} 
			if ($(this).hasClass('titVerde') || $(this).hasClass('btnVerde')) {
				colTrans = "#00846E";
			}
			if ($(this).hasClass('btnTransBlanco')) {
				colTrans = "#ffffff";
			}

			wid = $(this).outerWidth();
			fondo = $(this).css("background-color");

			estilo = 'inset '+wid+'px 0px 0px '+colTrans;

			$(this).css('box-shadow', estilo); 
		}, function() {
			$(this).css('box-shadow', ''); 
		});
	});
</script>