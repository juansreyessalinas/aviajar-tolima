<?php
/**
* CLASE CONEXION
*/
class conexion{
	private $conexion;
	
	/*public function __construct(){
		$servidor   = "localhost";
		$usuario    = "root";
		$contrasena = "";
		$basedatos  = "bd_fundavida";
		
		$this->conexion = mysqli_connect($servidor, $usuario, $contrasena);
		mysqli_select_db($this->conexion, $basedatos)or die(mysqli_error());
	}

	/**
	 * [FUNCION QUE BUSCA Y DEVUELVE SI LA CONSULTA EXISTE]
	 * @param  [VARCHAR]     $sql [VARIABLE QUE TRAE LA CONSULTA A EJECUTAR]
	 * @return [MYSQL_QUERY]      [VARIABLE QUE DEVUELVE LA EJECUCION DE LA CONSULTA]
	 */
	public function __construct(){
		$servidor   = "localhost";
		$usuario    = "aviajart_root";
		$contrasena = "VC5~28sv;M{M";
		$basedatos  = "aviajart_bd";

		
		$this->conexion = mysqli_connect($servidor, $usuario, $contrasena);
		mysqli_select_db($this->conexion, $basedatos)or die(mysqli_error($this->conexion));
	}


	/**
	 * [FUNCION QUE BUSCA Y DEVUELVE SI LA CONSULTA EXISTE]
	 * @param  [VARCHAR]     $sql [VARIABLE QUE TRAE LA CONSULTA A EJECUTAR]
	 * @return [MYSQL_QUERY]      [VARIABLE QUE DEVUELVE LA EJECUCION DE LA CONSULTA]
	 */
	public function busca($sql){
		$res = mysqli_query($this->conexion, $sql);
		$row = mysqli_fetch_array($res);
		return $row;
	}


	/**
	 * [FUNCION QUE EJECUTA Y DEVUELVE LA EJECUCION DE UNA CONSULTA]
	 * @param  [VARCHAR]     $sql [VARIABLE QUE TRAE LA CONSULTA A EJECUTAR]
	 * @return [MYSQL_QUERY]      [VARIABLE QUE DEVUELVE LA EJECUCION DE LA CONSULTA]
	 */
	public function filtro($sql){
		$result = mysqli_query($this->conexion, $sql);
		return $result;
	}


	/**
	 * [FUNCION QUE FINALIZA LA EJECUCION DE UNA CONSULTA]
	 * @param  [VARCHAR]    $query [VARIABLE QUE TRAE LA EJECUCION DE UNA CONSULTA A FINALIZAR]
	 */
	public function cerrarfiltro($query){
		mysqli_free_result($query);
	}


	/**
	 * [FUNCION QUE RETORNA LOS REGISTROS DE LA EJECUCION DE UNA CONSULTA]
	 * @param  [VARCHAR] 				   $query [VARIABLE QUE TRAE LA EJECUCION DE UNA CONSULTA]
	 * @return [mysqli_fetch_array]        [VARIABLE QUE DEVUELVE LOS REGISTROS DE LA VARIABLE TRAIDA]
	 */
	public function filas($query){
		$row = mysqli_fetch_array($query);
		return $row;
	}


	/**
	 * [FUNCION QUE CONSULTA UNA TABLA ENVIADA SEGUN LOS CAMPOS ENVIADOS]
	 * @param  [VARCHAR] $tabla  [VARIABLE QUE TRAE LA TABLA EN LA QUE SE VA A B]
	 * @param  [VARCHAR] $campos [VARIABLE QUE TRAE LOS CAMPOS A BUSCAR]
	 */
	public function consulta($tabla, $campos, $orden = ''){
		$sql = "SELECT * FROM $tabla WHERE $campos $orden";
		$res = mysqli_query($this->conexion, $sql) or die (mysqli_error($this->conexion));
		$row = mysqli_fetch_array($res);
		return $row;
	}


	/**
	 * [FUNCION QUE ACTUALIZA UN REGISTRO]
	 * @param  [VARCHAR] $tabla  [VARIABLE QUE TRAE LA TABLA QUE SE VA A ACTUALIZAR]
	 * @param  [VARCHAR] $campos [VARIABLE QUE TRAE LOS CAMPOS QUE SE VAN A ACTUALIZAR]
	 * @param  [VARCHAR] $where  [VARIABLE QUE TRAE EL CAMPO DE BUSQUEDA]
	 */
	public function actualizar($tabla, $campos, $where){
		$upd = "UPDATE $tabla SET $campos WHERE $where";
		$res = mysqli_query($this->conexion, $upd) or die (mysqli_error($this->conexion));
	}


	/**
	 * [FUNCION QUE INGRESA UN REGISTRO]
	 * @param  [VARCHAR] $tabla  [VARIABLE QUE TRAE LA TABLA QUE SE VA A INGRESAR]
	 * @param  [VARCHAR] $campos [VARIABLE QUE TRAE LOS CAMPOS QUE SE VAN A INGRESAR]
	 * @param  [VARCHAR] $where  [VARIABLE QUE TRAE LOS VALORES QUE SE VAN A INGRESAR]
	 */
	public function insertar($tabla, $campos, $valores){
		$ins = "INSERT INTO $tabla ($campos) VALUES ($valores)";
		$res = mysqli_query($this->conexion, $ins) or die (mysqli_error($this->conexion));
	}


	/**
	 * [FUNCION QUE EJECUTA UNA CONSULTA]
	 * @param  [VARCHAR] $sql [VARIABLE QUE TRAE LA CONSULTA A EJECUTAR]
	 */
	public function ejecutar($sql){
		mysqli_query($this->conexion, $sql)or die("No se puede eliminar el dato, existen datos ligados.");
	}


	/**
	 * [FUNCION QUE EJECUTA UNA CONSULTA Y DEVUELVE EL ULTIMO ID CREADO]
	 * @param  [VARCHAR] $sql [VARIABLE QUE TRAE LA CONSULTA A EJECUTAR]
	 * @return [INT]      	  [VARIABLE QUE DEVUELVE EL ID DEL ULTIMO REGISTRO INGRESADO]
	 */
	public function ejecutaId($sql){
		mysqli_query($this->conexion, $sql);
		return $this->conexion->insert_id;
	}


	/**
	 * [FUNCION QUE RETORNA LA CANTIDAD DE REGISTROS EJECUTADOS]
	 * @param  [VARCHAR] $res [VARIABLE QUE TRAE LA CONSLTA EJECUTADA A CONTAR]
	 * @return [INT]      	  [VARIABLE QUE DEVUELVE LA CANTIDAD DE REGISTROS BUSCADOS]
	 */
	public function numeroRegistros($res){
		return mysqli_num_rows($res);
	}


	/**
	 * [FUNCION QUE RETORNA SI HAY REGISTROS O NO DE UNA CONSULTA PASADA]
	 * @param  [VARCHAR] $sql [VARIABLE QUE TRAE LA CONSULTA A VERIFICAR]
	 * @return [INT]      	  [VARIABLE QUE DEVUELVE 1 O 0]
	 */
	public function siHayRegistros($sql){
		$res = mysqli_query($this->conexion, $sql);
		$num = mysqli_num_rows($res);
		
		if($num > 0){
			return 1;
		}else{
			return 0;
		}
	}


	/**
	 * [FUNCION QUE CIERRA LA CONEXION ABIERTA]
	 */
	public function cerrarconexion(){
		mysqli_close($this->conexion);
	}


	/**
	 * [FUNCION DESTRUCTORA DE LA CLASE]
	 */
	public function __destruct(){

	}

}



?>