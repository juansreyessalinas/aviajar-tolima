<?php 
include("partials/scripts.php");
$generales= busquedaIndividual("configuracion", "id = 1");

?>
<script>
    $(document).ready(function() {

        if (screen.width >= 481 && screen.width <= 780) {

            $('.navbar a.dropdown-toggle').on('click', function(e) {
                var $el = $(this);
                var $parent = $(this).offsetParent(".dropdown-menu");
                $(this).parent("li").toggleClass('open');

                if(!$parent.parent().hasClass('nav')) {
                    $el.next().css({"top": $el[0].offsetTop, "left": $parent.outerWidth() - 4});
                }

                $('.nav li.open').not($(this).parents("li")).removeClass("open");

                return false;
            });

            $(".infoH.infor").insertAfter('.infoH.logo');
           
        }
    if (screen.width <= 480) {

        $("nav.navbar").addClass("navbar-fixed-top");

        $('.navbar a.dropdown-toggle').on('click', function(e) {
            var $el = $(this);
            var $parent = $(this).offsetParent(".dropdown-menu");
            $(this).parent("li").toggleClass('open');

            if(!$parent.parent().hasClass('nav')) {
                $el.next().css({"top": $el[0].offsetTop, "left": $parent.outerWidth() - 4});
            }

            $('.nav li.open').not($(this).parents("li")).removeClass("open");

            return false;
        });
       
    } else {
        jQuery('li.dropdown').hover(function() {
            jQuery(this).children('.dropdown-menu').stop(true, true).delay(200).fadeIn();
            }, function() {
            jQuery(this).children('.dropdown-menu').stop(true, true).delay(200).fadeOut();
        });
    }

        
    });
</script>

<header>
    <div class="container">
        <div class="row">
            <div class="infoH logo col-md-3 col-sm-3 col-xs-12">
                <a href="index.php"><img src="public/img/logo.png" alt="a viajar tolima" class="imgLogo"></a>              
            </div>
            <div class="infoH menu col-md-6 col-sm-12 col-xs-12">
                <div class="infoC">
                    <?php include 'partials/menu.php'; ?>
                </div>
            </div>
            <div class="infoH infor col-md-3 col-md-offset-0 col-sm-offset-2 col-sm-6 col-xs-12">
                <div class="info-header">
                    <div class="info">
                        <p class="tels"><?php echo $generales['telefonos'] ?></p>
                       <!-- <p class="ciudad">Ibagué - Tolima</p>-->
                    </div>
                    <?php include 'partials/redes.php'; ?>
                </div>
            </div>
        </div>
    </div>
</header>