<meta charset="UTF-8">
<link rel="shortcut icon" href="public/img/favicon.ico" />
<title><?php echo $metas['titulo'];?></title>
<meta name="description" content="<?php echo $metas['descripcion'];?>">
<meta name="keywords" content="<?php echo $metas['palabras'];?>">
<meta name="Subject" content="<?php echo $metas['tema'];?>">
<meta name="robots" content="index,follow">
<meta NAME="Language" CONTENT="Spanish">
<meta NAME="Revisit" CONTENT="1 day">
<meta NAME="Distribution" CONTENT="Global">
<!--para desactivar el cache de todos los navegadores-->
<meta http-equiv="cache-control" content="max-age=0" />
<meta http-equiv="cache-control" content="no-cache, no-store, must-revalidate" />
<meta http-equiv="pragma" content="no-cache" />
<meta http-equiv="Expires" content="0">
<meta http-equiv="Last-Modified" content="0">
<!-- <?php //include 'partials/scripts.php';  ?> -->