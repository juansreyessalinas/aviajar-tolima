<div class="redes-sociales">
	<a href="<?php echo $generales['facebook'] ?>" target="_blank" class="icon-button facebook"><i class="fa fa-facebook"></i><span></span></a>
	<a href="<?php echo $generales['instagram'] ?>" target="_blank" class="icon-button instagram"><i class="fa fa-instagram"></i><span></span></a>
	<a href="<?php echo $generales['twitter'] ?>" target="_blank" class="icon-button twitter"><i class="fa fa-twitter"></i><span></span></a>
	<a href="<?php echo $generales['google'] ?>" target="_blank" class="icon-button google-plus"><i class="fa fa-google-plus"></i><span></span></a>
	<a href="<?php echo $generales['youtube'] ?>" target="_blank" class="icon-button youtube"><i class="fa fa-youtube"></i><span></span></a>
</div>