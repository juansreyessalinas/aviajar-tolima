<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script> -->

<div id="modal-js" class="modal">

  <!-- Modal content -->
  <div class="modal-content">
    <span class="close-modal">&times;</span>
    <a href="#"><img src="ventana-modal/modal-aviajar.jpg" alt=""></a>
  </div>

</div>


<style>
.modal { display: none; position: fixed; z-index: 99999999; left: 0; top: 0; width: 100%; height: 100%; overflow: auto; background-color: rgba(0, 0, 0, 0.6); }
.modal-content { background-color: #fefefe; margin: 6% auto 0; padding: 20px; border: 1px solid #888; width: 540px; position: relative;}
.modal-content img { width: 100%; }
.close-modal { color: #aaa; float: right; font-size: 28px; font-weight: bold; position: absolute; top: -21px; right: -21px; background-color: #333; width: 40px; height: 40px; text-align: center; line-height: 40px; border-radius: 50%; }
.close-modal:hover,
.close-modal:focus { color: #fff; text-decoration: none; cursor: pointer; }

@media only screen and (min-width:1368px) and (max-width:1720px) {
	.modal-content { width: 460px; }
}


@media only screen and (min-width:1285px) and (max-width:1366px) {

	.modal-content { margin: 5% auto 0; width: 400px; }
}


@media only screen and (min-width:1195px) and (max-width:1285px) {

	.modal-content { margin: 5% auto 0; width: 400px;  }
}


/*Tablet*/
@media only screen and (min-width:768px) and (max-width:1026px) {


}


@media only screen 
and (min-device-width:768px) 
and (max-device-width:1024px) {

	.modal-content { margin: 16% auto 0; width: 500px; }

}


/* iPads (landscape) ----------- */
@media only screen and (min-device-width:768px) and (max-device-width:1024px) and (orientation:landscape) {
}


/*Smartphone*/
@media only screen and (min-width:320px) and (max-width:480px) {
	.modal-content { margin: 30% auto 0; width: 300px; }
}

@media only screen and (min-device-width: 480px) 
                   and (max-device-width: 640px) 
                   and (orientation: landscape) {

	.modal-content { margin: 5% auto 0; width: 240px;  }
}
</style>


<script>
	jQuery(document).ready(function($) {

		var modal = document.getElementById('modal-js');
		var span = document.getElementsByClassName("close-modal")[0];

		// When the user clicks on <span> (x), close the modal
		span.onclick = function() {
		    modal.style.display = "none";
		}

		// When the user clicks anywhere outside of the modal, close it
		window.onclick = function(event) {
		    if (event.target == modal) {
		        modal.style.display = "none";
		    }
		}

		setTimeout(function() {
		    $('#modal-js').show();
		}, 2500);
	});
</script>