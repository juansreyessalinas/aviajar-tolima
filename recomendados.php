<?php include("partials/funciones.php");

$publicidad1= busquedaIndividual("imagenes", "id = 46");
$not= busquedasGenerales("noticias", "WHERE estado = 1 ", "ORDER BY id DESC LIMIT 0,3");
$metas       = busquedaIndividual("metatags", "id = 1"); 
$dato1 = busquedaIndividual("paginas", "id=1");
$dato2 = busquedaIndividual("paginas", "id=2");
$dato3 = busquedaIndividual("paginas", "id=3");



$img1 = busquedaIndividual("imagenes", "id=92");
$img2 = busquedaIndividual("imagenes", "id=93");
$img3 = busquedaIndividual("imagenes", "id=94");
$img4 = busquedaIndividual("imagenes", "id=95");
$img5 = busquedaIndividual("imagenes", "id=96");
$img6 = busquedaIndividual("imagenes", "id=97");

?>
<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8" />
        <title>RECOMENDADOS- A VIAJAR TOLIMA OPERADORES TURISTICOS</title>
        <meta name="description" content="RECOMENDADOS - A VIAJAR TOLIMA OPERADORES TURISTICOS">
        <meta name="author" content="www.creasotol.com, contactos@creasotol.com">
        <meta name="Subject" content="RECOMENDADOS - A VIAJAR TOLIMA OPERADORES TURISTICOS. ">
        <meta name="author" content="www.creasotol.com, contactos@creasotol.com">
        <meta name="robots" content="index,follow">
        <meta NAME="Language" CONTENT="Spanish">
        <meta NAME="Revisit" CONTENT="1 day">
        <meta NAME="Distribution" CONTENT="Global">
        <!--para desactivar el cache de todos los navegadores-->
        <meta http-equiv="cache-control" content="max-age=0" />
        <meta http-equiv="cache-control" content="no-cache, no-store, must-revalidate" />
        <meta http-equiv="pragma" content="no-cache" />
        <meta http-equiv="Expires" content="0">
        <meta http-equiv="Last-Modified" content="0">
        <!--cierra-->
        <!-- Mobile Specific Metas
        ================================================== -->
        
        <?php include 'partials/scriptsGlobal.php';  ?>
        
    </head>
    <body id="pageMiTierra" class="">                
        <?php include 'partials/header.php'; ?>

        <div class="tit-planes">
            <h1 class="title">Planes Turísticos Recomendados</h1>
        </div>


        <div class="cont-planes disTable">
            <?php
            
             $central = busquedasGenerales("tienda_producto", " WHERE recomendado=1 AND estado=1", "ORDER BY id DESC ");
                 $cantProd  = mysqli_num_rows($central);
		        if( $cantProd==0){
		            echo "<h1>EN ESTE MOMENTO NO EXISTEN DESTINOS DISPONIBLES</h1>";
		        }
                while ($rowCentral = mysqli_fetch_array($central)) {  ?>
      
                

             <div class="plan disTable">
                <div class="img-full">
                   <a href="detalle-planes.php?id=<?php echo $rowCentral['id']; ?>"> <img src="public/img/productos/<?php echo $rowCentral['id']; ?>/principal1.png<?php echo elRandom()?>" alt="<?php echo $rowCentral['titulo']; ?>"></a>
                </div>
                <div class="cont-centrado">
                    <div class="titulo">
                        <h4><?php echo $rowCentral['titulo']; ?></h4>
                        
                        <p class="precio">$ <?php echo number_format($rowCentral['valor'], 0, '', '.'); ?></p>
                    </div>
                    <a class="btnVerde btnBoxShadow" href="detalle-planes.php?id=<?php echo $rowCentral['id']; ?>">Ver detalle</a>
                </div>
            </div>
                 
	            <?php }  ?>  


 

            <div class="container">
                <div class="row">
                    <!--
                    <nav class="pagi">
                        <ul class="pagination">
                            <li>
                                <a href="#" aria-label="Previous">
                                    <
                                </a>
                            </li>
                            <li class="actual"> <a href="#">1</a></li>
                            <li><a href="#">2</a></li>
                            <li><a href="#">3</a></li>
                            <li>
                                <a href="#" aria-label="Next">
                                    >|
                                </a>
                            </li>
                        </ul>
                    </nav>-->
                </div>
            </div>
        </div>     

        
        <?php include 'partials/footer.php'; ?>        

    </body>
</html>