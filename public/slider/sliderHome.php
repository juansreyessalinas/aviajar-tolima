<script>
	jQuery(document).ready(function($) {

		$(".owl-carousel").owlCarousel({
		    loop:true,
		    autoplay: true,
		    navSpeed: 2500,
		    smartSpeed: 2500,
		    fluidSpeed: 2500,
		    nav:true,
		    navText: ["<img src='public/img/botonizquierdo.png'>","<img src='public/img/botonderecho.png'>"],
		    margin:0,
		    items: 1,
		});
		
	});
	
</script>

<div class="owl-carousel">
    
    <?php
		    $central = busquedasGenerales("banner", " WHERE tipo=1 and estado=1", "ORDER BY posicion ASC");
                while ($rowCentral = mysqli_fetch_array($central)) {  ?>
      
                 
	<div>
	    <a href="<?php echo $rowCentral['url']; ?>">
		<img src="public/img/banner/<?php echo $rowCentral['id']; ?>.jpg" alt=""> 
		<div class="infoBanner">
			<h2><span><?php echo $rowCentral['titulo']; ?></span><?php echo $rowCentral['subtitulo']; ?> </h2>
		</div>
		</a>
	</div>
                
	<?php }  ?>  
   

</div>