<link rel="stylesheet" type="text/css" href="public/js/slick/slick.css"/>
<link rel="stylesheet" type="text/css" href="public/js/slick/slick-theme.css"/>
<script type="text/javascript" src="public/js/slick/slick.min.js"></script>


<div class="carruHome1" id="carru-temas">

  <div>
    <div class="info">
      <div class="contImgz"><img src="public/img/interes1.jpg" alt=""></div>
      <h4>10 formas fáciles de leer unlibro en inglés</h4>
      <a href="detalle-tema.php" class="btnAzul btnBoxShadow">Leer más...</a>
    </div>
  </div>

  <div>
    <div class="info">
      <div class="contImgz"><img src="public/img/interes2.jpg" alt=""></div>
      <h4>como poder utilizar un buen vocabulario</h4>
      <a href="detalle-tema.php" class="btnAzul btnBoxShadow">Leer más...</a>
    </div>
  </div>

  <div>
    <div class="info">
      <div class="contImgz"><img src="public/img/interes3.jpg" alt=""></div>
      <h4>que cosas no se deben olvidar al momento de hablar</h4>
      <a href="detalle-tema.php" class="btnAzul btnBoxShadow">Leer más...</a>
    </div>
  </div>

  <div>
    <div class="info">
      <div class="contImgz"><img src="public/img/interes2.jpg" alt=""></div>
      <h4>como poder utilizar un buen vocabulario</h4>
      <a href="detalle-tema.php" class="btnAzul btnBoxShadow">Leer más...</a>
    </div>
  </div>
  

</div>


<script>
	
  $(document).ready(function() {
    $('#carru-temas').slick({
      dots: false,
      arrows: true,
      slidesToShow: 3,
      slidesToScroll: 1,
      autoplay: true,
      speed: 3200,
      responsive: [{
      breakpoint: 1024,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 1,
          infinite: !0,
          dots: !0
        }
        },{
          breakpoint: 780,
          settings: {
          slidesToShow: 2,
          slidesToScroll: 1
        }
        }, {
          breakpoint: 600,
          settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
        }, {
          breakpoint: 480,
          settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }]
      });
  });
</script>

<style>

	#carru-temas .slick-prev, #carru-temas .slick-next {  width: 53px;  height: 54px;  border-radius: 10px;  background-color: #85d1aa; }
  #carru-temas .slick-prev:hover, #carru-temas .slick-next:hover { background-color: #52d093; }
	#carru-temas .slick-prev::before {  color: #fff; content: '❮'; width: 60px; height: 60px; opacity: 1; font-size: 30px; }
  #carru-temas .slick-next::before {  color: #fff; content: '❯'; width: 60px; height: 60px; opacity: 1; font-size: 30px; } 
	#carru-temas .slick-next {   z-index: 999; right: -8%;  }
	#carru-temas .slick-prev {  z-index: 999; left: -8%; }
	
</style>