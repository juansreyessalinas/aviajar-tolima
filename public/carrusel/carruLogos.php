<link rel="stylesheet" type="text/css" href="public/js/slick/slick.css"/>
<link rel="stylesheet" type="text/css" href="public/js/slick/slick-theme.css"/>
<script type="text/javascript" src="public/js/slick/slick.min.js"></script>


<div class="carruHome1" id="carru-logos">

  <div>
    <div class="info">
      <a href="#" target="_blank"> <div class="contImgz"><img src="public/img/LOGOS/1.jpg" alt=""></div> </a>     
    </div>
  </div>

  <div>
    <div class="info">
      <a href="#" target="_blank"> <div class="contImgz"><img src="public/img/LOGOS/2.jpg" alt=""></div> </a>  
    </div>
  </div>

  <div>
    <div class="info">
      <a href="#" target="_blank"> <div class="contImgz"><img src="public/img/LOGOS/3.jpg" alt=""></div> </a>  
    </div>
  </div>

  <div>
    <div class="info">
      <a href="#" target="_blank"> <div class="contImgz"><img src="public/img/LOGOS/1.jpg" alt=""></div> </a>  
    </div>
  </div>
  

</div>


<script>
	
  $(document).ready(function() {
    $('#carru-logos').slick({
      dots: false,
      arrows: true,
      slidesToShow: 3,
      slidesToScroll: 1,
      autoplay: false,
      speed: 3200,
      responsive: [{
      breakpoint: 1024,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 1,
          infinite: !0,
          dots: !0
        }
        },{
          breakpoint: 780,
          settings: {
          slidesToShow: 2,
          slidesToScroll: 1
        }
        }, {
          breakpoint: 600,
          settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
        }, {
          breakpoint: 480,
          settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }]
      });
  });
</script>

<style>

	#carru-logos .slick-prev, #carru-logos .slick-next {  width: 53px;  height: 54px;   }
  #carru-logos .slick-prev:hover, #carru-logos .slick-next:hover { }
	#carru-logos .slick-prev::before {  color: #605e61; content: '❮'; width: 60px; height: 60px; opacity: 1; font-size: 30px; }
  #carru-logos .slick-next::before {  color: #605e61; content: '❯'; width: 60px; height: 60px; opacity: 1; font-size: 30px; } 
	#carru-logos .slick-next {   z-index: 999; right: -2%;  }
	#carru-logos .slick-prev {  z-index: 999; left: -2%; }


	
</style>